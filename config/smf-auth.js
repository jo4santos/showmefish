// expose our config directly to our application using module.exports
module.exports = {

    'facebookAuth' : {
        'clientID' 		: '380850002080100', // your App ID
        'clientSecret' 	: '4af7a4ffe5972229497ef705a5a5d503', // your App Secret
        'callbackURL' 	: 'http://showmefish-josapps.rhcloud.com/data/auth/facebook/callback'
    },

    'googleAuth' : {
        'clientID' 		: '716967070272-0vt0e6pgg71aphgl6gu2n5fq0m2vjnpg.apps.googleusercontent.com',
        'clientSecret' 	: 'vvr1YD5itOT826kREt_uZTDP',
        'callbackURL' 	: (process.env.OPENSHIFT_NODEJS_IP?'http://showmefish-josapps.rhcloud.com':'http://localhost:3000') + '/data/auth/google/callback'
    }

};
