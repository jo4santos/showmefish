var g_ = require(__dirname+'/smf-globals.js');

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;


//route middleware to make sure a user is logged in
g_.main.functions.isLoggedIn = function(req, res, next) {

	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
};

// expose this function to our app using module.exports
module.exports = function() {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    g_.main.passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    g_.main.passport.deserializeUser(function(id, done) {
    	g_.main.db.models.user.findById(id, function(err, user) {
            done(err, user);
        });
    });

 	// =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    g_.main.passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {
    	
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists
        	g_.main.db.models.user.findOne({ 'email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false);
            } else {
            	
				// if there is no user with that email
                // create the user
                var newUser            = new g_.main.db.models.user();

                // set the user's local credentials
                newUser.email		= email;
                newUser.password	= newUser.generateHash(password);
                newUser.firstaccess = false;
                newUser.firstaccessDate = +new Date;
                newUser.firstname	= req.body.firstname;
                newUser.lastname	= req.body.lastname;
                newUser.fullname	= req.body.firstname + " " + req.body.lastname;
                newUser.birthdate	= req.body.birthdate;

				// save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;

                    g_.main.mailer.sendMail({
                        from: "Show Me Fish <showmefish@gmail.com>",
                        to: newUser.firstname + " " + newUser.lastname + " <"+newUser.email+">",
                        subject: "Show Me Fish - Bem vindo " + newUser.firstname + " " + newUser.lastname,
                        text: "Hello world!!",
                        html: "<b>Hello</b> World!"
                    },function (error, response) {
                        if(error) {
                            console.log( "Error sending mail to " + newUser.email);
                            console.log(error);
                        } else {
                            console.log("Email sent to " + newUser.email);
                        }
                    });

                    g_.main.io.sockets.emit('users:added', {'added':true});
                    return done(null, newUser);
                });
            }

        });    

        });

    }));
 // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
	// we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

    g_.main.passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form
		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists

        console.log("here:"+email);

    	g_.main.db.models.user.findOne({ 'email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false); // req.flash is the way to set flashdata using connect-flash

			// if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });

    }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    g_.main.passport.use(new FacebookStrategy({

            // pull in our app id and secret from our smf-auth.js file
            clientID        : g_.main.config_auth.facebookAuth.clientID,
            clientSecret    : g_.main.config_auth.facebookAuth.clientSecret,
            callbackURL     : g_.main.config_auth.facebookAuth.callbackURL

        },

        // facebook will send back the token and profile
        function(token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // find the user in the database based on their facebook id
                g_.main.db.models.user.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        var changes = {};
                        changes.facebook = {};
                        changes.facebook.id = profile.id;
                        changes.facebook.token = token;
                        changes.facebook.name = profile.name.givenName;
                        changes.facebook.email = profile.emails[0].value;
                        changes.facebook.picture = "https://graph.facebook.com/"+profile.id+"/picture";
                        g_.main.db.models.user.update({"_id":user._id}, changes ,function(){
                            return done(null, user);
                        });
                    } else {

                        g_.main.db.models.user.findOne({'email':profile.emails[0].value}, function(err, user) {
                            if(err) {
                                res.writeHead(500);
                                res.write('Error accessing db');
                                res.end();
                            }
                            if(user) {

                                var changes = {};

                                if(!user.firstname && profile.name.givenName != "") {
                                    changes.firstname = profile.name.givenName;
                                    changes.fullname = profile.name.givenName + " ";
                                }
                                if(!user.lastname && profile.name.familyName != "") {
                                    changes.lastname = profile.name.familyName;
                                    if(!changes.fullname) changes.fullname = "";
                                    changes.fullname += profile.name.familyName;
                                }
                                if(!user.gender && profile.gender && profile.gender != "") {
                                    changes.gender = profile.gender;
                                }
                                changes.facebook = {};
                                changes.facebook.id = profile.id;
                                changes.facebook.token = token;
                                changes.facebook.name = profile.name.givenName;
                                changes.facebook.email = profile.emails[0].value;
                                changes.facebook.picture = "https://graph.facebook.com/"+profile.id+"/picture";

                                g_.main.db.models.user.update({"_id":user._id}, changes ,function(){
                                    g_.main.io.sockets.emit('users:updated', {'updated':true});
                                    return done(null, user);
                                });
                            }
                            else {
                                user                = new g_.main.db.models.user();
                                user.email		    = profile.emails[0].value;

                                user.password		= user.generateHash(process.hrtime());
                                user.firstaccess    = true;
                                user.firstname	    = profile.name.givenName;
                                user.lastname	    = profile.name.familyName;
                                user.fullname       = user.firstname + " " + user.lastname;
                                user.gender	        = profile.gender;
                                user.facebook.id    = profile.id; // set the users facebook id
                                user.facebook.token = token; // we will save the token that facebook provides to the user
                                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                                user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                                user.facebook.picture = "https://graph.facebook.com/"+profile.id+"/picture";

                                user.birthdate      = "";
                                user.interests      = [];
                                user.introduction   = "";

                                user.save(function(err) {
                                    if (err)
                                        throw err;
                                    g_.main.io.sockets.emit('users:updated', {'added':true});
                                    return done(null, user);
                                });
                            }
                        });
                    }

                });
            });

        }));

// =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    g_.main.passport.use(new GoogleStrategy({

            clientID        : g_.main.config_auth.googleAuth.clientID,
            clientSecret    : g_.main.config_auth.googleAuth.clientSecret,
            callbackURL     : g_.main.config_auth.googleAuth.callbackURL

        },
        function(token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function() {

                // try to find the user based on their google id
                g_.main.db.models.user.findOne({ 'google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        // if a user is found, log them in
                        var changes = {};
                        changes.google = {};
                        changes.google.id = profile.id;
                        changes.google.token = token;
                        changes.google.name = profile.name.givenName;
                        changes.google.email = profile.emails[0].value;
                        changes.google.picture = profile._json.picture;
                        g_.main.db.models.user.update({"_id":user._id}, changes ,function(err){
                            if(err) console.log(err);
                            return done(null, user);
                        });
                    } else {
                        g_.main.db.models.user.findOne({'email':profile.emails[0].value}, function(err, user) {
                            if(err) {
                                res.writeHead(500);
                                res.write('Error accessing db');
                                res.end();
                            }
                            if(user) {
                                var changes = {};

                                if(!user.firstname && profile.name.givenName != "") {
                                    changes.firstname = profile.name.givenName;
                                    changes.fullname = profile.name.givenName + " ";
                                }
                                if(!user.lastname && profile.name.familyName != "") {
                                    changes.lastname = profile.name.familyName;
                                    if(!changes.fullname) changes.fullname = "";
                                    changes.fullname += profile.name.familyName;
                                }
                                if(!user.gender && profile.gender && profile.gender != "") {
                                    changes.gender = profile.gender;
                                }
                                changes.google = {};
                                changes.google.id = profile.id;
                                changes.google.token = token;
                                changes.google.name = profile.name.givenName;
                                changes.google.email = profile.emails[0].value;
                                changes.google.picture = profile._json.picture;

                                g_.main.db.models.user.update({"_id":user._id}, changes ,function(response){
                                    g_.main.io.sockets.emit('users:updated', {'updated':true});
                                    return done(null, user);
                                });
                            }
                            else {
                                user                = new g_.main.db.models.user();
                                user.email		    = profile.emails[0].value;

                                user.password		= user.generateHash(process.hrtime());
                                user.firstaccess    = true;
                                user.firstname	    = profile.name.givenName;
                                user.lastname	    = profile.name.familyName;
                                user.fullname       = user.firstname + " " + user.lastname;
                                user.gender	        = profile.gender;
                                user.google.id    = profile.id; // set the users facebook id
                                user.google.token = token; // we will save the token that facebook provides to the user
                                user.google.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                                user.google.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                                user.google.picture = profile._json.picture;

                                user.birthdate      = "";
                                user.interests      = [];
                                user.introduction   = "";

                                user.save(function(err) {
                                    if (err)
                                        throw err;
                                    g_.main.io.sockets.emit('users:added', {'added':true});
                                    return done(null, user);
                                });
                            }
                        });
                    }
                });
            });

        }));
};