
var path = require('path')
var Globals = {
	main: {
	    app:null,
	    server:null,
	    io:null,
        mailer:null,
	    db:{
		    mongoose:null,
	    	handler:null,
	    	schemas:{},
	    	models:{}
	    },
	    functions:[
	    ]
	},
    public: {
        path: "/public/build/",
        imagePath: (process.env.OPENSHIFT_DATA_DIR || path.resolve("./images/") + "/")
    }
};    
module.exports = Globals;