var g_ = require(__dirname+'/smf-globals.js');
g_.main.db.mongoose = require('mongoose');

var mongodb_url = 'mongodb://localhost/showmefish';
if(process.env.OPENSHIFT_MONGODB_DB_HOST){
	mongodb_url = 'mongodb://'+process.env.OPENSHIFT_MONGODB_DB_USERNAME+':'+process.env.OPENSHIFT_MONGODB_DB_PASSWORD+'@'+process.env.OPENSHIFT_MONGODB_DB_HOST+':'+process.env.OPENSHIFT_MONGODB_DB_PORT+'/showmefish';
}
console.log("Connecting to mongodb with: " + mongodb_url);
g_.main.db.handler = g_.main.db.mongoose.connect(mongodb_url);

require(__dirname+'/models/user.js');
require(__dirname+'/models/userInterests.js');
require(__dirname+'/models/userReports.js');
require(__dirname+'/models/blog.js');
require(__dirname+'/models/classified.js');
require(__dirname+'/models/classifiedCategories.js');
require(__dirname+'/models/classifiedReports.js');
require(__dirname+'/models/image.js');
require(__dirname+'/models/imageReports.js');
require(__dirname+'/models/video.js');
require(__dirname+'/models/videoReports.js');
require(__dirname+'/models/post.js');
require(__dirname+'/models/postReports.js');
require(__dirname+'/models/group.js');
require(__dirname+'/models/chat.js');
require(__dirname+'/models/notification.js');