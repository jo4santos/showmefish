var g_ = require('../smf-globals.js');
g_.main.db.schemas.videoReports = new g_.main.db.mongoose.Schema({
    reporterId: String,
	videoId: String,
    timestamp: String,
    reason: String
});

