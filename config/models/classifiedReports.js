var g_ = require('../smf-globals.js');
g_.main.db.schemas.classifiedReports = new g_.main.db.mongoose.Schema({
	reporterId: String,
    classifiedId: String,
    timestamp: String,
    reason: String
});

