var g_ = require('../smf-globals.js');
g_.main.db.schemas.userReports = new g_.main.db.mongoose.Schema({
    reporterId: String,
	userId: String,
    timestamp: String,
    reason: String
});

