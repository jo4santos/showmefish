var g_ = require('../smf-globals.js');
g_.main.db.schemas.notification = new g_.main.db.mongoose.Schema({
    timestamp   : String,
    creatorId   : String,
    targetId    : String,
    postId      : String,
    type        : String
});
g_.main.db.schemas.notification.methods.add = function (userId, params, callback) {
    var element = new g_.main.db.models.notification();
    element.timestamp = +new Date;
    element.creatorId = userId;
    element.targetId = params.targetId;
    element.postId = params.postId;
    element.type = params.type;

    element.save(function(){
        g_.main.io.sockets.emit('notifications:updated', {'updated': true});
        callback(element._id);
    });
}
g_.main.db.schemas.notification.methods.remove = function (query, callback) {
    g_.main.db.models.notification.find(query).remove( function(){
        g_.main.io.sockets.emit('notifications:updated', {'updated': true});
        callback();
    });
}

g_.main.db.models.notification = g_.main.db.mongoose.model('notification', g_.main.db.schemas.notification);