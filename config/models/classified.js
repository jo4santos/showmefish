var g_ = require('../smf-globals.js');
g_.main.db.schemas.classified = new g_.main.db.mongoose.Schema({
	userId: String,
    type: String, /* sell,trade,buy */
    state: String, /* new,used */
    name: String,
    description: String,
    price: Number,
    timestamp: String,
    endDate: String,
    images: [String],
    category: String,
    location: String,
    likes: {
        count: Number,
        data: [String] // ToDo: Array of userIds (tmp)
    },
    views: {
        count: Number,
        data: [String] // ToDo: Array of userIds (tmp)
    }
});

