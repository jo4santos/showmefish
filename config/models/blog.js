var g_ = require('../smf-globals.js');
g_.main.db.schemas.blog = new g_.main.db.mongoose.Schema({
	userId: Number,
    url: String,
    name: String,
    description: String,
    image: String,
    theme: String,
    items: []
});

