var g_ = require('../smf-globals.js');
g_.main.db.schemas.postReports = new g_.main.db.mongoose.Schema({
    reporterId: String,
	postId: String,
    timestamp: String,
    reason: String
});

