var g_ = require('../smf-globals.js');
g_.main.db.schemas.classifiedCategories = new g_.main.db.mongoose.Schema({
    name: String,
    children: [{
        name:String
    }]
});

