var g_ = require('../smf-globals.js');
g_.main.db.schemas.image = new g_.main.db.mongoose.Schema({
	userId: String,
    title: String,
    timestamp: String,
    likes: {
        count: Number,
        data: [{
            userId: String,
            timestamp: String
        }]
    },
    views: {
        count: Number,
        data: [{
            userId: String,
            timestamp: String
        }]
    },
    comments: {
        count: Number,
        data: [{
            userId: String,
            comment: String,
            timestamp: String,
            likes: {
                count: Number,
                data: [{
                    userId: String,
                    timestamp: String
                }]
            }
        }]
    }
});
g_.main.db.schemas.image.methods.remove = function (type, param, callback) {
    var query = {};
    query[type] = param;
    g_.main.db.models.image.find(query).remove( callback );
}
g_.main.db.schemas.image.methods.addLike = function (elementId, userId, callback) {
    g_.main.db.models.image.update({"_id": elementId}, {
        $pull: {"likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.image.update({"_id": elementId}, {
            $push: {"likes.data": {"userId": userId, "timestamp": +new Date}}
        }, function () {
            g_.main.db.models.image.findOne({"_id": elementId}, function (err, doc) {
                g_.main.db.models.image.update({"_id": elementId}, {
                    "likes.count": doc.likes.data.length
                }, callback);
            });
        });
    });
}
g_.main.db.schemas.image.methods.removeLike = function (elementId, userId, callback) {
    g_.main.db.models.image.update({"_id": elementId}, {
        $pull: {"likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.image.findOne({"_id": elementId}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.image.update({"_id": elementId}, {
                    "likes.count": doc.likes.data.length
                }, callback);
            }
        });
    });
}
g_.main.db.schemas.image.methods.addLikeComment = function (commentId, userId, callback) {
    g_.main.db.models.image.update({"comments.data._id":commentId}, {
        $pull: {"comments.data.$.likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.image.update({"comments.data._id":commentId}, {
            $push: {"comments.data.$.likes.data": {"userId": userId, "timestamp": +new Date}}
        }, function () {
            g_.main.db.models.image.findOne({"comments.data._id":commentId}, {'comments.data.$': 1}, function (err, doc) {
                g_.main.db.models.image.update({"comments.data._id":commentId}, {
                    "comments.data.$.likes.count": doc.comments.data[0].likes.data.length
                }, callback);
            });
        });
    });
}
g_.main.db.schemas.image.methods.removeLikeComment = function (commentId, userId, callback) {
    g_.main.db.models.image.update({"comments.data._id":commentId}, {
        $pull: {"comments.data.$.likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.image.findOne({"comments.data._id":commentId}, {'comments.data.$': 1}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.image.update({"comments.data._id":commentId}, {
                    "comments.data.$.likes.count": doc.comments.data[0].likes.data.length
                }, callback);
            }
        });
    });
}
g_.main.db.schemas.image.methods.addComment = function (elementId, userId, comment, callback) {
    g_.main.db.models.image.update({"_id": elementId}, {
        $push: {"comments.data": {"userId": userId, "comment": comment, "timestamp": +new Date}}
    }, function () {
        g_.main.db.models.image.findOne({"_id": elementId}, function (err, doc) {
            g_.main.db.models.image.update({"_id": elementId}, {
                "comments.count": doc.comments.data.length
            }, callback);
        });
    });
}
g_.main.db.schemas.image.methods.removeComment = function (elementId, commentId, callback) {
    g_.main.db.models.image.update({"_id": elementId}, {
        $pull: {"comments.data": {"_id": commentId}}
    }, function () {
        g_.main.db.models.image.findOne({"_id": elementId}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.image.update({"_id": elementId}, {
                    "comments.count": doc.comments.data.length
                }, callback);
            }
        });
    });
}

g_.main.db.models.image = g_.main.db.mongoose.model('image', g_.main.db.schemas.image);