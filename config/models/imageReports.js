var g_ = require('../smf-globals.js');
g_.main.db.schemas.imageReports = new g_.main.db.mongoose.Schema({
    reporterId: String,
	imageId: String,
    timestamp: String,
    reason: String
});

