var g_ = require('../smf-globals.js');
g_.main.db.schemas.group = new g_.main.db.mongoose.Schema({
    timestamp   : String,
    creatorId   : String,
    name        : String,
    description : String,
    isPrivate   : Boolean,
    isSecret    : Boolean,
    interests   : [String],
    users       : [String],
    admins      : [String],
    requests    : [String]
});

g_.main.db.schemas.group.methods.add = function (userId, params, callback) {
    var element = new g_.main.db.models.group();
    element.timestamp = +new Date;
    element.creatorId = userId;
    element.name = params.name || "";
    element.description = params.description || "";
    element.isPrivate = params.isPrivate || false;
    element.isSecret = params.isSecret || false;
    element.interests = params.interests || [];
    element.users = params.users || [];
    element.users.push(userId);
    element.admins = params.admins || [];
    element.admins.push(userId);
    element.requests = [];

    element.save(callback);
}
g_.main.db.schemas.group.methods.remove = function (type, param, callback) {
    var query = {};
    query[type] = param;
    g_.main.db.models.group.find(query).remove( callback );
}

g_.main.db.models.group = g_.main.db.mongoose.model('group', g_.main.db.schemas.group);