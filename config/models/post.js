var g_ = require('../smf-globals.js');
g_.main.db.schemas.post = new g_.main.db.mongoose.Schema({
	userId: String,
    description: String,
    timestamp: String,
    images: [String],
    video: String,
    targetUserId: String,
    targetGroupId: String,
    likes: {
        count: Number,
        data: [{
            userId: String,
            timestamp: String
        }]
    },
    views: {
        count: Number,
        data: [{
            userId: String,
            timestamp: String
        }]
    },
    comments: {
        count: Number,
        data: [{
            userId: String,
            comment: String,
            timestamp: String,
            likes: {
                count: Number,
                data: [{
                    userId: String,
                    timestamp: String
                }]
            }
        }]
    }
});

g_.main.db.models.post = g_.main.db.mongoose.model('post', g_.main.db.schemas.post);