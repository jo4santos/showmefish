var g_ = require('../smf-globals.js');
g_.main.db.schemas.user = new g_.main.db.mongoose.Schema({
    image           :String,
    coverImage      :String,
	email           :String,
	password        :String,
    fullname        :String,
	firstname       :String,
    lastname        :String,
	birthdate       :String,
    gender          :String,
    role            :String,
    location        :String,
    introduction    :String,
    phonenumber     :Number,
    interests   : [String],
    facebook    : {
        id          : String,
        token       : String,
        email       : String,
        name        : String,
        picture     : String
    },
    google      : {
        id          : String,
        token       : String,
        email       : String,
        name        : String,
        picture     : String
    },
    last_login  :   {
        time        :Number,
        ip          :String
    },
    recovery    :   {
        time        :Number,
        hash        :String
    },
    status          :Number,    //  0: OK | 1: BLOCKED | 2: DISABLED
    firstaccess     :Boolean,
    firstaccessDate :Number,
    friends:  {
        confirmed   :[String],
        sent        :[String],
        received    :[String]
    },
    notifications:   {
        lastReadDate: Number
    }
});