var g_ = require('../smf-globals.js');
g_.main.db.schemas.chat = new g_.main.db.mongoose.Schema({
    timestamp   : String,
    creatorId   : String,
    name        : String,
    users       : [String],
    classifiedId: String,
    views: [{
        userId: String,
        timestamp: String
    }],
    messages: {
        count: Number,
        data: [{
            userId: String,
            content: String,
            timestamp: String
        }]
    }
});
g_.main.db.schemas.chat.methods.add = function (userId, params, callback) {
    var element = new g_.main.db.models.chat();
    element.timestamp = +new Date;
    element.creatorId = userId;
    element.name = params.name || "";
    element.classifiedId = params.classifiedId || "";
    element.users.push(userId);
    for(var i in params.users) {
        element.users.push(params.users[i]);
    }
    element.messages = {};
    element.messages.count = 0;
    element.messages.data = [];
    element.views = [];

    element.save(function(){
        callback(element._id);
    });
}
g_.main.db.schemas.chat.methods.remove = function (type, param, callback) {
    var query = {};
    query[type] = param;
    g_.main.db.models.chat.find(query).remove( callback );
}
g_.main.db.schemas.chat.methods.addMessage = function (elementId, userId, message, callback) {
    g_.main.db.models.chat.update({"_id": elementId}, {
        $push: {"messages.data": {"userId": userId, "content": message.content || "", "timestamp": +new Date}}
    }, function () {
        g_.main.db.models.chat.findOne({"_id": elementId}, function (err, doc) {
            g_.main.db.models.chat.update({"_id": elementId}, {
                "messages.count": doc.messages.data.length
            }, callback);
        });
    });
}
g_.main.db.schemas.chat.methods.setRead = function (elementId, userId, callback) {
    g_.main.db.models.chat.update({"_id": elementId}, {
        $pull: {"views": {"userId": userId}}
    }, function(){
        g_.main.db.models.chat.update({"_id": elementId}, {
            $push: {"views": {"userId": userId,  "timestamp": +new Date}}
        }, callback);
    });
}

g_.main.db.models.chat = g_.main.db.mongoose.model('chat', g_.main.db.schemas.chat);