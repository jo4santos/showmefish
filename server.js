// Global object to share in full project
var g_ = require(__dirname+'/config/smf-globals.js');

// Mail connection and handler
require(__dirname+'/config/smf-emails.js');

// DB schemas and connection
require(__dirname+'/config/smf-db.js');

var express			= require('express');
var bodyParser		= require('body-parser');
var cookieParser	= require('cookie-parser');
var morgan			= require('morgan');
var session			= require('express-session');
var url				= require('url');
var multipart       = require('connect-multiparty');

g_.main.app 		= express();
g_.main.server 		= require('http').Server(g_.main.app);
g_.main.io 			= require('socket.io')(g_.main.server);
g_.main.passport 	= require('passport');
g_.main.port 		= process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000;
g_.main.ip 			= process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";


//Authentication configuration
g_.main.config_auth = require(__dirname+'/config/smf-auth.js');
require(__dirname+'/config/smf-passport.js')();

//console.log("image path:",g_.public.imagePath);

g_.main.app.use(cookieParser());
g_.main.app.use(bodyParser.json());
g_.main.app.use(bodyParser.urlencoded({extended:true}));
g_.main.app.use(multipart({uploadDir: g_.public.imagePath}));

// Print every request to console
g_.main.app.use(morgan('dev'));

// Required for passport
g_.main.app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
g_.main.app.use(g_.main.passport.initialize());
g_.main.app.use(g_.main.passport.session()); // persistent login sessions

g_.main.app.use('/data/loggeduser', require('./routes/loggeduser'));
g_.main.app.use('/data/users', require('./routes/users'));
g_.main.app.use('/data/friends', require('./routes/friends'));
g_.main.app.use('/data/blogs', require('./routes/blogs'));
g_.main.app.use('/data/classified', require('./routes/classified'));
g_.main.app.use('/data/auth', require('./routes/auth'));
g_.main.app.use('/data/images', require('./routes/images'));
g_.main.app.use('/data/videos', require('./routes/videos'));
g_.main.app.use('/data/posts', require('./routes/posts'));
g_.main.app.use('/data/groups', require('./routes/groups'));
g_.main.app.use('/data/chats', require('./routes/chats'));
g_.main.app.use('/data/notifications', require('./routes/notifications'));

g_.main.app.get('/', function(req, res){
	if(req.user) {
        if(req.user.firstaccess === true) {
            res.sendFile(__dirname + g_.public.path + "static/firstaccess.html");
        }
        else {
            res.sendFile(__dirname + g_.public.path + "index.html");
        }
	}
	else{
		res.sendFile(__dirname + g_.public.path + "static/login.html");
	}
});
g_.main.app.get('/signup.html', function(req, res){
    if(req.user) {
        res.redirect('/');
    }
    else{
        res.sendFile(__dirname + g_.public.path + "static/signup.html");
    }
});
g_.main.app.get('/recover.html', function(req, res){
    if(req.user) {
        res.redirect('/');
    }
    else{
        res.sendFile(__dirname + g_.public.path + "static/recover.html");
    }
});
g_.main.app.get(['/index.html','/signup.html','/login.html'], function(req, res){
	res.redirect('/');
});

g_.main.app.get('/cdn/*', function(req, res){
	var file_url = __dirname + g_.public.path + url.parse(req.url).pathname;
	res.sendFile(file_url);
});

g_.main.app.get('*', g_.main.functions.isLoggedIn, function(req, res){
	var file_url = (url.parse(req.url).pathname == "/public/")?__dirname + g_.public.path + "index.html":__dirname + g_.public.path + url.parse(req.url).pathname;
	res.sendFile(file_url);
});

if(process.env.OPENSHIFT_NODEJS_IP) {
	g_.main.server.listen(g_.main.port,g_.main.ip);
}
else {
	g_.main.server.listen(g_.main.port);
}
console.log("Listening on "+g_.main.port);