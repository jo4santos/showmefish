var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();
var parser = require('rssparser');
var discover = require('rssdiscovery');
var schedule = require('node-schedule');

var rule = new schedule.RecurrenceRule();
rule.minute = 0;

/*var j = schedule.scheduleJob(rule, function(){
    g_.main.db.schemas.blog.methods.loadPosts(function(){
        g_.main.io.sockets.emit('blogs:updated', {'updated':true});
    });
});*/

router.get('/', function(req, res) {
    var findQuery = g_.main.db.models.blog.find();
    var filters = {};
    if(req.query && req.query.filters) {
        var filters = JSON.parse(req.query.filters) ;
    }

    if (filters.theme && filters.theme != "") {
        findQuery.where('theme').equals(filters.theme);
    }
    if (filters.userId && filters.userId != "") {
        if(filters.userId == "me") filters.userId = req.user._id;
        findQuery.where('userId').equals(filters.userId);
    }
    if (filters.text && filters.text != "") {
        findQuery.or([
            {"name": new RegExp(filters.text.toLowerCase(), "i")},
            {"description": new RegExp(filters.text.toLowerCase(), "i")}
        ]);
    }

    findQuery.exec(function (err, docs) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/refresh', function(req, res) {
    g_.main.db.schemas.blog.methods.loadPosts(function(){
        g_.main.io.sockets.emit('blogs:updated', {'updated':true});
    });
    res.send("OK")
});

router.get('/:param/:type?', function(req, res) {
	var type = req.params.type || "_id";
	var param = req.params.param;
	var query = {};
	query[type] = param;
	g_.main.db.models.blog.find(query, function(err, docs) {
		if(err) {
    		res.writeHead(500);
    		res.write('Error accessing db');
    		res.end();
		}
		res.send(docs);
	});
});

router.post('/', function(req, res) {
	var blog = req.body;
	discover(blog.url, function (err, results) {
        for(var i in results.links) {
            if(results.links[i].href && results.links[i].type.indexOf("rss") != -1) {

                //https://github.com/danmactough/node-feedparser
                parser.parseURL(results.links[i].href, {}, function(err, out){
                    blog.name = out.title || "";
                    blog.description = out.description || "";
                    blog.items = out.items || [];

                    g_.main.db.schemas.blog.methods.add(blog,function(){
                        g_.main.io.sockets.emit('blogs:added', {'added':true});
                    });
                });
            }
        }
    });

    res.send("OK");
});

router.put('/', function(req, res) {
	var blog = req.body;
	g_.main.db.schemas.blog.methods.update(blog,function(){
		g_.main.io.sockets.emit('blogs:updated', {'updated':true});
	});
	res.send("OK");
});

router.delete('/:param/:type?', function(req, res) {
	var type = req.params.type || "_id";
	var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.blog.find(query, function(err, docs) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if(docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.blog.methods.remove("_id",docs[0]._id,function(){
            g_.main.io.sockets.emit('blogs:removed', {'removed':true});
            res.send("OK");
        });
    });
});

module.exports = router;


//methods ======================
g_.main.db.schemas.blog.methods.add = function (params, callback) {
	var blog = new g_.main.db.models.blog();
	blog.userId = params.userId;
    blog.url = params.url;
    blog.name = params.name;
    blog.description = params.description;
    blog.image = params.image;
    blog.theme = params.theme;
    blog.items = params.items;

	blog.save(callback);
}
g_.main.db.schemas.blog.methods.update = function (blog, callback) {
    g_.main.db.schemas.blog.methods.remove("_id",blog._id,function() {
        delete blog._id;
        g_.main.db.schemas.blog.methods.add(blog,callback);
    })
}
g_.main.db.schemas.blog.methods.remove = function (type, param, callback) {
	var query = {};
	query[type] = param;
	g_.main.db.models.blog.find(query).remove( callback );
}

g_.main.db.schemas.blog.methods.loadPosts = function (callback) {
    g_.main.db.models.blog.find({}, function(err, blogs) {
        if(!err) {
            for(var i=0;i<blogs.length;i++) {
                var blog = blogs[i];
                discover(blog.url, function (err, results) {
                    for(var i in results.links) {
                        if(results.links[i].href && results.links[i].type.indexOf("rss") != -1) {

                            //https://github.com/danmactough/node-feedparser
                            parser.parseURL(results.links[i].href, {}, function(err, out){
                                blog.name = out.title || "";
                                blog.description = out.description || "";
                                blog.items = out.items || [];

                                g_.main.db.schemas.blog.methods.update(blog,callback);
                            });
                        }
                    }
                });
            }
        }
    });
}

g_.main.db.models.blog = g_.main.db.mongoose.model('blog', g_.main.db.schemas.blog);