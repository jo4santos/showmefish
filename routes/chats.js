var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.chat.find();

    var filters = {};
    if (req.query && req.query.filters) {
        filters = JSON.parse(req.query.filters);
    }

    if (filters.userId && filters.userId != "") {
        if (filters.userId == "me") filters.userId = req.user._id;
        findQuery.where('creatorId').equals(filters.userId);
    }

    findQuery.where('users').equals(req.user._id);

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/unread/:_id?', function (req, res) {
    var findQuery = g_.main.db.models.chat.find();

    if(req.params._id) {
        findQuery.where('_id').equals(req.params._id);
    }
    findQuery.where('users').equals(req.user._id);

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        var counter = 0;
        for(var i in docs) {
            var timestamp = 0;
            for(var j = 0; j < docs[i].views.length; j++) {
                if(docs[i].views[j].userId == req.user._id) {
                    timestamp = docs[i].views[j].timestamp;
                    continue;
                }
            }
            for(var k = 0; k < docs[i].messages.data.length; k++) {
                if(docs[i].messages.data[k].timestamp > timestamp) {
                    console.log(docs[i].messages.data[k].timestamp,timestamp);
                    counter++;
                }
            }
        }
        res.send(counter+"");
    });
});

router.delete('/unread/:_id', function (req, res) {
    g_.main.db.schemas.chat.methods.setRead(req.params._id, req.user._id, function () {
        g_.main.io.sockets.emit('chats:message:updated', {'updated': true});
        res.send("OK");
    });
});

router.get('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.chat.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/message/:chatId', function (req, res) {
    var message = req.body;
    var chatId = req.params.chatId;
    g_.main.db.schemas.chat.methods.addMessage(chatId, req.user._id, message, function () {
        g_.main.io.sockets.emit('chats:message:updated', {'updated': true});
        g_.main.io.sockets.emit('chats:message:added', {'added': true});
        g_.main.db.schemas.chat.methods.setRead(chatId, req.user._id, function () {
            g_.main.io.sockets.emit('chats:message:updated', {'updated': true});
        });
    });
    res.send("OK");
});

router.post('/users/:chatId', function (req, res) {
    var chatId = req.params.chatId;
    var users = req.body.users;

    g_.main.db.models.chat.findOne({"_id": chatId}, function (err, doc) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
            return;
        }

        var pushObj = {};
        pushObj.users = users;
        g_.main.db.models.chat.update({"_id": chatId}, {
            $pullAll: pushObj
        }, function () {
            g_.main.db.models.chat.update({"_id": chatId}, {
                $pushAll: pushObj
            }, function () {
                g_.main.io.sockets.emit('chats:user:added', {'added': true});
                res.send("OK");
            });
        });
    });
});

router.post('/', function (req, res) {
    var chat = req.body;
    g_.main.db.schemas.chat.methods.add(req.user._id, chat, function (id) {
        res.send(id);
        g_.main.io.sockets.emit('chats:added', {'added': true});
    });
});

router.put('/', function (req, res) {
    var chat = req.body;
    g_.main.db.schemas.chat.methods.update(chat, function () {
        g_.main.io.sockets.emit('chats:updated', {'updated': true});
    });
    res.send("OK");
});

router.delete('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.chat.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.chat.methods.remove("_id", docs[0]._id, function () {
            g_.main.io.sockets.emit('chats:removed', {'removed': true});
            res.send("OK");
        });
    });
});

module.exports = router;