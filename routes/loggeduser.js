var g_ = require('../config/smf-globals.js');
var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var router = express.Router();
var request = require('request');
var fs = require('fs');

router.get('/', function (req, res) {
    if (req.user) {
        res.send(req.user);
    }
    else {
        res.writeHead(403);
        res.write('Not logged in');
        res.end();
    }
});

router.put('/', function (req, res) {
    var user = req.body;
    if (!user._id || user._id == "" || user._id == "me") {
        user._id = req.user._id;
    }
    user.fullname = user.firstname + " " + user.lastname;

    if(user.password && user.password != "") {
        var newPassword = user.password;
        user.password = g_.main.db.schemas.user.methods.generateHash(newPassword);
    }

    g_.main.db.schemas.user.methods.update(user, function () {
        g_.main.io.sockets.emit('users:updated', {'updated': true});
    });
    res.send("OK");
});

// ToDo: We need to support multiple image extensions?
// Check if file exists

router.put('/profileImage', function (req, res) {
    var cropData = req.body;

    require('lwip').open(g_.public.imagePath + req.user.image + '.jpg', function (err, image) {

        var left = parseInt(cropData.x);
        var top = parseInt(cropData.y);
        var right = left + parseInt(cropData.width);
        var bottom = top + parseInt(cropData.height);

        image.batch()
            .crop(left, top, right, bottom)
            .resize(128)
            .writeFile(g_.public.imagePath + req.user.image + '.jpg', function (err) {
                res.send("OK");
            })

    });
});

router.put('/profileImage/social', function (req, res) {
    var src = req.body.src;
    var url = "";
    if (src == "google") {
        url = req.user.google.picture;
    }
    else if (src == "facebook") {
        url = req.user.facebook.picture;
    }
    else {
        res.writeHead(404);
        res.write('Source not found');
        res.end();
        return;
    }

    request
        .get(url)
        .on('response', function(response) {
            res.send("OK");
        })
        .on('error', function(response) {
            res.writeHead(500);
            res.write('Error downloading');
            res.end();
        })
        .pipe(fs.createWriteStream(g_.public.imagePath + req.user.image + ".jpg"))
});

module.exports = router;