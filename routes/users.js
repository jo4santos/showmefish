var g_ = require('../config/smf-globals.js');
var express = require('express');
var bcrypt   = require('bcrypt-nodejs');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.user.find();

    var filters = {};
    if(req.query && req.query.filters) {
        var filters = JSON.parse(req.query.filters) ;
    }

    if (filters.text && filters.text != "") {
        findQuery.or([
            {"fullname": new RegExp(filters.text)},
            {"firstname": new RegExp(filters.text)},
            {"lastname": new RegExp(filters.text)}
        ]);
    }

    if(filters.interests && filters.interests.length > 0) {
        findQuery.where("interests").in(filters.interests);
    }

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/images/:param/:type?', function(req, res) {
    var type = req.params.type || "userId";
    var param = req.params.param || "me";
    if(param == "me" && req.user) {
        param = req.user._id;
    }
    var query = {};
    query[type] = param;
    g_.main.db.models.image.find(query, function(err, docs) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/videos/:param/:type?', function(req, res) {
    var type = req.params.type || "userId";
    var param = req.params.param || "me";
    if(param == "me" && req.user) {
        param = req.user._id;
    }
    var query = {};
    query[type] = param;
    g_.main.db.models.video.find(query, function(err, docs) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});


router.get('/interests', function (req, res) {
    g_.main.db.models.userInterests.find({}, function (err, docs) {
        if (err) {
            console.log(err);
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/interests', function (req, res) {
    var interest = req.body;
    var element = new g_.main.db.models.userInterests();
    element.name = interest.name;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/interests/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.userInterests.find(query).remove(function () {
        res.send("OK");
    });
});
router.get('/interests/:_ids', function (req, res) {
    var elements = req.params._ids.toString();
    var validElements = [];
    elements = elements.split(",");
    for(var i in elements) {
        if(g_.main.db.mongoose.Types.ObjectId.isValid(elements[i])) {
            validElements.push(elements[i]);
        }
    }
    g_.main.db.models.userInterests.find({'_id': { $in: validElements}}, function (err, elements) {
        if (err) {
            console.log(err);
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(elements);
    });
});

router.put('/interests', function (req, res) {
    var interest = req.body;
    g_.main.db.models.userInterests.update({"_id": interest._id}, {
        "name": interest.name
    }, function () {
        res.send("OK");
    });
});

router.get('/reports', function (req, res) {
    g_.main.db.models.userReports.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});
router.post('/reports', function (req, res) {
    var params = req.body;
    var element = new g_.main.db.models.userReports();
    element.reporterId = req.user._id;
    element.timestamp = +new Date;
    element.reason = params.reason;
    element.userId = params.userId;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/reports/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.userReports.find(query).remove(function () {
        res.send("OK");
    });
});

router.get('/:param/:type?', function(req, res) {
	var type = req.params.type || "_id";
	var param = req.params.param || "me";
    if(param == "me" && req.user) {
        param = req.user._id;
    }
	var query = {};
	query[type] = param;
	g_.main.db.models.user.find(query, function(err, docs) {
		if(err) {
    		res.writeHead(500);
    		res.write('Error accessing db');
    		res.end();
		}
		res.send(docs);
	});
});

router.post('/', function(req, res) {
	var users = req.body;
	for(var i in users) {
		g_.main.db.schemas.user.methods.add(users[i],function(){
			g_.main.io.sockets.emit('users:added', {'added':true});
		});
	}
	res.send("OK");
});

router.put('/', function(req, res) {
	var user = req.body;
	g_.main.db.schemas.user.methods.update(user,function(){
		g_.main.io.sockets.emit('users:updated', {'updated':true});
	});
	res.send("OK");
});

router.delete('/:type/:param', function(req, res) {
	var type = req.params.type;
	var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.user.find(query, function(err, docs) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if(docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.user.methods.remove("_id",docs[0]._id,function(){
            g_.main.mailer.sendMail({
                from: "Show Me Fish <showmefish@gmail.com>",
                to: docs[0].firstname + " " + docs[0].lastname + " <"+docs[0].email+">",
                subject: "Show Me Fish - Utilizador removido",
                text: "O seu utilizador foi removido",
                html: "O seu utilizador foi removido"
            },function (error, response) {
                if(error) {
                    console.log( "Error sending mail to " + docs[0].email);
                    console.log(error);
                } else {
                    console.log("Email sent to " + docs[0].email);
                }
            });
            g_.main.io.sockets.emit('users:removed', {'removed':true});
            res.send("OK");
        });
    });
});

module.exports = router;


//methods ======================
//generating a hash
g_.main.db.schemas.user.methods.generateHash = function(password) {
return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//checking if password is valid
g_.main.db.schemas.user.methods.validPassword = function(password) {
return bcrypt.compareSync(password, this.password);
};

g_.main.db.schemas.user.methods.add = function (params, callback) {
	var user = new g_.main.db.models.user();
	user.username=params.username;
    user.fullname=params.firstname + " " + params.lastname;
	user.email=params.email;
	user.password=params.password;
    user.image=params.image || "";

	user.save(callback);
}
g_.main.db.schemas.user.methods.update = function (user, callback) {
    // TODO: Check fields

    var params = {};

    if(user.coverImage) params.coverImage = user.coverImage;
    if(user.image) params.image = user.image;
    if(user.username) params.username = user.username;
    if(user.firstname) params.firstname = user.firstname;
    if(user.lastname) params.lastname = user.lastname;
    if(user.email) params.email = user.email;
    if(user.password) params.password = user.password;
    if(user.firstaccess) params.firstaccess = user.firstaccess;
    if(user.firstaccessDate) params.firstaccessDate = user.firstaccessDate;
    if(user.interests) params.interests = user.interests;
    if(user.birthdate) params.birthdate = user.birthdate;
    if(user.introduction) params.introduction = user.introduction;

    if(user.firstname && user.lastname) {
        params.fullname=user.firstname + " " + user.lastname;
    }

	g_.main.db.models.user.update({"_id":user._id}, params, callback);
}
g_.main.db.schemas.user.methods.remove = function (type, param, callback) {
	var query = {};
	query[type] = param;
	g_.main.db.models.user.find(query).remove( callback );
}

g_.main.db.models.user = g_.main.db.mongoose.model('user', g_.main.db.schemas.user);
g_.main.db.models.userInterests = g_.main.db.mongoose.model('userInterests', g_.main.db.schemas.userInterests);
g_.main.db.models.userReports = g_.main.db.mongoose.model('userReports', g_.main.db.schemas.userReports);