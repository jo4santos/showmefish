var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();
var request = require('request');
var fs = require('fs');

router.post('/:userId', function (req, res) {
    var userId = req.params.userId;

    g_.main.db.models.user.update({"_id": userId}, {
        $push: {"friends.received": req.user._id}
    }, function () {
        g_.main.db.models.user.update({"_id": req.user._id}, {
            $push: {"friends.sent": userId}
        }, function () {
            res.send("OK");
        });
    });
});

router.delete('/:userId', function (req, res) {
    var userId = req.params.userId;

    g_.main.db.models.user.update({"_id": req.user._id}, {
        $pull: {"friends.confirmed": userId,"friends.sent": userId,"friends.received": userId}
    }, function () {
        g_.main.db.models.user.update({"_id": userId}, {
            $pull: {"friends.confirmed": req.user.id,"friends.sent": req.user._id,"friends.received": req.user._id}
        }, function () {
            res.send("OK");
        });
    });
});

router.post('/action/:userId', function (req, res) {
    var userId = req.params.userId;

    g_.main.db.models.user.update({"_id": req.user._id}, {
        $push: {"friends.confirmed": userId},
        $pull: {"friends.sent": userId,"friends.received": userId}
    }, function () {
        g_.main.db.models.user.update({"_id": userId}, {
            $push: {"friends.confirmed": req.user.id},
            $pull: {"friends.sent": req.user._id,"friends.received": req.user._id}
        }, function () {
            res.send("OK");
        });
    });
});

module.exports = router;