var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.group.find();

    var filters = {};
    if (req.query && req.query.filters) {
        filters = JSON.parse(req.query.filters);
    }

    if (filters.userId && filters.userId != "") {
        if(filters.userId == "me") filters.userId = req.user._id;
        findQuery.where('creatorId').equals(filters.userId);
    }

    if (filters.interests && filters.interests.length > 0) {
        findQuery.where("interests").in(filters.interests);
    }

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/admin/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.update({"_id": groupId}, {
        $push: {"admins": userId}
    }, function () {
        res.send("OK");
    });
});

router.delete('/admin/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.update({"_id": groupId}, {
        $pull: {"admins": userId}
    }, function () {
        res.send("OK");
    });
});

router.post('/subscription/action/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.findOne({"_id": groupId}, function (err, doc) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
            return;
        }
        if(doc.admins.indexOf(req.user._id) === -1) {
            res.writeHead(403);
            res.write('Forbidden');
            res.end();
            return;
        }
        var pushObj = {
            users: userId
        };
        var pullObj = {
            users: userId,
            requests: userId
        };
        g_.main.db.models.group.update({"_id": groupId}, {
            $pull: pullObj
        }, function () {
            g_.main.db.models.group.update({"_id": groupId}, {
                $push: pushObj
            }, function () {
                res.send("OK");
            });
        });
    });
});

router.delete('/subscription/action/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.findOne({"_id": groupId}, function (err, doc) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
            return;
        }
        if(doc.admins.indexOf(req.user._id) === -1) {
            res.writeHead(403);
            res.write('Forbidden');
            res.end();
            return;
        }
        var pullObj = {
            requests: userId
        };
        g_.main.db.models.group.update({"_id": groupId}, {
            $pull: pullObj
        }, function () {
            res.send("OK");
        });
    });
});

router.post('/subscription/:groupId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.body.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }
    var users = req.body.users;

    g_.main.db.models.group.findOne({"_id": groupId}, function (err, doc) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
            return;
        }

        var pushObj = {};
        if(doc.isSecret) {
            if(doc.admins.indexOf(req.user._id) === -1) {
                res.writeHead(403);
                res.write('User is not group admin');
                res.end();
                return;
            }
            else {
                pushObj.users = users;
            }
        }
        else if(doc.isPrivate) {
            if(doc.admins.indexOf(req.user._id) === -1) {
                pushObj.requests = users;
            }
            else {
                pushObj.users = users;
            }
        }
        else {
            pushObj.users = users;
        }
        g_.main.db.models.group.update({"_id": groupId}, {
            $pullAll: pushObj
        }, function () {
            g_.main.db.models.group.update({"_id": groupId}, {
                $pushAll: pushObj
            }, function () {
                res.send("OK");
            });
        });
    });
});

router.post('/subscription/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.findOne({"_id": groupId}, function (err, doc) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
            return;
        }
        var pushObj = {
            requests: userId
        };
        if(doc.isSecret) {
            console.log("secret");
            res.writeHead(403);
            res.write('Forbidden');
            res.end();
            return;
        }
        g_.main.db.models.group.update({"_id": groupId}, {
            $pull: pushObj
        }, function () {
            g_.main.db.models.group.update({"_id": groupId}, {
                $push: pushObj
            }, function () {
                res.send("OK");
            });
        });
    });
});

router.delete('/subscription/:groupId/:userId', function (req, res) {
    var groupId = req.params.groupId;
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.group.update({"_id": groupId}, {
        $pull: {"users": userId,"requests": userId,"admins": userId}
    }, function () {
        res.send("OK");
    });
});

router.get('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param || "me";
    if (param == "me" && req.user) {
        param = req.user._id;
    }
    var query = {};
    query[type] = param;
    g_.main.db.models.group.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/', function (req, res) {
    var group = req.body;
    g_.main.db.schemas.group.methods.add(req.user._id, group, function () {
        g_.main.io.sockets.emit('groups:added', {'added': true});
    });
    res.send("OK");
});

router.put('/', function (req, res) {
    var group = req.body;
    g_.main.db.schemas.group.methods.update(group, function () {
        g_.main.io.sockets.emit('groups:updated', {'updated': true});
    });
    res.send("OK");
});

router.delete('/:type/:param', function (req, res) {
    var type = req.params.type;
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.group.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.group.methods.remove("_id", docs[0]._id, function () {
            g_.main.io.sockets.emit('groups:removed', {'removed': true});
            res.send("OK");
        });
    });
});

module.exports = router;