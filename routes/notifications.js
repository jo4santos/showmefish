var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.notification.find();

    findQuery.where('targetId').equals(req.user._id);
    findQuery.where('creatorId').ne(req.user._id);

    findQuery.sort('-timestamp');

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/read', function (req, res) {
    g_.main.db.models.user.update({"_id":req.user._id}, {"notifications.lastReadDate": +new Date}, function() {
        g_.main.io.sockets.emit('notifications:updated', {'updated': true});
        res.send("OK");
    });
});

module.exports = router;