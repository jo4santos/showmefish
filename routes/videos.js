var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();
var path = require('path')
var fs = require('fs');

router.get('/', function (req, res) {
    g_.main.db.models.video.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/reports', function (req, res) {
    g_.main.db.models.videoReports.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/reports', function (req, res) {
    var params = req.body;
    var element = new g_.main.db.models.videoReports();
    element.reporterId = req.user._id;
    element.timestamp = +new Date;
    element.reason = params.reason;
    element.videoId = params.videoId;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/reports/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.videoReports.find(query).remove(function () {
        res.send("OK");
    });
});

router.post('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.video.methods.addLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.video.methods.removeLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/like/:elementId', function (req, res) {
    var elementId = req.params.elementId;

    g_.main.db.schemas.video.methods.addLike(elementId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/:elementId', function (req, res) {
    var elementId = req.params.elementId;

    g_.main.db.schemas.video.methods.removeLike(elementId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/comment/:elementId', function (req, res) {
    var elementId = req.params.elementId;
    var comment = req.body.comment;

    g_.main.db.schemas.video.methods.addComment(elementId, req.user._id, comment, function () {
        res.send("OK");
    });
});
router.delete('/comment/:elementId/:commentId', function (req, res) {
    var elementId = req.params.elementId;
    var commentId = req.params.commentId;

    g_.main.db.schemas.video.methods.removeComment(elementId, commentId, function () {
        res.send("OK");
    });
});

router.get('/:videos', function (req, res) {
    var videos = req.params.videos.toString();
    videos = videos.split(",");
    g_.main.db.models.video.find({'_id': { $in: videos}}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/', function (req, res) {
    var element = new g_.main.db.models.video();
    element.userId = req.user._id;
    element.title = req.body.title || "";
    element.url = req.body.url || "";
    element.timestamp = +new Date;

    element.save(function () {
        res.send(element._id);
    });

});

router.get('/:param/:type', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.video.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.delete('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.video.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.video.methods.remove("_id", docs[0]._id, function () {
            res.send("OK");
        });
    });
});



module.exports = router;

g_.main.db.models.videoReports = g_.main.db.mongoose.model('videoReports', g_.main.db.schemas.videoReports);