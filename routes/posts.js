var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.post.find();

    var filters = {};
    if (req.query && req.query.filters) {
        filters = JSON.parse(req.query.filters);
    }

    if (filters.userId && filters.userId != "") {
        if (filters.userId == "me") filters.userId = req.user._id;
    }

    if (filters.targetUserId && filters.targetUserId != "") {
        if (filters.targetUserId == "me") filters.targetUserId = req.user._id;
        findQuery.or([
            {"userId": filters.userId},
            {"targetUserId": filters.targetUserId}
        ]);
    }
    else {
        if (filters.userId && filters.userId != "") {
            findQuery.where('userId').equals(filters.userId);
        }
    }

    if (filters.targetGroupId && filters.targetGroupId != "") {
        if (filters.targetGroupId == "me") filters.targetGroupId = req.user._id;
        findQuery.where('targetGroupId').equals(filters.targetGroupId);
    }
    else {
        findQuery.where('targetGroupId').in([null, ""]);
    }

    var sort = {};
    if (req.query && req.query.sort) {
        sort = JSON.parse(req.query.sort);
    }
    if (!sort.field || sort.field == "") {
        sort.field = "timestamp";
    }
    if (!sort.direction || sort.direction == "") {
        sort.direction = "-1";
    }
    var sortOptions = {};
    sortOptions[sort.field] = sort.direction;
    findQuery.sort(sortOptions);

    findQuery.exec(function (err, docs) {

        console.log(docs);

        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.post.methods.addLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.post.methods.removeLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/like/:postId', function (req, res) {
    var postId = req.params.postId;

    g_.main.db.schemas.post.methods.addLike(postId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/:postId', function (req, res) {
    var postId = req.params.postId;

    g_.main.db.schemas.post.methods.removeLike(postId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/comment/:postId', function (req, res) {
    var postId = req.params.postId;
    var comment = req.body.comment;

    g_.main.db.schemas.post.methods.addComment(postId, req.user._id, comment, function () {
        res.send("OK");
    });
});
router.delete('/comment/:postId/:commentId', function (req, res) {
    var postId = req.params.postId;
    var commentId = req.params.commentId;

    g_.main.db.schemas.post.methods.removeComment(postId, commentId, function () {
        res.send("OK");
    });
});

router.get('/reports', function (req, res) {
    g_.main.db.models.postReports.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});
router.post('/reports', function (req, res) {
    var params = req.body;
    var element = new g_.main.db.models.postReports();
    element.reporterId = req.user._id;
    element.timestamp = +new Date;
    element.reason = params.reason;
    element.postId = params.postId;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/reports/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.postReports.find(query).remove(function () {
        res.send("OK");
    });
});

router.get('/:userId', function (req, res) {
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }
    var query = {};
    query["userId"] = userId;
    g_.main.db.models.classified.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/:param/:type', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.post.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/details/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.post.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/', function (req, res) {
    var post = req.body;
    post.userId = req.user._id;

    console.log(post);

    g_.main.db.schemas.post.methods.add(post, function () {
        g_.main.io.sockets.emit('post:added', {'added': true});
    });
    res.send("OK");
});

router.delete('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.post.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.post.methods.remove("_id", docs[0]._id, function () {
            g_.main.io.sockets.emit('post:removed', {'removed': true});
            res.send("OK");
        });
    });
});

module.exports = router;


//methods ======================
g_.main.db.schemas.post.methods.add = function (params, callback) {
    var element = new g_.main.db.models.post();
    element.userId = params.userId;
    element.targetUserId = params.targetUserId;
    element.targetGroupId = params.targetGroupId;
    element.description = params.description;
    element.video = params.video;
    element.images = params.images;
    element.timestamp = +new Date;
    element.views = {};
    element.views.count = 0;
    element.views.data = [];
    element.likes = {};
    element.likes.count = 0;
    element.likes.data = [];
    element.comments = {};
    element.comments.count = 0;
    element.comments.data = [];

    element.save(callback);
}
g_.main.db.schemas.post.methods.remove = function (type, param, callback) {
    var query = {};
    query[type] = param;
    g_.main.db.models.post.find(query).remove(callback);
}
g_.main.db.schemas.post.methods.addLike = function (elementId, userId, callback) {
    g_.main.db.models.post.update({"_id": elementId}, {
        $pull: {"likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.post.update({"_id": elementId}, {
            $push: {"likes.data": {"userId": userId, "timestamp": +new Date}}
        }, function () {
            g_.main.db.models.post.findOne({"_id": elementId}, function (err, doc) {
                g_.main.db.models.post.update({"_id": elementId}, {
                    "likes.count": doc.likes.data.length
                }, function () {
                    g_.main.db.schemas.notification.methods.add(userId, {
                        targetId: doc.userId,
                        postId: elementId,
                        type: "like"
                    }, callback);
                });
            });
        });
    });
}
g_.main.db.schemas.post.methods.removeLike = function (elementId, userId, callback) {
    g_.main.db.models.post.update({"_id": elementId}, {
        $pull: {"likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.post.findOne({"_id": elementId}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.post.update({"_id": elementId}, {
                    "likes.count": doc.likes.data.length
                }, function () {
                    g_.main.db.schemas.notification.methods.remove({
                        creatorId: userId,
                        postId: elementId,
                        type: "like"
                    }, callback);
                });
            }
        });
    });
}
g_.main.db.schemas.post.methods.addLikeComment = function (commentId, userId, callback) {
    g_.main.db.models.post.update({"comments.data._id": commentId}, {
        $pull: {"comments.data.$.likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.post.update({"comments.data._id": commentId}, {
            $push: {"comments.data.$.likes.data": {"userId": userId, "timestamp": +new Date}}
        }, function () {
            g_.main.db.models.post.findOne({"comments.data._id": commentId}, {'comments.data.$': 1}, function (err, doc) {
                g_.main.db.models.post.update({"comments.data._id": commentId}, {
                    "comments.data.$.likes.count": doc.comments.data[0].likes.data.length
                }, callback);
            });
        });
    });
}
g_.main.db.schemas.post.methods.removeLikeComment = function (commentId, userId, callback) {
    g_.main.db.models.post.update({"comments.data._id": commentId}, {
        $pull: {"comments.data.$.likes.data": {"userId": userId}}
    }, function () {
        g_.main.db.models.post.findOne({"comments.data._id": commentId}, {'comments.data.$': 1}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.post.update({"comments.data._id": commentId}, {
                    "comments.data.$.likes.count": doc.comments.data[0].likes.data.length
                }, callback);
            }
        });
    });
}
g_.main.db.schemas.post.methods.addComment = function (elementId, userId, comment, callback) {
    g_.main.db.models.post.update({"_id": elementId}, {
        $push: {"comments.data": {"userId": userId, "comment": comment, "timestamp": +new Date}}
    }, function () {
        g_.main.db.models.post.findOne({"_id": elementId}, function (err, doc) {
            g_.main.db.models.post.update({"_id": elementId}, {
                "comments.count": doc.comments.data.length
            }, function(){
                g_.main.db.schemas.notification.methods.add(userId, {
                    targetId: doc.userId,
                    postId: elementId,
                    type: "comment"
                }, callback);
            });
        });
    });
}
g_.main.db.schemas.post.methods.removeComment = function (elementId, commentId, callback) {
    g_.main.db.models.post.update({"_id": elementId}, {
        $pull: {"comments.data": {"_id": commentId}}
    }, function () {
        g_.main.db.models.post.findOne({"_id": elementId}, function (err, doc) {
            if (err) {
                console.log(err);
            }
            else if (!doc) {
                callback();
            }
            else {
                g_.main.db.models.post.update({"_id": elementId}, {
                    "comments.count": doc.comments.data.length
                }, callback);
            }
        });
    });
}
g_.main.db.models.post = g_.main.db.mongoose.model('post', g_.main.db.schemas.post);
g_.main.db.models.postReports = g_.main.db.mongoose.model('postReports', g_.main.db.schemas.postReports);