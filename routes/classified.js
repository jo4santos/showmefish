var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    var findQuery = g_.main.db.models.classified.find();
    var filters = {};
    if(req.query && req.query.filters) {
        var filters = JSON.parse(req.query.filters) ;
    }

    if (filters.type && filters.type != "") {
        findQuery.where('type').equals(filters.type);
    }
    if (filters.category && filters.category != "") {
        findQuery.where('category').equals(filters.category);
    }
    if (filters.state && filters.state != "") {
        findQuery.where('state').equals(filters.state);
    }
    if (filters.userId && filters.userId != "") {
        if(filters.userId == "me") filters.userId = req.user._id;
        findQuery.where('userId').equals(filters.userId);
    }
    if (filters.text && filters.text != "") {
        findQuery.or([
            {"name": new RegExp(filters.text.toLowerCase(), "i")},
            {"description": new RegExp(filters.text.toLowerCase(), "i")}
        ]);
    }
    if (filters.priceMin && filters.priceMin != "") {
        findQuery.where('price').gte(parseFloat(filters.priceMin));
    }
    if (filters.priceMax && filters.priceMax != "") {
        findQuery.where('price').lte(parseFloat(filters.priceMax));
    }

    if(req.query.sort) {
        var sort = JSON.parse(req.query.sort);
        if(!sort.field || sort.field == "") {
            sort.field = "_id";
        }
        if(!sort.direction || sort.direction == "") {
            sort.direction = "-1";
        }
        var sortOptions = {};
        sortOptions[sort.field] = sort.direction;
        findQuery.sort(sortOptions);
    }

    if(req.query.pagination) {
        var pagination = JSON.parse(req.query.pagination);
        pagination.limit = parseInt(pagination.limit);
        pagination.page = parseInt(pagination.page) - 1;
        if (!pagination.limit || pagination.limit < 1) {
            pagination.limit = 12;
        }
        findQuery.limit(pagination.limit);
        findQuery.skip(pagination.page * pagination.limit);
    }

    findQuery.exec(function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/categories', function (req, res) {
    g_.main.db.models.classifiedCategories.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.post('/categories', function (req, res) {
    var category = req.body;
    var parentId = category.parentId;
    var element = new g_.main.db.models.classifiedCategories();
    element.name = category.name;

    if(parentId) {
        g_.main.db.models.classifiedCategories.update({"_id": parentId}, {
            $push: {"children": element}
        }, function () {
            res.send("OK");
        });
    }
    else {
        element.save(function () {
            res.send("OK");
        });
    }
});

router.delete('/categories/:_id/:parentId?', function (req, res) {
    var parentId = req.params.parentId;

    console.log(req.params);

    if(parentId) {
        g_.main.db.models.classifiedCategories.update({"_id": parentId}, {
            $pull: {"children": {"_id": req.params._id}}
        }, function () {
            res.send("OK");
        });
    }
    else {
        g_.main.db.models.classifiedCategories.find({"_id":req.params._id}).remove(function () {
            res.send("OK");
        });
    }
});

router.put('/categories', function (req, res) {
    var category = req.body;
    g_.main.db.models.classifiedCategories.update({"_id": category._id}, {
        "name": category.name
    }, function () {
        g_.main.db.models.classifiedCategories.update(
            { "children._id": category._id },
            {
                "$set": {
                    "children.$": category
                }
            },
            function(err,doc) {
                res.send("OK");
            }
        );
    });
});

router.get('/reports', function (req, res) {
    g_.main.db.models.classifiedReports.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});
router.post('/reports', function (req, res) {
    var params = req.body;
    var element = new g_.main.db.models.classifiedReports();
    element.reporterId = req.user._id;
    element.timestamp = +new Date;
    element.reason = params.reason;
    element.classifiedId = params.classifiedId;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/reports/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.classifiedReports.find(query).remove(function () {
        res.send("OK");
    });
});

router.get('/:userId', function (req, res) {
    var userId = req.params.userId || "me";
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }
    var query = {};
    query["userId"] = userId;
    g_.main.db.models.classified.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/:param/:type', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.classified.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.get('/details/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.classified.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
        if (req.user) {
            g_.main.db.schemas.classified.methods.addView(param, req.user._id, function(){
                // Added
            });
        }
    });
});

router.post('/', function (req, res) {
    var classifieds = req.body;
    for (var i in classifieds) {
        classifieds[i].userId = req.user._id;
        g_.main.db.schemas.classified.methods.add(classifieds[i], function () {
            g_.main.io.sockets.emit('classified:added', {'added': true});
        });
    }
    res.send("OK");
});

router.put('/', function (req, res) {
    var classified = req.body;
    g_.main.db.schemas.classified.methods.update(classified, function () {
        g_.main.io.sockets.emit('classified:updated', {'updated': true});
    });
    res.send("OK");
});

router.delete('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.classified.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.classified.methods.remove("_id", docs[0]._id, function () {
            g_.main.io.sockets.emit('classified:removed', {'removed': true});
            res.send("OK");
        });
    });
});

module.exports = router;


//methods ======================
g_.main.db.schemas.classified.methods.add = function (params, callback) {
    var element = new g_.main.db.models.classified();
    element.userId = params.userId;
    element.name = params.name;
    element.description = params.description;
    element.images = params.images;
    element.category = params.category;

    /*
     ToDo: Improve the string to number conversion
     */
    element.price = parseFloat(params.price);
    element.location = params.location;
    element.state = params.state;
    element.type = params.type;
    element.timestamp = +new Date;
    element.endDate = params.endDate;
    element.views.count = 0;


    element.save(callback);
}
g_.main.db.schemas.classified.methods.update = function (element, callback) {
    // TODO: Check fields
    g_.main.db.models.classified.update({"_id": element._id}, {
        "name": element.name,
        "description": element.description,
        "image": element.image,
        "category": element.category
    }, callback);
}
g_.main.db.schemas.classified.methods.remove = function (type, param, callback) {
    var query = {};
    query[type] = param;
    g_.main.db.models.classified.find(query).remove(callback);
}
g_.main.db.schemas.classified.methods.addView = function (elementId, userId, callback) {
    g_.main.db.models.classified.update({"_id": elementId}, {
        $pull: {"views.data": userId}
    }, function () {
        g_.main.db.models.classified.update({"_id": elementId}, {
            $push: {"views.data": userId}
        }, function () {
            g_.main.db.models.classified.findOne({"_id": elementId}, function (err, doc) {
                g_.main.db.models.classified.update({"_id": elementId}, {
                    "views.count": doc.views.data.length
                }, callback);
            });
        });
    });
}

g_.main.db.models.classified = g_.main.db.mongoose.model('classified', g_.main.db.schemas.classified);
g_.main.db.models.classifiedCategories = g_.main.db.mongoose.model('classifiedCategories', g_.main.db.schemas.classifiedCategories);
g_.main.db.models.classifiedReports = g_.main.db.mongoose.model('classifiedReports', g_.main.db.schemas.classifiedReports);