var g_ = require('../config/smf-globals.js');
var express = require('express');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var router = express.Router();
var path = require('path')
var fs = require('fs');

router.get('/display/user/:userId', function(req, res){
    var userId = req.params.userId;
    if (userId == "me" && req.user) {
        userId = req.user._id;
    }

    g_.main.db.models.user.findOne({"_id":userId}, function(err, doc) {
        if(err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        if(!doc) {
            var file_url = path.resolve("./images/img-fallback.png");
            res.sendFile(file_url);
            return;
        }

        var filename = doc.image+".jpg";
        var file_url = g_.public.imagePath+filename;

        fs.exists(file_url, function(exists) {
            if (exists) {
                res.sendFile(file_url);
            }
            else {
                file_url = path.resolve("./images/img-fallback.png");
                res.sendFile(file_url);
            }
        });
    });
});

router.get('/display/:filename', function(req, res){
    var filename = req.params.filename;
    var file_url = g_.public.imagePath+filename;

    fs.exists(file_url, function(exists) {
        if (exists) {
            res.sendFile(file_url);
        }
        else {
            file_url = path.resolve("./images/img-fallback.png");
            res.sendFile(file_url);
        }
    });
});

router.get('/', function (req, res) {
    g_.main.db.models.image.find({}, function (err, images) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(images);
    });
});



router.get('/reports', function (req, res) {
    g_.main.db.models.imageReports.find({}, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});
router.post('/reports', function (req, res) {
    var params = req.body;
    var element = new g_.main.db.models.imageReports();
    element.reporterId = req.user._id;
    element.timestamp = +new Date;
    element.reason = params.reason;
    element.imageId = params.imageId;
    element.save(function () {
        res.send("OK");
    });
});

router.delete('/reports/:_id', function (req, res) {
    var query = {};
    query["_id"] = req.params._id;
    g_.main.db.models.imageReports.find(query).remove(function () {
        res.send("OK");
    });
});

router.post('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.image.methods.addLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/comment/:commentId', function (req, res) {
    g_.main.db.schemas.image.methods.removeLikeComment(req.params.commentId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/like/:elementId', function (req, res) {
    var elementId = req.params.elementId;

    g_.main.db.schemas.image.methods.addLike(elementId, req.user._id, function () {
        res.send("OK");
    });
});
router.delete('/like/:elementId', function (req, res) {
    var elementId = req.params.elementId;

    g_.main.db.schemas.image.methods.removeLike(elementId, req.user._id, function () {
        res.send("OK");
    });
});

router.post('/comment/:elementId', function (req, res) {
    var elementId = req.params.elementId;
    var comment = req.body.comment;

    g_.main.db.schemas.image.methods.addComment(elementId, req.user._id, comment, function () {
        res.send("OK");
    });
});
router.delete('/comment/:elementId/:commentId', function (req, res) {
    var elementId = req.params.elementId;
    var commentId = req.params.commentId;

    g_.main.db.schemas.image.methods.removeComment(elementId, commentId, function () {
        res.send("OK");
    });
});

router.get('/:images', function (req, res) {
    var images = req.params.images.toString();
    images = images.split(",");
    g_.main.db.models.image.find({'_id': { $in: images}}, function (err, images) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(images);
    });
});

router.post('/', multipartMiddleware, function (req, res) {
    var thumb = req.body.thumb || false;

    var element = new g_.main.db.models.image();
    element.userId = req.user._id;
    element.title = req.body.title || "";
    element.timestamp = +new Date;

    element.save(function () {

        require('lwip').open(req.files.file.path, function(err, image){
            var ratioMain = Math.min(1024 / image.width(), 768 / image.height());
            ratioMain = ratioMain < 1 ? ratioMain : 1;

            image.batch()
                .scale(ratioMain)
                .writeFile(g_.public.imagePath+element._id+path.extname(req.files.file.path).toLowerCase(), function(err){
                    if(thumb) {
                        var ratioThumb = Math.min(256 / image.width(), 192 / image.height());
                        ratioThumb = ratioThumb < 1 ? ratioThumb : 1;
                        image.batch()
                            .scale(ratioThumb)
                            .writeFile(g_.public.imagePath+element._id+"-thumb"+path.extname(req.files.file.path).toLowerCase(), function(err){
                                res.send(element._id);
                            })
                    }
                    else {
                        res.send(element._id);
                    }
                })

        });
    });

});

router.get('/:param/:type', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;
    var query = {};
    query[type] = param;
    g_.main.db.models.image.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            res.end();
        }
        res.send(docs);
    });
});

router.delete('/:param/:type?', function (req, res) {
    var type = req.params.type || "_id";
    var param = req.params.param;

    var query = {};
    query[type] = param;
    g_.main.db.models.image.find(query, function (err, docs) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (docs.length == 0) {
            res.writeHead(404);
            res.write('Not Found');
            return res.end();
        }
        g_.main.db.schemas.image.methods.remove("_id", docs[0]._id, function () {
            res.send("OK");
        });
    });
});



module.exports = router;

g_.main.db.models.imageReports = g_.main.db.mongoose.model('imageReports', g_.main.db.schemas.imageReports);