var g_ = require('../config/smf-globals.js');
var express = require('express');
var router = express.Router();

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
router.get('/signup', g_.main.functions.isLoggedIn, function (req, res) {
    res.redirect('/');
});
router.post('/signup', function (req, res, next) {
    g_.main.passport.authenticate('local-signup', function (err, user, info) {
        if (err) {
            return res.status(500).send('Error registering');
        }
        if (!user) {
            return res.status(403).send('Forbidden');
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).send('Error logging in');
            }

            // User logged in
            return res.redirect('/');
        });
    })(req, res, next);
});
router.get('/login', function (req, res) {
    res.redirect('/');
});
router.post('/recover', function (req, res) {
    g_.main.db.models.user.findOne({"email": req.body.email}, function (err, user) {
        if (err) {
            res.writeHead(500);
            res.write('Error accessing db');
            return res.end();
        }
        if (!user) {
            res.writeHead(404);
            res.write('Account with given email not found.');
            return res.end();
        }
        var new_password = process.hrtime();
        user.password = g_.main.db.schemas.user.methods.generateHash(new_password);
        g_.main.db.schemas.user.methods.update(user, function () {
            g_.main.mailer.sendMail({
                from: "Show Me Fish <showmefish@gmail.com>",
                to: user.firstname + " " + user.lastname + " <" + user.email + ">",
                subject: "Show Me Fish - Recuperação de password",
                text: "Nova password ou link de confirmação: " + new_password,
                html: "Nova password ou link de confirmação: " + new_password
            }, function (error, response) {
                if (error) {
                    console.log(error);
                    res.writeHead(500);
                    res.write('Error sending email');
                    return res.end();
                } else {
                    console.log("Email sent to " + user.email);

                    res.writeHead(200);
                    res.write('OK');
                    return res.end();
                }
            });
        });
    });
});
router.post('/firstaccess', function (req, res) {
    req.user.firstaccess = false;
    req.user.firstaccessDate = +new Date;
    req.user.introduction = "";
    if (req.body.birthdate) {
        req.user.birthdate = req.body.birthdate;
    }
    g_.main.db.models.user.update({"_id": req.user._id}, {
        "firstaccess": false,
        "firstaccessDate": +new Date,
        "birthdate": req.user.birthdate
    }, function(error) {
        if (error) {
            console.log(error);
            res.writeHead(500);
            res.write('Error applying configurations');
            return res.end();
        } else {
            res.writeHead(200);
            res.write('OK');
            return res.end();
        }
    });
});

router.post('/login', function (req, res, next) {
    g_.main.passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            return res.status(500).send('Error authenticating');
        }
        if (!user) {
            return res.status(403).send('Forbidden');
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).send('Error logging in');
            }

            // User logged in
            return res.redirect('/');
        });
    })(req, res, next);
});

// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login
router.get('/facebook', g_.main.passport.authenticate('facebook', { scope: 'email' }));

// handle the callback after facebook has authenticated the user
router.get('/facebook/callback',
    g_.main.passport.authenticate('facebook', {
        successRedirect: '/',
        failureRedirect: '/'
    }));

// =====================================
// GOOGLE ROUTES =======================
// =====================================
// send to google to do the authentication
// profile gets us their basic information including their name
// email gets their emails
router.get('/google', g_.main.passport.authenticate('google', { scope: ['profile', 'email'] }));

// the callback after google has authenticated the user
router.get('/google/callback',
    g_.main.passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/'
    }));

// facebook -------------------------------
router.get('/unlink/facebook', function (req, res) {
    g_.main.db.models.user.update({"_id": req.user._id}, {$unset: {"facebook.token": 1 }}, function (error) {
        if (error) {
            console.log(error);
            res.writeHead(500);
            res.write('Error');
            return res.end();
        }
        res.writeHead(200);
        res.write('OK');
        return res.end();
    });
});

// google ---------------------------------
router.get('/unlink/google', function (req, res) {
    g_.main.db.models.user.update({"_id": req.user._id}, {$unset: {"google.token": 1 }}, function (error) {
        if (error) {
            console.log(error);
            res.writeHead(500);
            res.write('Error');
            return res.end();
        }
        res.writeHead(200);
        res.write('OK');
        return res.end();
    });
});

module.exports = router;