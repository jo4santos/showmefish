# Show Me Fish

# Requisites

- Npm
- Gulp

# Openshift integration
 - git url

```
ssh://5454fcab4382ec63e100065e@showmefish-josapps.rhcloud.com/~/git/showmefish.git/
```

 - SSH connection
```
ssh 5454fcab4382ec63e100065e@showmefish-josapps.rhcloud.com
```

 - Clone into local folder
 - Add openshift as remote repository
 - Push to both repositories

# Compiling

To compile the project, run in your terminal:

```
gulp clean compile
```

If you are building a final/distribution version, run in your terminal:

```
gulp clean compile -d
```
