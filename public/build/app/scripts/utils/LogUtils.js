var smf;
(function (smf) {
    var utils;
    (function (utils) {
        var LogUtils = (function () {
            function LogUtils() {
            }
            LogUtils.format = function (data) {
                return "[" + new Date().toUTCString() + "] - " + data;
            };
            return LogUtils;
        })();
        utils.LogUtils = LogUtils;
    })(utils = smf.utils || (smf.utils = {}));
})(smf || (smf = {}));
