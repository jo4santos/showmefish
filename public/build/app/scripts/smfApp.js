"use strict";
var smf;
(function (smf) {
    var smfApp = (function () {
        function smfApp() {
            this.bootstrapAngular();
        }
        smfApp.prototype.bootstrapAngular = function () {
            var angularApp = angular.module("smfApp", ["ui.router", "ui.bootstrap", 'angularFileUpload', 'ngCropper', 'luegg.directives', "anguvideo"]);
            angularApp.config(function ($urlRouterProvider, $stateProvider, $locationProvider, $compileProvider, $controllerProvider) {
                $stateProvider.state("admin", {
                    url: "/admin",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/admin/index.html"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/admin/magic-bar.html",
                            controller: smf.controllers.MagicBarSocialController,
                            controllerAs: "MagicBarSocialController"
                        }
                    }
                }).state("admin.users", {
                    url: "/users",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/users/index.html"
                        }
                    }
                }).state("admin.images", {
                    url: "/images",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/images/index.html"
                        }
                    }
                }).state("admin.videos", {
                    url: "/videos",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/videos/index.html"
                        }
                    }
                }).state("admin.posts", {
                    url: "/posts",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/posts/index.html"
                        }
                    }
                }).state("admin.groups", {
                    url: "/groups",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/groups/index.html"
                        }
                    }
                }).state("admin.blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/blogs/index.html"
                        }
                    }
                }).state("admin.classified", {
                    url: "/classified",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/classified/index.html"
                        }
                    }
                });
                $stateProvider.state("social", {
                    url: "/social",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/timeline.html",
                            controller: smf.controllers.SocialTimelineController,
                            controllerAs: "SocialTimelineController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar.html",
                            controller: smf.controllers.MagicBarSocialController,
                            controllerAs: "MagicBarSocialController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/social/sidebar-right.html",
                            controller: smf.controllers.SocialSidebarRightController,
                            controllerAs: "SocialSidebarRightController"
                        }
                    }
                }).state("social.users", {
                    url: "/users",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/users.html",
                            controller: smf.controllers.SocialUsersController,
                            controllerAs: "SocialUsersController"
                        }
                    }
                }).state("social.notifications", {
                    url: "/notifications",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/notifications.html",
                            controller: smf.controllers.NotificationsController,
                            controllerAs: "NotificationsController"
                        }
                    }
                }).state("social.chats", {
                    url: "/chats",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/chats.html",
                            controller: smf.controllers.ChatsController,
                            controllerAs: "ChatsController"
                        }
                    }
                }).state("social.chats.details", {
                    url: "/:chatId",
                    views: {
                        "smf-view-chat-details": {
                            templateUrl: "/app/partials/common/chat-details.html",
                            controller: smf.controllers.ChatDetailsController,
                            controllerAs: "ChatDetailsController"
                        }
                    }
                }).state("social.groups", {
                    url: "/groups",
                    views: {
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar-groups.html",
                            controller: smf.controllers.MagicBarGroupsController,
                            controllerAs: "MagicBarGroupsController"
                        },
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/groups.html",
                            controller: smf.controllers.SocialGroupsController,
                            controllerAs: "SocialGroupsController"
                        }
                    }
                }).state("social.groups.details", {
                    url: "/:groupId",
                    views: {
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar-group-details.html",
                            controller: smf.controllers.MagicBarGroupDetailsController,
                            controllerAs: "MagicBarGroupDetailsController"
                        },
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/group-details.html",
                            controller: smf.controllers.SocialGroupDetailsController,
                            controllerAs: "SocialGroupDetailsController"
                        }
                    }
                }).state("social.profile", {
                    url: "/profile/{userId}",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/profile.html",
                            controller: smf.controllers.SocialProfileController,
                            controllerAs: "SocialProfileController"
                        }
                    }
                }).state("social.profile.timeline", {
                    url: "/timeline",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/timeline.html",
                            controller: smf.controllers.SocialTimelineController,
                            controllerAs: "SocialTimelineController"
                        }
                    }
                }).state("social.profile.blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/blogs/index.html",
                            controller: smf.controllers.BlogsController,
                            controllerAs: "BlogsController"
                        }
                    }
                }).state("social.profile.classified", {
                    url: "/classified",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/classified.html",
                            controller: smf.controllers.SocialClassifiedController,
                            controllerAs: "SocialClassifiedController"
                        }
                    }
                }).state("social.profile.about", {
                    url: "/about",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/about.html",
                            controller: smf.controllers.SocialProfileAboutController,
                            controllerAs: "SocialProfileAboutController"
                        }
                    }
                }).state("social.profile.images", {
                    url: "/images",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/images.html",
                            controller: smf.controllers.SocialProfileImagesController,
                            controllerAs: "SocialProfileImagesController"
                        }
                    }
                }).state("social.profile.videos", {
                    url: "/videos",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/videos.html",
                            controller: smf.controllers.SocialProfileVideosController,
                            controllerAs: "SocialProfileVideosController"
                        }
                    }
                }).state("social.profile.friends", {
                    url: "/friends",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/friends.html",
                            controller: smf.controllers.SocialProfileFriendsController,
                            controllerAs: "SocialProfileFriendsController"
                        }
                    }
                });
                $stateProvider.state("classified", {
                    url: "/classified/:type",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/classified/index.html",
                            controller: smf.controllers.ClassifiedController,
                            controllerAs: "ClassifiedController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/classified/magic-bar.html",
                            controller: smf.controllers.MagicBarClassifiedController,
                            controllerAs: "MagicBarClassifiedController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/classified/sidebar-right.html"
                        }
                    }
                }).state("classified.details", {
                    url: "/details/{_id}",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/classified/details.html",
                            controller: smf.controllers.ClassifiedDetailsController,
                            controllerAs: "ClassifiedDetailsController"
                        }
                    }
                });
                $stateProvider.state("blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/blogs/index.html",
                            controller: smf.controllers.BlogsController,
                            controllerAs: "BlogsController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/blogs/magic-bar.html",
                            controller: smf.controllers.MagicBarBlogsController,
                            controllerAs: "MagicBarBlogsController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/blogs/sidebar-right.html"
                        }
                    }
                });
                $stateProvider.state("settings", {
                    url: "/settings",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/settings.html",
                            controller: smf.controllers.SettingsController,
                            controllerAs: "SettingsController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/common/settings-magic-bar.html",
                            controller: smf.controllers.SettingsMagicBarController,
                            controllerAs: "SettingsMagicBarController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/common/settings-sidebar-right.html"
                        }
                    }
                });
                $urlRouterProvider.otherwise("/social");
                $controllerProvider.register("controllers.SocialUsersController", smf.controllers.SocialUsersController);
                $controllerProvider.register("controllers.SocialPostController", smf.controllers.SocialPostController);
                $controllerProvider.register("controllers.SocialUserController", smf.controllers.SocialUserController);
                $controllerProvider.register("controllers.SocialCommentController", smf.controllers.SocialCommentController);
                $controllerProvider.register("controllers.SocialTimelineController", smf.controllers.SocialTimelineController);
                $controllerProvider.register("controllers.SocialProfileAboutController", smf.controllers.SocialProfileAboutController);
                $controllerProvider.register("controllers.SocialProfileImagesController", smf.controllers.SocialProfileImagesController);
                $controllerProvider.register("controllers.SocialProfileFriendsController", smf.controllers.SocialProfileFriendsController);
                $controllerProvider.register("controllers.SocialShareController", smf.controllers.SocialShareController);
                $controllerProvider.register("controllers.SocialPostReportController", smf.controllers.SocialPostReportController);
                $controllerProvider.register("controllers.SocialGroupCreateController", smf.controllers.SocialGroupCreateController);
                $controllerProvider.register("controllers.SocialGroupsController", smf.controllers.SocialGroupsController);
                $controllerProvider.register("controllers.SettingsController", smf.controllers.SettingsController);
                $controllerProvider.register("controllers.SettingsMagicBarController", smf.controllers.SettingsMagicBarController);
                $controllerProvider.register("controllers.BlogsController", smf.controllers.BlogsController);
                $controllerProvider.register("controllers.BlogsShareController", smf.controllers.BlogsShareController);
                $controllerProvider.register("controllers.ClassifiedController", smf.controllers.ClassifiedController);
                $controllerProvider.register("controllers.ClassifiedShareController", smf.controllers.ClassifiedShareController);
                $controllerProvider.register("controllers.ClassifiedReportController", smf.controllers.ClassifiedReportController);
                $controllerProvider.register("controllers.CommonController", smf.controllers.CommonController);
                $controllerProvider.register("controllers.ChatsController", smf.controllers.ChatsController);
                $controllerProvider.register("controllers.ChatDetailsController", smf.controllers.ChatDetailsController);
                $controllerProvider.register("controllers.NavigationController", smf.controllers.NavigationController);
                $controllerProvider.register("controllers.ProfileChangeImageController", smf.controllers.ProfileChangeImageController);
                $controllerProvider.register("controllers.UserReportController", smf.controllers.UserReportController);
                $controllerProvider.register("controllers.ImageReportController", smf.controllers.ImageReportController);
                $controllerProvider.register("controllers.ImageViewerController", smf.controllers.ImageViewerController);
                $controllerProvider.register("controllers.VideoReportController", smf.controllers.VideoReportController);
                $controllerProvider.register("controllers.VideoViewerController", smf.controllers.VideoViewerController);
                $controllerProvider.register("controllers.PostViewerController", smf.controllers.PostViewerController);
                $controllerProvider.register("controllers.ModalSelectUsersController", smf.controllers.ModalSelectUsersController);
                $controllerProvider.register("controllers.AdminClassifiedListController", smf.controllers.AdminClassifiedListController);
                $controllerProvider.register("controllers.AdminClassifiedCategoriesController", smf.controllers.AdminClassifiedCategoriesController);
                $controllerProvider.register("controllers.AdminClassifiedReportsController", smf.controllers.AdminClassifiedReportsController);
                $controllerProvider.register("controllers.AdminUserListController", smf.controllers.AdminUserListController);
                $controllerProvider.register("controllers.AdminUserInterestsController", smf.controllers.AdminUserInterestsController);
                $controllerProvider.register("controllers.AdminUserReportsController", smf.controllers.AdminUserReportsController);
                $controllerProvider.register("controllers.AdminImagesReportsController", smf.controllers.AdminImagesReportsController);
                $controllerProvider.register("controllers.AdminVideosReportsController", smf.controllers.AdminVideosReportsController);
                $controllerProvider.register("controllers.AdminPostsReportsController", smf.controllers.AdminPostsReportsController);
                $controllerProvider.register("controllers.AdminGroupListController", smf.controllers.AdminGroupListController);
            });
            angularApp.directive('smfLoading', function () {
                return {
                    restrict: 'E',
                    templateUrl: './app/partials/common/directives/loading.html',
                    transclude: true
                };
            });
            angularApp.directive('smfUserLink', function () {
                return {
                    restrict: 'E',
                    templateUrl: './app/partials/common/directives/user-link.html',
                    scope: { userId: '=userId' },
                    controller: smf.controllers.UserLinkController,
                    controllerAs: "UserLinkController"
                };
            });
            angularApp.directive('smfSidebarRightPost', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/common/directives/sidebar-right-post.html'
                };
            });
            angularApp.directive('smfSocialPost', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/post.html',
                    scope: { post: '=post' },
                    controller: smf.controllers.SocialPostController,
                    controllerAs: "SocialPostController"
                };
            });
            angularApp.directive('smfSocialUser', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/user.html',
                    scope: { userId: '=userId', actions: '=actions' },
                    controller: smf.controllers.SocialUserController,
                    controllerAs: "SocialUserController"
                };
            });
            angularApp.directive('smfSocialGroup', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/group.html',
                    scope: { groupId: '=groupId' },
                    controller: smf.controllers.SocialGroupController,
                    controllerAs: "SocialGroupController"
                };
            });
            angularApp.directive('smfSocialComment', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/comment.html',
                    scope: { comment: '=comment', remove: '&', toggleLike: '&' },
                    controller: smf.controllers.SocialCommentController,
                    controllerAs: "SocialCommentController"
                };
            });
            angularApp.directive('smfBlogPost', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/blogs/directives/post.html',
                    scope: { post: '=post', blog: '=blog' }
                };
            });
            angularApp.directive('smfClassifiedPost', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/classified/directives/post.html',
                    scope: { classified: '=classified' }
                };
            });
            angularApp.directive('smfClassifiedThumbnail', function () {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/classified/directives/thumbnail.html',
                    scope: { classifiedId: '=classifiedId' },
                    controller: smf.controllers.ClassifiedThumbnailController,
                    controllerAs: "ClassifiedThumbnailController"
                };
            });
            angularApp.directive('smfImageThumbnail', function ($modal) {
                return {
                    restrict: 'A',
                    scope: false,
                    link: function (scope, elem, attrs) {
                        elem.bind('click', function () {
                            var modalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-image-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.ImageViewerController as ImageViewerController";
                            modalSettings.resolve = {
                                imageId: function () {
                                    return attrs.imageId;
                                }
                            };
                            $modal.open(modalSettings);
                        });
                    }
                };
            });
            angularApp.directive('smfPostThumbnail', function ($modal) {
                return {
                    restrict: 'A',
                    scope: false,
                    link: function (scope, elem, attrs) {
                        elem.bind('click', function () {
                            var modalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-post-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.PostViewerController as PostViewerController";
                            modalSettings.resolve = {
                                postId: function () {
                                    return attrs.postId;
                                }
                            };
                            $modal.open(modalSettings);
                        });
                    }
                };
            });
            angularApp.directive('smfVideoLink', function ($modal) {
                return {
                    restrict: 'A',
                    scope: false,
                    link: function (scope, elem, attrs) {
                        elem.bind('click', function () {
                            var modalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-video-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.VideoViewerController as VideoViewerController";
                            modalSettings.resolve = {
                                videoId: function () {
                                    return attrs.videoId;
                                }
                            };
                            $modal.open(modalSettings);
                        });
                    }
                };
            });
            angularApp.directive('scrollPosition', function ($window) {
                return {
                    scope: {
                        scroll: '=scrollPosition'
                    },
                    link: function (scope, element, attrs) {
                        var windowEl = angular.element($window);
                        var handler = function () {
                            scope.scroll = windowEl.scrollTop();
                        };
                        windowEl.on('scroll', scope.$apply.bind(scope, handler));
                        handler();
                    }
                };
            });
            angularApp.filter('bytes', function () {
                return function (bytes, precision) {
                    if (isNaN(parseFloat(bytes)) || !isFinite(bytes))
                        return '-';
                    if (typeof precision === 'undefined')
                        precision = 1;
                    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
                    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
                };
            });
            angularApp.filter('reverse', function () {
                return function (items) {
                    return items.slice().reverse();
                };
            });
            angularApp.provider("LoggedUserService", function () {
                var LoggedUserService;
                this.$get = function ($q, $http) {
                    if (LoggedUserService == null) {
                        LoggedUserService = new smf.services.LoggedUserService($q, $http);
                    }
                    return LoggedUserService;
                };
            });
            angularApp.provider("UsersService", function () {
                var UsersService;
                this.$get = function ($q, $http) {
                    if (UsersService == null) {
                        UsersService = new smf.services.UsersService($q, $http);
                    }
                    return UsersService;
                };
            });
            angularApp.provider("FriendService", function () {
                var FriendService;
                this.$get = function ($q, $http, UsersService, LoggedUserService, $modal) {
                    if (FriendService == null) {
                        FriendService = new smf.services.FriendService($q, $http, UsersService, LoggedUserService, $modal);
                    }
                    return FriendService;
                };
            });
            angularApp.provider("ImageService", function () {
                var ImageService;
                this.$get = function ($q, $http, UsersService, LoggedUserService) {
                    if (ImageService == null) {
                        ImageService = new smf.services.ImageService($q, $http, UsersService, LoggedUserService);
                    }
                    return ImageService;
                };
            });
            angularApp.provider("VideoService", function () {
                var VideoService;
                this.$get = function ($q, $http, UsersService, LoggedUserService) {
                    if (VideoService == null) {
                        VideoService = new smf.services.VideoService($q, $http, UsersService, LoggedUserService);
                    }
                    return VideoService;
                };
            });
            angularApp.provider("ChatService", function () {
                var ChatService;
                this.$get = function ($q, $http, UsersService, LoggedUserService) {
                    if (ChatService == null) {
                        ChatService = new smf.services.ChatService($q, $http, UsersService, LoggedUserService);
                    }
                    return ChatService;
                };
            });
            angularApp.provider("PostService", function () {
                var PostService;
                this.$get = function ($q, $http, UsersService, LoggedUserService) {
                    if (PostService == null) {
                        PostService = new smf.services.PostService($q, $http, UsersService, LoggedUserService);
                    }
                    return PostService;
                };
            });
            angularApp.provider("GroupService", function () {
                var GroupService;
                this.$get = function ($q, $http, UsersService, LoggedUserService) {
                    if (GroupService == null) {
                        GroupService = new smf.services.GroupService($q, $http, UsersService, LoggedUserService);
                    }
                    return GroupService;
                };
            });
            angularApp.provider("BlogsService", function () {
                var BlogsService;
                this.$get = function ($q, $http, UsersService) {
                    if (BlogsService == null) {
                        BlogsService = new smf.services.BlogsService($q, $http, UsersService);
                    }
                    return BlogsService;
                };
            });
            angularApp.provider("ClassifiedService", function () {
                var ClassifiedService;
                this.$get = function ($q, $http) {
                    if (ClassifiedService == null) {
                        ClassifiedService = new smf.services.ClassifiedService($q, $http);
                    }
                    return ClassifiedService;
                };
            });
            angularApp.provider("NavigationService", function () {
                var NavigationService;
                this.$get = function ($q, $http) {
                    if (NavigationService == null) {
                        NavigationService = new smf.services.NavigationService($q, $http);
                    }
                    return NavigationService;
                };
            });
            angularApp.provider("SocketIOFactory", function () {
                var SocketIOFactory;
                this.$get = function ($rootScope) {
                    if (SocketIOFactory == null) {
                        SocketIOFactory = new smf.services.SocketIOFactory($rootScope);
                    }
                    return SocketIOFactory;
                };
            });
            angularApp.provider("NotificationService", function () {
                var NotificationService;
                this.$get = function ($http, $q, SocketIOFactory, LoggedUserService) {
                    if (NotificationService == null) {
                        NotificationService = new smf.services.NotificationService($http, $q, SocketIOFactory, LoggedUserService);
                    }
                    return NotificationService;
                };
            });
            angularApp.run(function ($state, $rootScope, LoggedUserService) {
                $rootScope.scroll = 0;
                LoggedUserService.load();
            });
        };
        return smfApp;
    })();
    smf.smfApp = smfApp;
})(smf || (smf = {}));
new smf.smfApp();
console.debug("[" + new Date().toUTCString() + "] - smfApp has just started!");
