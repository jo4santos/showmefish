"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var BlogsShareController = (function () {
            function BlogsShareController($modalInstance, BlogsService) {
                var _this = this;
                this.$modalInstance = $modalInstance;
                this.BlogsService = BlogsService;
                this.blog = new smf.models.Blog();
                this.BlogsService.getThemes()
                    .then(function (themes) {
                    _this.themes = themes;
                });
                console.debug(smf.utils.LogUtils.format("BlogsShareController initialized"));
            }
            BlogsShareController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            BlogsShareController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.blog.theme = this.blog.themeObj._id;
                    this.BlogsService.create(this.blog).then(function () {
                        _this.$modalInstance.dismiss();
                        alert("BLOG REGISTADO COM SUCESSO");
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return BlogsShareController;
        })();
        controllers.BlogsShareController = BlogsShareController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
