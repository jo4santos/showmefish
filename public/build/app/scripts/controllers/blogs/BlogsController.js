"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var BlogsController = (function () {
            function BlogsController(BlogsService, SocketIOFactory) {
                var _this = this;
                this.blogs = [];
                this.themes = [];
                this.items = [];
                this.BlogsService = BlogsService;
                this.SocketIOFactory = SocketIOFactory;
                this.getList();
                this.SocketIOFactory.on('blogs:added', function () {
                    _this.getList();
                });
                this.SocketIOFactory.on('blogs:updated', function () {
                    _this.getList();
                });
                this.SocketIOFactory.on('blogs:removed', function () {
                    _this.getList();
                });
                this.BlogsService.getThemes()
                    .then(function (themes) {
                    _this.themes = themes;
                });
                console.debug(smf.utils.LogUtils.format("BlogsController initialized"));
            }
            BlogsController.prototype.getList = function () {
                var _this = this;
                this.items = [];
                this.BlogsService.getList(this.filters).then(function (blogs) {
                    _this.blogs = blogs;
                    _this.blogs.forEach(function (blog) {
                        var item = blog.items[0];
                        item.blog = {};
                        item.blog.name = blog.name;
                        item.blog.description = blog.description;
                        item.blog.url = blog.url;
                        _this.items.push(item);
                    });
                });
            };
            BlogsController.prototype.remove = function (type, param) {
                if (confirm("Remove blog?")) {
                    this.BlogsService.remove(type, param).then(function () {
                        console.log("done");
                    });
                }
            };
            BlogsController.prototype.applyFilters = function () {
                this.filters.theme = "";
                if (this.filters.themeObj) {
                    this.filters.theme = this.filters.themeObj._id;
                }
                this.getList();
            };
            return BlogsController;
        })();
        controllers.BlogsController = BlogsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
