"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var MagicBarBlogsController = (function () {
            // Public attributes available to view
            function MagicBarBlogsController($modal, BlogsService) {
                this.$modal = $modal;
                this.BlogsService = BlogsService;
                console.debug(smf.utils.LogUtils.format("MagicBarBlogsController initialized"));
            }
            MagicBarBlogsController.prototype.updatePosts = function () {
                this.BlogsService.refreshPosts();
            };
            MagicBarBlogsController.prototype.share = function () {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/blogs/modal-share.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.BlogsShareController as BlogsShareController";
                this.$modal.open(modalSettings);
            };
            return MagicBarBlogsController;
        })();
        controllers.MagicBarBlogsController = MagicBarBlogsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
