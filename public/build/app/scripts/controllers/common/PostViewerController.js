"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var PostViewerController = (function () {
            function PostViewerController($modal, $modalInstance, PostService, postId) {
                this.$modal = $modal;
                this.$modalInstance = $modalInstance;
                this.PostService = PostService;
                this.post = new smf.models.Post();
                this.post._id = postId;
                this.load();
                console.debug(smf.utils.LogUtils.format("PostViewerController initialized"));
            }
            PostViewerController.prototype.load = function () {
                var _this = this;
                this.PostService.getDetails("_id", this.post._id).then(function (post) {
                    _this.post = post;
                });
            };
            PostViewerController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            return PostViewerController;
        })();
        controllers.PostViewerController = PostViewerController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
