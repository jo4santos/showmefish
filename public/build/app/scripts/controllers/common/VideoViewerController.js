"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var VideoViewerController = (function () {
            function VideoViewerController($scope, $modal, $modalInstance, VideoService, UsersService, LoggedUserService, videoId) {
                var _this = this;
                this.$scope = $scope;
                this.$modal = $modal;
                this.$modalInstance = $modalInstance;
                this.VideoService = VideoService;
                this.LoggedUserService = LoggedUserService;
                this.UsersService = UsersService;
                this.video = new smf.models.Video();
                this.video._id = videoId;
                this.loggedUser = new smf.models.LoggedUser();
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                });
                this.load();
                console.debug(smf.utils.LogUtils.format("VideoViewerController initialized"));
            }
            VideoViewerController.prototype.load = function () {
                var _this = this;
                this.comment = "";
                this.VideoService.getDetails("_id", this.video._id).then(function (video) {
                    _this.video = video;
                    _this.VideoService.enhance(_this.video);
                    for (var i in _this.video.comments.data) {
                        _this.UsersService.getDetails("_id", _this.video.comments.data[i].userId).then(function (user) {
                            for (var j in _this.video.comments.data) {
                                if (_this.video.comments.data[j].userId == user[0]._id) {
                                    _this.video.comments.data[j].user = user[0];
                                }
                            }
                        });
                    }
                });
            };
            VideoViewerController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            VideoViewerController.prototype.submit = function () {
                var _this = this;
                this.VideoService.addComment(this.video._id, this.comment).then(function () {
                    _this.load();
                });
            };
            VideoViewerController.prototype.toggleLike = function () {
                var _this = this;
                if (this.video.likes.userLiked) {
                    this.VideoService.removeLike(this.video._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.VideoService.addLike(this.video._id).then(function () {
                        _this.load();
                    });
                }
            };
            VideoViewerController.prototype.report = function () {
                var _this = this;
                //todo
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-report-video.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.VideoReportController as VideoReportController";
                modalSettings.resolve = {
                    videoId: function () {
                        return _this.video._id;
                    }
                };
                this.$modal.open(modalSettings);
            };
            VideoViewerController.prototype.removeComment = function (comment) {
                var _this = this;
                if (confirm("Remove comment?")) {
                    this.VideoService.removeComment(this.video._id, comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            VideoViewerController.prototype.toggleLikeComment = function (comment) {
                var _this = this;
                if (comment.likes.userLiked) {
                    this.VideoService.removeLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.VideoService.addLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            return VideoViewerController;
        })();
        controllers.VideoViewerController = VideoViewerController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
