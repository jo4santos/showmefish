"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var UserLinkController = (function () {
            function UserLinkController($scope, UsersService) {
                this.$scope = $scope;
                this.UsersService = UsersService;
                this.user = new smf.models.User();
                this.user._id = this.$scope.userId;
                this.load();
                console.debug(smf.utils.LogUtils.format("UserLinkController initialized"));
            }
            UserLinkController.prototype.load = function () {
                var _this = this;
                this.UsersService.getDetails("_id", this.user._id).then(function (user) {
                    _this.user = user[0];
                });
            };
            return UserLinkController;
        })();
        controllers.UserLinkController = UserLinkController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
