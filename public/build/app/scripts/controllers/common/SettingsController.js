"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SettingsController = (function () {
            function SettingsController($scope, LoggedUserService, UsersService) {
                var _this = this;
                this.$scope = $scope;
                this.LoggedUserService = LoggedUserService;
                this.UsersService = UsersService;
                this.newPassword = "";
                this.UsersService.getInterests(true).then(function (interests) {
                    _this.interests = interests;
                    _this.selectedInterests = [];
                    for (var i in _this.interests) {
                        _this.selectedInterests[_this.interests[i]._id] = false;
                    }
                    _this.LoggedUserService.getUser(true).then(function (user) {
                        _this.user = user;
                        for (var i in _this.user.interests) {
                            _this.selectedInterests[_this.user.interests[i]] = true;
                        }
                    });
                });
                console.debug(smf.utils.LogUtils.format("SettingsController initialized"));
            }
            SettingsController.prototype.submit = function () {
                if (!this.form.$invalid) {
                    this.user.interests = [];
                    for (var i in this.selectedInterests) {
                        if (this.selectedInterests[i] == true) {
                            this.user.interests.push(angular.copy(i));
                        }
                    }
                    if (this.newPassword != "") {
                        this.user.password = angular.copy(this.newPassword);
                    }
                    this.LoggedUserService.save(this.user).then(function () {
                        location.reload();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            SettingsController.prototype.unlinkGoogle = function () {
                this.LoggedUserService.unlinkGoogle().then(function () {
                    location.reload();
                });
            };
            SettingsController.prototype.unlinkFacebook = function () {
                this.LoggedUserService.unlinkFacebook().then(function () {
                    location.reload();
                });
            };
            return SettingsController;
        })();
        controllers.SettingsController = SettingsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
