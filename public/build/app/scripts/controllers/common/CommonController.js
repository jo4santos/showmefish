"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var CommonController = (function () {
            /**
             *
             * @param userApi
             */
            function CommonController($scope, $modal, SocketIOFactory, UsersService, ChatService, LoggedUserService, NotificationService) {
                var _this = this;
                this.$scope = $scope;
                this.SocketIOFactory = SocketIOFactory;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.$modal = $modal;
                this.ChatService = ChatService;
                this.NotificationService = NotificationService;
                this.loadUser();
                this.loadMessagesUnread();
                this.loadNotifications();
                this.SocketIOFactory.on('chats:message:updated', function () {
                    _this.loadMessagesUnread();
                });
                this.SocketIOFactory.on('notifications:updated', function () {
                    _this.loadNotifications();
                });
                console.debug(smf.utils.LogUtils.format("CommonController initialized"));
            }
            CommonController.prototype.loadUser = function () {
                var _this = this;
                this.LoggedUserService.getUser().then(function (user) {
                    _this.user = user;
                    _this.UsersService.enhance(_this.user);
                });
            };
            CommonController.prototype.loadMessagesUnread = function () {
                var _this = this;
                this.ChatService.getUnread().then(function (unread) {
                    _this.messagesUnread = unread;
                });
            };
            CommonController.prototype.loadNotifications = function () {
                var _this = this;
                this.NotificationService.getAll().then(function (notifications) {
                    _this.notifications = notifications;
                    _this.notificationsUnread = _this.notifications.filter(function (notification) {
                        return !notification.read;
                    }).length;
                });
            };
            CommonController.prototype.setNotificationsRead = function () {
                this.NotificationService.setRead();
            };
            CommonController.prototype.changeProfileImage = function () {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-profile-change-image.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.ProfileChangeImageController as ProfileChangeImageController";
                this.$modal.open(modalSettings);
            };
            CommonController.prototype.reportClassified = function (classifiedId) {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/classified/modal-report.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.ClassifiedReportController as ClassifiedReportController";
                modalSettings.resolve = {
                    classifiedId: function () {
                        return classifiedId;
                    }
                };
                this.$modal.open(modalSettings);
            };
            CommonController.prototype.reportUser = function (userId) {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-report-user.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.UserReportController as UserReportController";
                modalSettings.resolve = {
                    userId: function () {
                        return userId;
                    }
                };
                this.$modal.open(modalSettings);
            };
            CommonController.prototype.reportImage = function (imageId) {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-report-image.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.ImageReportController as ImageReportController";
                modalSettings.resolve = {
                    imageId: function () {
                        return imageId;
                    }
                };
                this.$modal.open(modalSettings);
            };
            return CommonController;
        })();
        controllers.CommonController = CommonController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
