"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var UserReportController = (function () {
            function UserReportController($scope, $modalInstance, SocketIOFactory, UsersService, userId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.SocketIOFactory = SocketIOFactory;
                this.UsersService = UsersService;
                this.userId = userId;
                this.user = new smf.models.User();
                this.reason = "";
                console.debug(smf.utils.LogUtils.format("UserReportController initialized"));
            }
            UserReportController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            UserReportController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.UsersService.addReport(this.userId, this.reason).then(function () {
                        _this.$modalInstance.dismiss();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return UserReportController;
        })();
        controllers.UserReportController = UserReportController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
