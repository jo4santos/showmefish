"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var NavigationController = (function () {
            /**
             *
             * @param userApi
             */
            function NavigationController($scope, $location, NavigationService, SocketIOFactory) {
                // Public attributes available to view
                this.pages = new Array();
                this.$scope = $scope;
                this.$location = $location;
                this.SocketIOFactory = SocketIOFactory;
                this.NavigationService = NavigationService;
                this.pages = [];
                this.getList();
                console.debug(smf.utils.LogUtils.format("NavigationController initialized"));
            }
            //
            // Private
            //
            NavigationController.prototype.getList = function () {
                var _this = this;
                this.NavigationService.getList().then(function (pages) {
                    _this.pages = pages;
                });
            };
            return NavigationController;
        })();
        controllers.NavigationController = NavigationController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
