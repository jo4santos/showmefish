"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminImagesReportsController = (function () {
            function AdminImagesReportsController($scope, ImageService) {
                this.$scope = $scope;
                this.ImageService = ImageService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminImagesReportsController initialized"));
            }
            AdminImagesReportsController.prototype.getList = function () {
                var _this = this;
                this.ImageService.getReports().then(function (reports) {
                    _this.reports = reports;
                });
            };
            AdminImagesReportsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove report?")) {
                    this.ImageService.removeReport(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminImagesReportsController;
        })();
        controllers.AdminImagesReportsController = AdminImagesReportsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
