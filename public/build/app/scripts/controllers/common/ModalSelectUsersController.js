"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ModalSelectUsersController = (function () {
            function ModalSelectUsersController($scope, $modalInstance, DataService, title, description, excluded) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.DataService = DataService;
                this.title = title;
                this.description = description;
                this.excluded = excluded;
                this.selectedUsers = [];
                this.getList();
                console.debug(smf.utils.LogUtils.format("ModalSelectUsersController initialized"));
            }
            ModalSelectUsersController.prototype.getList = function () {
                var _this = this;
                this.DataService.getAll(this.filters).then(function (users) {
                    _this.users = [];
                    for (var i in users) {
                        if (_this.excluded.indexOf(users[i]._id) === -1) {
                            _this.users.push(users[i]);
                        }
                    }
                });
            };
            ModalSelectUsersController.prototype.selectUser = function (user) {
                this.selectedUsers.push(user);
            };
            ModalSelectUsersController.prototype.removeUser = function (user) {
                for (var i in this.selectedUsers) {
                    if (this.selectedUsers[i]._id == user._id) {
                        this.selectedUsers.splice(i, 1);
                        return;
                    }
                }
            };
            ModalSelectUsersController.prototype.isSelected = function (user) {
                for (var i in this.selectedUsers) {
                    if (user._id == this.selectedUsers[i]._id)
                        return true;
                }
                return false;
            };
            ModalSelectUsersController.prototype.confirm = function () {
                this.$modalInstance.close(this.selectedUsers);
            };
            ModalSelectUsersController.prototype.cancel = function () {
                this.$modalInstance.dismiss('cancel');
            };
            return ModalSelectUsersController;
        })();
        controllers.ModalSelectUsersController = ModalSelectUsersController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
