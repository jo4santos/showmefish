"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ImageReportController = (function () {
            function ImageReportController($scope, $modalInstance, SocketIOFactory, ImageService, imageId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.SocketIOFactory = SocketIOFactory;
                this.ImageService = ImageService;
                this.imageId = imageId;
                this.image = new smf.models.Image();
                this.reason = "";
                console.debug(smf.utils.LogUtils.format("ImageReportController initialized"));
            }
            ImageReportController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            ImageReportController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.ImageService.addReport(this.imageId, this.reason).then(function () {
                        _this.$modalInstance.dismiss();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return ImageReportController;
        })();
        controllers.ImageReportController = ImageReportController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
