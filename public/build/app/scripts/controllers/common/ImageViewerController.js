"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ImageViewerController = (function () {
            function ImageViewerController($scope, $modal, $modalInstance, ImageService, UsersService, LoggedUserService, imageId) {
                var _this = this;
                this.$scope = $scope;
                this.$modal = $modal;
                this.$modalInstance = $modalInstance;
                this.ImageService = ImageService;
                this.LoggedUserService = LoggedUserService;
                this.UsersService = UsersService;
                this.image = new smf.models.Image();
                this.image._id = imageId;
                this.loggedUser = new smf.models.LoggedUser();
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                });
                this.load();
                console.debug(smf.utils.LogUtils.format("ImageViewerController initialized"));
            }
            ImageViewerController.prototype.load = function () {
                var _this = this;
                this.comment = "";
                this.ImageService.getDetails("_id", this.image._id).then(function (image) {
                    _this.image = image;
                    _this.ImageService.enhance(_this.image);
                    for (var i in _this.image.comments.data) {
                        _this.UsersService.getDetails("_id", _this.image.comments.data[i].userId).then(function (user) {
                            for (var j in _this.image.comments.data) {
                                if (_this.image.comments.data[j].userId == user[0]._id) {
                                    _this.image.comments.data[j].user = user[0];
                                }
                            }
                        });
                    }
                });
            };
            ImageViewerController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            ImageViewerController.prototype.submit = function () {
                var _this = this;
                this.ImageService.addComment(this.image._id, this.comment).then(function () {
                    _this.load();
                });
            };
            ImageViewerController.prototype.toggleLike = function () {
                var _this = this;
                if (this.image.likes.userLiked) {
                    this.ImageService.removeLike(this.image._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.ImageService.addLike(this.image._id).then(function () {
                        _this.load();
                    });
                }
            };
            ImageViewerController.prototype.report = function () {
                var _this = this;
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-report-image.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.ImageReportController as ImageReportController";
                modalSettings.resolve = {
                    imageId: function () {
                        return _this.image._id;
                    }
                };
                this.$modal.open(modalSettings);
            };
            ImageViewerController.prototype.setCover = function () {
                if (confirm("Escolher como imagem de capa?")) {
                    this.loggedUser.coverImage = this.image._id;
                    this.LoggedUserService.save(this.loggedUser).then(function () {
                        location.reload();
                    });
                }
            };
            ImageViewerController.prototype.removeComment = function (comment) {
                var _this = this;
                if (confirm("Remove comment?")) {
                    this.ImageService.removeComment(this.image._id, comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            ImageViewerController.prototype.toggleLikeComment = function (comment) {
                var _this = this;
                if (comment.likes.userLiked) {
                    this.ImageService.removeLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.ImageService.addLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            return ImageViewerController;
        })();
        controllers.ImageViewerController = ImageViewerController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
