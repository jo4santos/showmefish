"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SettingsMagicBarController = (function () {
            // Public attributes available to view
            function SettingsMagicBarController($scope) {
                this.$scope = $scope;
                console.debug(smf.utils.LogUtils.format("SettingsMagicBarController initialized"));
            }
            return SettingsMagicBarController;
        })();
        controllers.SettingsMagicBarController = SettingsMagicBarController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
