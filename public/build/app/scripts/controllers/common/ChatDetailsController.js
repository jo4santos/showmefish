"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ChatDetailsController = (function () {
            function ChatDetailsController($scope, ChatService, FriendService, SocketIOFactory, $stateParams) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.ChatService = ChatService;
                this.FriendService = FriendService;
                this.SocketIOFactory = SocketIOFactory;
                this.chat = new smf.models.Chat();
                this.chat._id = this.$stateParams.chatId;
                this.content = "";
                this.load();
                this.setListeners();
                this.ChatService.setRead(this.chat._id);
                console.debug(smf.utils.LogUtils.format("ChatDetailsController initialized"));
            }
            ChatDetailsController.prototype.setListeners = function () {
                var _this = this;
                this.SocketIOFactory.on('chats:message:added', function () {
                    _this.load();
                });
                this.SocketIOFactory.on('chats:user:added', function () {
                    _this.load();
                });
                this.SocketIOFactory.on('chats:removed', function () {
                    _this.load();
                });
            };
            ChatDetailsController.prototype.load = function () {
                var _this = this;
                this.ChatService.getDetails("_id", this.chat._id).then(function (chat) {
                    _this.chat = chat[0];
                });
            };
            ChatDetailsController.prototype.send = function () {
                var _this = this;
                this.ChatService.sendMessage(this.chat._id, this.content).then(function () {
                    _this.content = "";
                    _this.load();
                });
            };
            ChatDetailsController.prototype.addUsers = function () {
                var _this = this;
                this.FriendService.select(this.chat.users).then(function (users) {
                    _this.ChatService.addUsers(_this.chat._id, users).then(function () {
                        _this.load();
                    });
                });
            };
            return ChatDetailsController;
        })();
        controllers.ChatDetailsController = ChatDetailsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
