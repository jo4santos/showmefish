"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ChatsController = (function () {
            function ChatsController($scope, ChatService, SocketIOFactory) {
                this.$scope = $scope;
                this.ChatService = ChatService;
                this.SocketIOFactory = SocketIOFactory;
                this.chats = [];
                this.getList();
                this.setListeners();
                console.debug(smf.utils.LogUtils.format("ChatsController initialized"));
            }
            ChatsController.prototype.setListeners = function () {
                var _this = this;
                this.SocketIOFactory.on('chats:added', function () {
                    _this.getList();
                });
                this.SocketIOFactory.on('chats:removed', function () {
                    _this.getList();
                });
                this.SocketIOFactory.on('chats:user:added', function () {
                    _this.getList();
                });
                this.SocketIOFactory.on('chats:message:updated', function () {
                    _this.getList();
                });
            };
            ChatsController.prototype.getList = function () {
                var _this = this;
                this.ChatService.getAll(this.filters)
                    .then(function (chats) {
                    _this.chats = chats;
                    for (var i in _this.chats) {
                        _this.ChatService.enhance(_this.chats[i]);
                    }
                });
            };
            ChatsController.prototype.createChat = function () {
                var _this = this;
                var name = prompt("Nome do chat?");
                if (name) {
                    var chat = new smf.models.Chat();
                    chat.name = name;
                    this.ChatService.create(chat).then(function () {
                        _this.getList();
                    });
                }
            };
            ChatsController.prototype.removeChat = function (chatId) {
                var _this = this;
                if (confirm("Apagar chat? Todos os conteúdos serão removidos do servidor." + chatId)) {
                    this.ChatService.remove("_id", chatId).then(function () {
                        _this.getList();
                    });
                }
            };
            return ChatsController;
        })();
        controllers.ChatsController = ChatsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
