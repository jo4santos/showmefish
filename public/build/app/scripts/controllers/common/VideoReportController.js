"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var VideoReportController = (function () {
            function VideoReportController($scope, $modalInstance, SocketIOFactory, VideoService, videoId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.SocketIOFactory = SocketIOFactory;
                this.VideoService = VideoService;
                this.videoId = videoId;
                this.video = new smf.models.Video();
                this.reason = "";
                console.debug(smf.utils.LogUtils.format("VideoReportController initialized"));
            }
            VideoReportController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            VideoReportController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.VideoService.addReport(this.videoId, this.reason).then(function () {
                        _this.$modalInstance.dismiss();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return VideoReportController;
        })();
        controllers.VideoReportController = VideoReportController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
