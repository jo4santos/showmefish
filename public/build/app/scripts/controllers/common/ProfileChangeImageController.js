"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ProfileChangeImageController = (function () {
            function ProfileChangeImageController($scope, $timeout, $modalInstance, SocketIOFactory, $upload, LoggedUserService, Cropper) {
                var _this = this;
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.$timeout = $timeout;
                this.$upload = $upload;
                this.SocketIOFactory = SocketIOFactory;
                this.LoggedUserService = LoggedUserService;
                this.Cropper = Cropper;
                this.files = [];
                this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
                this.options = {
                    maximize: true,
                    aspectRatio: 1 / 1,
                    done: function (cropData) {
                        _this.cropData = cropData;
                    }
                };
                this.loadUser();
                console.debug(smf.utils.LogUtils.format("ProfileChangeImageController initialized"));
            }
            ProfileChangeImageController.prototype.loadUser = function () {
                var _this = this;
                this.LoggedUserService.getUser(true).then(function (user) {
                    _this.user = user;
                });
            };
            ProfileChangeImageController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            ProfileChangeImageController.prototype.generateThumbs = function () {
                if (this.files.length > 0) {
                    for (var i = 0; i < this.files.length; i++) {
                        if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL(this.files[i]);
                            fileReader.onload = (function (file) {
                                return function (e) {
                                    file.dataUrl = e.target.result;
                                };
                            })(this.files[i]);
                        }
                    }
                }
            };
            ProfileChangeImageController.prototype.upload = function (file) {
                var _this = this;
                if (!file)
                    return;
                this.files.upload = this.$upload.upload({
                    url: "/data/images",
                    file: file,
                    fields: { title: file.title }
                }).progress(function (evt) {
                    console.log('progress: ' + _this.filesProgress + '% ' + evt.config.file.name);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == evt.config.file.name) {
                            _this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                            break;
                        }
                    }
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == config.file.name) {
                            _this.files[i].progress = 100;
                            _this.files[i].id = data;
                            _this.user.image = _this.files[i].id;
                            _this.$timeout((function (ctrl) {
                                return function () {
                                    ctrl.showCropper();
                                };
                            })(_this));
                            break;
                        }
                    }
                });
            };
            ProfileChangeImageController.prototype.cancelUpload = function (file) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == file.name) {
                        if (this.files[i].upload) {
                            this.files[i].upload.cancel();
                        }
                        else {
                            this.files.splice(i, 1);
                        }
                        break;
                    }
                }
            };
            ProfileChangeImageController.prototype.submit = function () {
                var _this = this;
                var user = new smf.models.User();
                user.image = this.files[0].id;
                this.LoggedUserService.save(user).then(function () {
                    _this.LoggedUserService.setProfileImage(_this.cropData).then(function (response) {
                        _this.loadUser();
                        location.reload();
                    });
                });
            };
            ProfileChangeImageController.prototype.showCropper = function () {
                this.$scope.$broadcast("showCrop");
                this.cropEnabled = true;
            };
            ProfileChangeImageController.prototype.setSocial = function (src) {
                var _this = this;
                this.LoggedUserService.setProfileImageSocial(src).then(function (response) {
                    _this.loadUser();
                    location.reload();
                });
            };
            return ProfileChangeImageController;
        })();
        controllers.ProfileChangeImageController = ProfileChangeImageController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
