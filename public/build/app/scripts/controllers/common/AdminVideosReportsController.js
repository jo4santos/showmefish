"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminVideosReportsController = (function () {
            function AdminVideosReportsController(VideoService) {
                this.VideoService = VideoService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminVideosReportsController initialized"));
            }
            AdminVideosReportsController.prototype.getList = function () {
                var _this = this;
                this.VideoService.getReports().then(function (reports) {
                    _this.reports = reports;
                });
            };
            AdminVideosReportsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove report?")) {
                    this.VideoService.removeReport(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminVideosReportsController;
        })();
        controllers.AdminVideosReportsController = AdminVideosReportsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
