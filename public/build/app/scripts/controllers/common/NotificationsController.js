"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var NotificationsController = (function () {
            function NotificationsController(NotificationService, SocketIOFactory) {
                var _this = this;
                this.NotificationService = NotificationService;
                this.SocketIOFactory = SocketIOFactory;
                this.notifications = [];
                this.setRead();
                this.getList();
                this.SocketIOFactory.on('notifications:updated', function () {
                    _this.getList();
                });
                console.debug(smf.utils.LogUtils.format("NotificationController initialized"));
            }
            NotificationsController.prototype.getList = function () {
                var _this = this;
                this.NotificationService.getAll()
                    .then(function (notifications) {
                    _this.notifications = notifications;
                });
            };
            NotificationsController.prototype.setRead = function () {
                this.NotificationService.setRead();
            };
            return NotificationsController;
        })();
        controllers.NotificationsController = NotificationsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
