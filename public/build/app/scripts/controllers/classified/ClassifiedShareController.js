"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ClassifiedShareController = (function () {
            function ClassifiedShareController($scope, $modalInstance, SocketIOFactory, ClassifiedService, $upload) {
                var _this = this;
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.$upload = $upload;
                this.SocketIOFactory = SocketIOFactory;
                this.ClassifiedService = ClassifiedService;
                this.classified = new smf.models.Classified();
                this.files = [];
                this.ClassifiedService.getCategories()
                    .then(function (categories) {
                    _this.categories = [];
                    categories.forEach(function (category) {
                        category.children.forEach(function (child) {
                            child["parent"] = category.name;
                            _this.categories.push(child);
                        });
                    });
                });
                this.classified.type = "sell";
                this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
                console.debug(smf.utils.LogUtils.format("ClassifiedShareController initialized"));
            }
            ClassifiedShareController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            ClassifiedShareController.prototype.generateThumbs = function () {
                if (this.files.length > 0) {
                    for (var i = 0; i < this.files.length; i++) {
                        if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL(this.files[i]);
                            fileReader.onload = (function (file) {
                                return function (e) {
                                    file.dataUrl = e.target.result;
                                };
                            })(this.files[i]);
                        }
                    }
                }
            };
            ClassifiedShareController.prototype.upload = function (file) {
                var _this = this;
                this.files.upload = this.$upload.upload({
                    url: "/data/images",
                    file: file,
                    fields: { title: file.title, thumb: true }
                }).progress(function (evt) {
                    console.log('progress: ' + _this.filesProgress + '% ' + evt.config.file.name);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == evt.config.file.name) {
                            _this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                            break;
                        }
                    }
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == config.file.name) {
                            _this.files[i].progress = 100;
                            _this.files[i].id = data;
                            break;
                        }
                    }
                });
            };
            ClassifiedShareController.prototype.cancelUpload = function (file) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == file.name) {
                        if (this.files[i].upload) {
                            this.files[i].upload.cancel();
                        }
                        else {
                            this.files.splice(i, 1);
                        }
                        break;
                    }
                }
            };
            ClassifiedShareController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.classified.images = [];
                    if (!this.files)
                        this.files = [];
                    if (this.files.length == 0) {
                        alert("Por favor envie pelo menos uma foto.");
                        return;
                    }
                    for (var i = 0; i < this.files.length; i++) {
                        if (!this.files[i].id) {
                            alert("Existe uma fotografia por enviar, confirme o envio ou cancele.");
                            return;
                        }
                        else {
                            this.classified.images.push((this.files[i].id));
                        }
                    }
                    this.classified.category = this.classified.categoryObj._id;
                    this.ClassifiedService.create(this.classified).then(function () {
                        _this.$modalInstance.dismiss();
                        alert("POST PARTILHADO COM SUCESSO");
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return ClassifiedShareController;
        })();
        controllers.ClassifiedShareController = ClassifiedShareController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
