"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminClassifiedReportsController = (function () {
            function AdminClassifiedReportsController($scope, ClassifiedService) {
                this.$scope = $scope;
                this.ClassifiedService = ClassifiedService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminClassifiedReportsController initialized"));
            }
            AdminClassifiedReportsController.prototype.getList = function () {
                var _this = this;
                this.ClassifiedService.getReports().then(function (reports) {
                    _this.reports = reports;
                });
            };
            AdminClassifiedReportsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove report?")) {
                    this.ClassifiedService.removeReport(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminClassifiedReportsController;
        })();
        controllers.AdminClassifiedReportsController = AdminClassifiedReportsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
