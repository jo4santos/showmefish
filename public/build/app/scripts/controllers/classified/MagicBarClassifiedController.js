"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var MagicBarClassifiedController = (function () {
            // Public attributes available to view
            /**
             *
             * @param userApi
             */
            function MagicBarClassifiedController($scope, $location, $modal, SocketIOFactory) {
                this.$scope = $scope;
                this.$location = $location;
                this.$modal = $modal;
                this.SocketIOFactory = SocketIOFactory;
                console.debug(smf.utils.LogUtils.format("MagicBarClassifiedController initialized"));
            }
            MagicBarClassifiedController.prototype.share = function () {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/classified/modal-share.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.ClassifiedShareController as ClassifiedShareController";
                this.$modal.open(modalSettings);
            };
            return MagicBarClassifiedController;
        })();
        controllers.MagicBarClassifiedController = MagicBarClassifiedController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
