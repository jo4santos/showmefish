"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminClassifiedListController = (function () {
            function AdminClassifiedListController($scope, ClassifiedService, SocketIOFactory) {
                this.classified = new Array();
                this.$scope = $scope;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                this.getList();
                this.setListeners();
                this.removed = this.added = this.updated = 0;
                console.debug(smf.utils.LogUtils.format("AdminClassifiedListController initialized"));
            }
            AdminClassifiedListController.prototype.getList = function () {
                var _this = this;
                this.ClassifiedService.getAll(null, null, null).then(function (classified) {
                    _this.classified = classified;
                    _this.removed = _this.added = _this.updated = 0;
                });
            };
            AdminClassifiedListController.prototype.remove = function (type, param) {
                if (confirm("Remove classified?")) {
                    this.ClassifiedService.remove(type, param).then(function () {
                        console.log("done");
                    });
                }
            };
            AdminClassifiedListController.prototype.setListeners = function () {
                var _this = this;
                this.SocketIOFactory.on('classified:removed', function () {
                    _this.removed++;
                });
                this.SocketIOFactory.on('classified:added', function () {
                    _this.added++;
                });
                this.SocketIOFactory.on('classified:updated', function () {
                    _this.updated++;
                });
            };
            AdminClassifiedListController.prototype.enhance = function (element) {
                this.ClassifiedService.enhance(element);
            };
            return AdminClassifiedListController;
        })();
        controllers.AdminClassifiedListController = AdminClassifiedListController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
