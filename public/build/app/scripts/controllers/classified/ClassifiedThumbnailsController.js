"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ClassifiedThumbnailController = (function () {
            function ClassifiedThumbnailController($scope, ClassifiedService, SocketIOFactory, $modal, ChatService) {
                this.$scope = $scope;
                this.ChatService = ChatService;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                this.$modal = $modal;
                this.classified = {};
                this.classified._id = this.$scope.classifiedId;
                this.getDetails();
            }
            ClassifiedThumbnailController.prototype.getDetails = function () {
                var _this = this;
                this.ClassifiedService.getDetails("_id", this.classified._id).then(function (classified) {
                    _this.classified = classified[0];
                    _this.ClassifiedService.enhance(_this.classified);
                });
            };
            return ClassifiedThumbnailController;
        })();
        controllers.ClassifiedThumbnailController = ClassifiedThumbnailController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
