"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ClassifiedReportController = (function () {
            function ClassifiedReportController($scope, $modalInstance, SocketIOFactory, ClassifiedService, classifiedId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.SocketIOFactory = SocketIOFactory;
                this.ClassifiedService = ClassifiedService;
                this.classifiedId = classifiedId;
                this.classified = new smf.models.Classified();
                this.reason = "";
                console.debug(smf.utils.LogUtils.format("ClassifiedReportController initialized"));
            }
            ClassifiedReportController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            ClassifiedReportController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.ClassifiedService.addReport(this.classifiedId, this.reason).then(function () {
                        _this.$modalInstance.dismiss();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return ClassifiedReportController;
        })();
        controllers.ClassifiedReportController = ClassifiedReportController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
