"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ClassifiedController = (function () {
            function ClassifiedController($scope, ClassifiedService, SocketIOFactory, $stateParams) {
                var _this = this;
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                this.classified = [];
                this.categories = [];
                this.sortOptionsList = [];
                this.sortOptionsList.push({ title: "Mais recentes", field: "_id", direction: "-1" });
                this.sortOptionsList.push({ title: "Mais antigos", field: "_id", direction: "1" });
                this.sortOptionsList.push({ title: "Mais caros", field: "price", direction: "-1" });
                this.sortOptionsList.push({ title: "Mais baratos", field: "price", direction: "1" });
                this.sortOptionsList.push({ title: "Mais vistos", field: "views.count", direction: "-1" });
                this.sortOptionsList.push({ title: "Menos vistos", field: "views.count", direction: "1" });
                this.sortIndex = 0;
                this.filters = new smf.models.ClassifiedFilters();
                if (this.$stateParams.type && this.$stateParams.type != "") {
                    this.filters.type = this.$stateParams.type;
                }
                this.sortOptions = new smf.models.ClassifiedSortOptions();
                this.paginationOptions = new smf.models.ClassifiedPaginationOptions();
                this.paginationOptions.page = 1;
                this.paginationOptions.limit = 2;
                this.getCategories();
                this.getList();
                this.setListeners();
                this.removed = this.added = this.updated = 0;
                this.$scope.$watch('[ClassifiedController.sortIndex, ClassifiedController.paginationOptions.page, ClassifiedController.paginationOptions.limit]', function () {
                    _this.getList();
                });
                console.debug(smf.utils.LogUtils.format("ClassifiedController initialized"));
            }
            ClassifiedController.prototype.getList = function () {
                var _this = this;
                this.sortOptions.field = this.sortOptionsList[this.sortIndex].field;
                this.sortOptions.direction = this.sortOptionsList[this.sortIndex].direction;
                this.ClassifiedService.getAll(this.filters, this.sortOptions, this.paginationOptions)
                    .then(function (classified) {
                    _this.classified = classified;
                    for (var i in _this.classified) {
                        _this.ClassifiedService.enhance(_this.classified[i]);
                    }
                    _this.removed = _this.added = _this.updated = 0;
                });
            };
            ClassifiedController.prototype.getCategories = function () {
                var _this = this;
                this.ClassifiedService.getCategories()
                    .then(function (categories) {
                    _this.categories = [];
                    categories.forEach(function (category) {
                        category.children.forEach(function (child) {
                            child["parent"] = category.name;
                            _this.categories.push(child);
                        });
                    });
                });
            };
            ClassifiedController.prototype.remove = function (type, param) {
                if (confirm("Remove classified?")) {
                    this.ClassifiedService.remove(type, param).then(function () {
                        console.log("done");
                    });
                }
            };
            ClassifiedController.prototype.setListeners = function () {
                var _this = this;
                this.SocketIOFactory.on('classified:removed', function () {
                    _this.removed++;
                });
                this.SocketIOFactory.on('classified:added', function () {
                    _this.added++;
                });
                this.SocketIOFactory.on('classified:updated', function () {
                    _this.updated++;
                });
            };
            ClassifiedController.prototype.applyFilters = function () {
                this.filters.category = "";
                if (this.filters.categoryObj) {
                    this.filters.category = this.filters.categoryObj._id;
                }
                this.getList();
            };
            return ClassifiedController;
        })();
        controllers.ClassifiedController = ClassifiedController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
