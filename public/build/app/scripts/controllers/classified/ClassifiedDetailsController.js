"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var ClassifiedDetailsController = (function () {
            function ClassifiedDetailsController($scope, ClassifiedService, SocketIOFactory, $modal, $stateParams, ChatService) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.ChatService = ChatService;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                this.$modal = $modal;
                this._id = this.$stateParams._id;
                this.getDetails("_id", this._id);
            }
            ClassifiedDetailsController.prototype.getDetails = function (type, param) {
                var _this = this;
                this.ClassifiedService.getDetails(type, param).then(function (classified) {
                    _this.classified = classified[0];
                    _this.ClassifiedService.enhance(_this.classified);
                });
            };
            ClassifiedDetailsController.prototype.sendMessage = function () {
                var _this = this;
                var message = prompt("Message?");
                if (message) {
                    var chat = new smf.models.Chat();
                    chat.users = [this.classified.userId];
                    chat.classifiedId = this.classified._id;
                    this.ChatService.create(chat).then(function (id) {
                        _this.ChatService.sendMessage(id, message).then(function () {
                            alert("Mensagem enviada");
                        });
                    });
                }
            };
            return ClassifiedDetailsController;
        })();
        controllers.ClassifiedDetailsController = ClassifiedDetailsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
