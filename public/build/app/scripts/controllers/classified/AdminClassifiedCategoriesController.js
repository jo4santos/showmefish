"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminClassifiedCategoriesController = (function () {
            function AdminClassifiedCategoriesController($scope, ClassifiedService) {
                this.$scope = $scope;
                this.ClassifiedService = ClassifiedService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminClassifiedCategoriesController initialized"));
            }
            AdminClassifiedCategoriesController.prototype.getList = function () {
                var _this = this;
                this.ClassifiedService.getCategories(true).then(function (categories) {
                    _this.categories = categories;
                });
            };
            AdminClassifiedCategoriesController.prototype.remove = function (_id, parentId) {
                var _this = this;
                if (confirm("Remove category?")) {
                    this.ClassifiedService.removeCategory(_id, parentId).then(function () {
                        _this.getList();
                    });
                }
            };
            AdminClassifiedCategoriesController.prototype.add = function (parentId) {
                var _this = this;
                var name = prompt("Category name?");
                if (name) {
                    this.ClassifiedService.addCategory(name, parentId).then(function () {
                        _this.getList();
                    });
                }
            };
            AdminClassifiedCategoriesController.prototype.update = function (_id, parentId) {
                var _this = this;
                var name = prompt("New name?");
                if (name) {
                    this.ClassifiedService.updateCategory(_id, name, parentId).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminClassifiedCategoriesController;
        })();
        controllers.AdminClassifiedCategoriesController = AdminClassifiedCategoriesController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
