"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialProfileController = (function () {
            function SocialProfileController($scope, $state, FriendService, UsersService, SocketIOFactory, $stateParams, LoggedUserService, ChatService) {
                var _this = this;
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.$state = $state;
                this.FriendService = FriendService;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.ChatService = ChatService;
                this.SocketIOFactory = SocketIOFactory;
                var userId = this.$stateParams.userId || "me";
                this.user = new smf.models.User();
                this.user._id = userId;
                this.loggedUser = new smf.models.LoggedUser();
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                    _this.UsersService.getDetails("_id", userId).then(function (user) {
                        _this.user = user[0];
                        _this.UsersService.enhance(_this.user);
                        if (_this.loggedUser.friends.confirmed.indexOf(_this.user._id) !== -1) {
                            _this.friendStatus = "confirmed";
                        }
                        else if (_this.loggedUser.friends.sent.indexOf(_this.user._id) !== -1) {
                            _this.friendStatus = "sent";
                        }
                        else if (_this.loggedUser.friends.received.indexOf(_this.user._id) !== -1) {
                            _this.friendStatus = "received";
                        }
                        _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                    });
                });
                console.debug(smf.utils.LogUtils.format("SocialProfileController initialized"));
            }
            SocialProfileController.prototype.friendInvite = function () {
                this.FriendService.invite(this.user._id).then(function () {
                    location.reload();
                });
            };
            SocialProfileController.prototype.friendRemove = function () {
                this.FriendService.remove(this.user._id).then(function () {
                    location.reload();
                });
            };
            SocialProfileController.prototype.friendAccept = function () {
                this.FriendService.accept(this.user._id).then(function () {
                    location.reload();
                });
            };
            SocialProfileController.prototype.startChat = function () {
                var _this = this;
                var chat = new smf.models.Chat();
                chat.users = [this.user._id];
                this.ChatService.create(chat).then(function (id) {
                    _this.$state.go('social.chats.details', { chatId: id });
                });
            };
            return SocialProfileController;
        })();
        controllers.SocialProfileController = SocialProfileController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
