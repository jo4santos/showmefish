"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var MagicBarGroupsController = (function () {
            // Public attributes available to view
            /**
             *
             * @param userApi
             */
            function MagicBarGroupsController($scope, $modal) {
                this.$scope = $scope;
                this.$modal = $modal;
                console.debug(smf.utils.LogUtils.format("MagicBarGroupsController initialized"));
            }
            MagicBarGroupsController.prototype.create = function () {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/social/modal-create-group.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.SocialGroupCreateController as SocialGroupCreateController";
                this.$modal.open(modalSettings);
            };
            return MagicBarGroupsController;
        })();
        controllers.MagicBarGroupsController = MagicBarGroupsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
