"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialShareController = (function () {
            function SocialShareController($scope, $modalInstance, SocketIOFactory, PostService, VideoService, $upload, targetGroupId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.$upload = $upload;
                this.SocketIOFactory = SocketIOFactory;
                this.PostService = PostService;
                this.VideoService = VideoService;
                this.post = new smf.models.Post();
                this.post.targetGroupId = targetGroupId || "";
                this.files = [];
                this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);
                console.debug(smf.utils.LogUtils.format("SocialShareController initialized"));
            }
            SocialShareController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            SocialShareController.prototype.generateThumbs = function () {
                if (this.files.length > 0) {
                    for (var i = 0; i < this.files.length; i++) {
                        if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL(this.files[i]);
                            fileReader.onload = (function (file) {
                                return function (e) {
                                    file.dataUrl = e.target.result;
                                };
                            })(this.files[i]);
                        }
                    }
                }
            };
            SocialShareController.prototype.upload = function (file) {
                var _this = this;
                this.files.upload = this.$upload.upload({
                    url: "/data/images",
                    file: file,
                    fields: { title: file.title, thumb: true }
                }).progress(function (evt) {
                    console.log('progress: ' + _this.filesProgress + '% ' + evt.config.file.name);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == evt.config.file.name) {
                            _this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                            break;
                        }
                    }
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    for (var i = 0; i < _this.files.length; i++) {
                        if (_this.files[i].name == config.file.name) {
                            _this.files[i].progress = 100;
                            _this.files[i].id = data;
                            break;
                        }
                    }
                });
            };
            SocialShareController.prototype.cancelUpload = function (file) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == file.name) {
                        if (this.files[i].upload) {
                            this.files[i].upload.cancel();
                        }
                        else {
                            this.files.splice(i, 1);
                        }
                        break;
                    }
                }
            };
            SocialShareController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.post.images = [];
                    if (!this.files)
                        this.files = [];
                    for (var i = 0; i < this.files.length; i++) {
                        if (!this.files[i].id) {
                            alert("Existe uma fotografia por enviar, confirme o envio ou cancele.");
                            return;
                        }
                        else {
                            this.post.images.push((this.files[i].id));
                        }
                    }
                    this.VideoService.create(this.video).then(function (videoId) {
                        _this.post.video = videoId;
                        _this.PostService.create(_this.post).then(function () {
                            _this.$modalInstance.dismiss();
                            location.reload();
                        });
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return SocialShareController;
        })();
        controllers.SocialShareController = SocialShareController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
