"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialGroupDetailsController = (function () {
            function SocialGroupDetailsController($scope, FriendService, GroupService, LoggedUserService, PostService, $stateParams) {
                var _this = this;
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.LoggedUserService = LoggedUserService;
                this.GroupService = GroupService;
                this.FriendService = FriendService;
                this.PostService = PostService;
                this.group = new smf.models.Group();
                this.group._id = this.$stateParams.groupId;
                this.posts = [];
                this.LoggedUserService.getUser().then(function (loggedUser) {
                    _this.loggedUser = loggedUser;
                });
                this.load();
                console.debug(smf.utils.LogUtils.format("SocialGroupDetailsController initialized"));
            }
            SocialGroupDetailsController.prototype.load = function () {
                var _this = this;
                this.GroupService.getDetails("_id", this.group._id).then(function (group) {
                    _this.group = group[0];
                    _this.userBelongs = _this.belongs(_this.loggedUser._id);
                    _this.userIsAdmin = _this.isAdmin(_this.loggedUser._id);
                    if (!_this.userBelongs && _this.group.isSecret && !_this.userIsAdmin) {
                        _this.error = "This group is secret, you can only join via the admin!";
                        return;
                    }
                    else if (!_this.userBelongs && _this.group.isPrivate && !_this.userIsAdmin) {
                        _this.error = "This group is private, please send a join request!";
                        return;
                    }
                    var filters = new smf.models.PostFilters();
                    filters.targetGroupId = _this.group._id;
                    _this.PostService.getAll(filters).then(function (posts) {
                        _this.posts = posts;
                    });
                });
            };
            SocialGroupDetailsController.prototype.back = function () {
                window.history.back();
            };
            SocialGroupDetailsController.prototype.remove = function () {
                if (confirm("Remover grupo?")) {
                    this.GroupService.remove("_id", this.group._id).then(function () {
                        window.history.back();
                    });
                }
            };
            SocialGroupDetailsController.prototype.acceptRequest = function (userId) {
                var _this = this;
                this.GroupService.acceptRequest(this.group._id, userId).then(function () {
                    _this.load();
                });
            };
            SocialGroupDetailsController.prototype.denyRequest = function (userId) {
                var _this = this;
                this.GroupService.denyRequest(this.group._id, userId).then(function () {
                    _this.load();
                });
            };
            SocialGroupDetailsController.prototype.removeUser = function (userId) {
                var _this = this;
                if (confirm("Remover utilizador do grupo?")) {
                    this.GroupService.leave(this.group._id, userId).then(function () {
                        _this.load();
                    });
                }
            };
            SocialGroupDetailsController.prototype.setAdmin = function (userId) {
                var _this = this;
                if (this.isAdmin(userId)) {
                    if (this.group.admins.length == 1) {
                        alert("Não é possível remover administrador. Seleccione pelo menos mais um administrador antes de continuar.");
                        return;
                    }
                    if (confirm("Remover privilégios de administrador do grupo?")) {
                        this.GroupService.removeAdmin(this.group._id, userId).then(function () {
                            _this.load();
                        });
                    }
                }
                else {
                    if (confirm("Dar privilégios de administrador do grupo?")) {
                        this.GroupService.addAdmin(this.group._id, userId).then(function () {
                            _this.load();
                        });
                    }
                }
            };
            SocialGroupDetailsController.prototype.isAdmin = function (userId) {
                if (this.group.admins.indexOf(userId) !== -1) {
                    return true;
                }
                return false;
            };
            SocialGroupDetailsController.prototype.belongs = function (userId) {
                if (this.group.users.indexOf(userId) !== -1) {
                    return true;
                }
                return false;
            };
            SocialGroupDetailsController.prototype.addUser = function () {
                var _this = this;
                this.FriendService.select(this.group.users).then(function (users) {
                    _this.GroupService.addUsers(_this.group._id, _this.loggedUser._id, users).then(function () {
                        _this.load();
                    });
                });
            };
            return SocialGroupDetailsController;
        })();
        controllers.SocialGroupDetailsController = SocialGroupDetailsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
