"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialProfileAboutController = (function () {
            function SocialProfileAboutController($scope, ClassifiedService, UsersService, SocketIOFactory, $stateParams, LoggedUserService) {
                var _this = this;
                this.classified = new Array();
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                var userId = this.$stateParams.userId || "me";
                this.user = new smf.models.User();
                this.loggedUser = new smf.models.LoggedUser();
                this.isLoggedUser = this.user._id == this.loggedUser._id;
                this.UsersService.getDetails("_id", userId).then(function (user) {
                    _this.user = user[0];
                    _this.UsersService.enhance(_this.user);
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.UsersService.getImages("userId", userId).then(function (images) {
                    _this.images = images;
                });
                this.UsersService.getVideos("userId", userId).then(function (videos) {
                    _this.videos = videos;
                });
                var classifiedFilters = new smf.models.ClassifiedFilters();
                classifiedFilters.userId = userId;
                this.ClassifiedService.getAll(classifiedFilters, { field: "views.count", direction: "-1" }, null)
                    .then(function (classified) {
                    _this.classified = classified;
                });
                console.debug(smf.utils.LogUtils.format("SocialProfileAboutController initialized"));
            }
            return SocialProfileAboutController;
        })();
        controllers.SocialProfileAboutController = SocialProfileAboutController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
