"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialGroupsController = (function () {
            function SocialGroupsController($scope, GroupService) {
                this.$scope = $scope;
                this.GroupService = GroupService;
                this.groups = [];
                this.getList();
                console.debug(smf.utils.LogUtils.format("SocialGroupsController initialized"));
            }
            SocialGroupsController.prototype.getList = function () {
                var _this = this;
                this.GroupService.getAll(this.filters)
                    .then(function (groups) {
                    _this.groups = groups;
                });
            };
            return SocialGroupsController;
        })();
        controllers.SocialGroupsController = SocialGroupsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
