"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialCommentController = (function () {
            function SocialCommentController($scope, UsersService) {
                var _this = this;
                this.$scope = $scope;
                this.UsersService = UsersService;
                this.comment = this.$scope.comment;
                this.UsersService.getDetails("_id", this.comment.userId).then(function (user) {
                    _this.comment.user = user[0];
                    _this.UsersService.enhance(_this.comment.user);
                });
                console.debug(smf.utils.LogUtils.format("SocialCommentController initialized"));
            }
            return SocialCommentController;
        })();
        controllers.SocialCommentController = SocialCommentController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
