"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminPostsReportsController = (function () {
            function AdminPostsReportsController($scope, PostService) {
                this.$scope = $scope;
                this.PostService = PostService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminPostsReportsController initialized"));
            }
            AdminPostsReportsController.prototype.getList = function () {
                var _this = this;
                this.PostService.getReports().then(function (reports) {
                    _this.reports = reports;
                });
            };
            AdminPostsReportsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove report?")) {
                    this.PostService.removeReport(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminPostsReportsController;
        })();
        controllers.AdminPostsReportsController = AdminPostsReportsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
