"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialPostReportController = (function () {
            function SocialPostReportController($scope, $modalInstance, SocketIOFactory, PostService, postId) {
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.SocketIOFactory = SocketIOFactory;
                this.PostService = PostService;
                this.postId = postId;
                this.post = new smf.models.Post();
                this.reason = "";
                console.debug(smf.utils.LogUtils.format("SocialPostReportController initialized"));
            }
            SocialPostReportController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            SocialPostReportController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.PostService.addReport(this.postId, this.reason).then(function () {
                        _this.$modalInstance.dismiss();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return SocialPostReportController;
        })();
        controllers.SocialPostReportController = SocialPostReportController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
