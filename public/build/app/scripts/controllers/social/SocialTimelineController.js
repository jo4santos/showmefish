"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialTimelineController = (function () {
            function SocialTimelineController($scope, $stateParams, SocketIOFactory, PostService) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.SocketIOFactory = SocketIOFactory;
                this.PostService = PostService;
                if (this.$stateParams.userId) {
                    this.userId = this.$stateParams.userId;
                }
                this.isLoading = true;
                this.posts = [];
                this.getList();
                console.debug(smf.utils.LogUtils.format("SocialTimelineController initialized"));
            }
            SocialTimelineController.prototype.getList = function () {
                var _this = this;
                this.isLoading = true;
                var filters = new smf.models.PostFilters();
                if (this.userId) {
                    if (this.userId && this.userId != "") {
                        filters.userId = this.userId;
                        filters.targetUserId = this.userId;
                    }
                }
                this.PostService.getAll(filters).then(function (posts) {
                    _this.posts = posts;
                    _this.isLoading = false;
                });
            };
            return SocialTimelineController;
        })();
        controllers.SocialTimelineController = SocialTimelineController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
