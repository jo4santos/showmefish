"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialClassifiedController = (function () {
            function SocialClassifiedController($scope, ClassifiedService, SocketIOFactory, $stateParams) {
                this.classified = new Array();
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.ClassifiedService = ClassifiedService;
                this.SocketIOFactory = SocketIOFactory;
                var userId = this.$stateParams.userId || "me";
                this.getList(userId);
                console.debug(smf.utils.LogUtils.format("SocialClassifiedController initialized"));
            }
            SocialClassifiedController.prototype.getList = function (userId) {
                var _this = this;
                var filters = new smf.models.ClassifiedFilters();
                if (userId && userId != "") {
                    filters.userId = userId;
                }
                this.ClassifiedService.getAll(filters, null, null)
                    .then(function (classified) {
                    _this.classified = classified;
                });
            };
            return SocialClassifiedController;
        })();
        controllers.SocialClassifiedController = SocialClassifiedController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
