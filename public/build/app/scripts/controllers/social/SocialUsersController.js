"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialUsersController = (function () {
            function SocialUsersController($scope, UsersService, SocketIOFactory) {
                var _this = this;
                this.$scope = $scope;
                this.UsersService = UsersService;
                this.SocketIOFactory = SocketIOFactory;
                this.users = [];
                this.UsersService.getInterests(true).then(function (interests) {
                    _this.interests = interests;
                });
                this.getList();
                console.debug(smf.utils.LogUtils.format("SocialUsersController initialized"));
            }
            SocialUsersController.prototype.getList = function () {
                var _this = this;
                this.UsersService.getAll(this.filters)
                    .then(function (users) {
                    _this.users = users;
                    for (var i in _this.users) {
                        _this.UsersService.enhance(_this.users[i]);
                    }
                });
            };
            SocialUsersController.prototype.applyFilters = function () {
                this.filters.interests = [];
                for (var i in this.filters.interestsObj) {
                    this.filters.interests.push(this.filters.interestsObj[i]._id);
                }
                this.getList();
            };
            return SocialUsersController;
        })();
        controllers.SocialUsersController = SocialUsersController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
