"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminGroupListController = (function () {
            function AdminGroupListController($scope, GroupService) {
                this.$scope = $scope;
                this.GroupService = GroupService;
                this.groups = [];
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminGroupListController initialized"));
            }
            AdminGroupListController.prototype.getList = function () {
                var _this = this;
                var filters = new smf.models.GroupFilters();
                this.GroupService.getAll(filters).then(function (groups) {
                    _this.groups = groups;
                });
            };
            AdminGroupListController.prototype.remove = function (type, param) {
                var _this = this;
                if (confirm("Remover grupo?")) {
                    this.GroupService.remove(type, param).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminGroupListController;
        })();
        controllers.AdminGroupListController = AdminGroupListController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
