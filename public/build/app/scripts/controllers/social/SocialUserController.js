"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialUserController = (function () {
            function SocialUserController($scope, $modal, FriendService, UsersService, SocketIOFactory, LoggedUserService) {
                this.$scope = $scope;
                this.$modal = $modal;
                this.FriendService = FriendService;
                this.UsersService = UsersService;
                this.SocketIOFactory = SocketIOFactory;
                this.LoggedUserService = LoggedUserService;
                this.actions = this.$scope.actions;
                this.user = new smf.models.User();
                this.user._id = this.$scope.userId;
                this.load();
                console.debug(smf.utils.LogUtils.format("SocialUserController initialized"));
            }
            SocialUserController.prototype.load = function () {
                var _this = this;
                this.UsersService.getDetails("_id", this.user._id).then(function (user) {
                    _this.user = user[0];
                    _this.UsersService.enhance(_this.user);
                    _this.LoggedUserService.getUser().then(function (loggedUser) {
                        if (loggedUser._id == _this.user._id) {
                            _this.actions = false;
                        }
                        if (loggedUser.friends.confirmed.indexOf(_this.user._id) !== -1) {
                            _this.status = "confirmed";
                        }
                        else if (loggedUser.friends.sent.indexOf(_this.user._id) !== -1) {
                            _this.status = "sent";
                        }
                        else if (loggedUser.friends.received.indexOf(_this.user._id) !== -1) {
                            _this.status = "received";
                        }
                    });
                });
            };
            SocialUserController.prototype.friendInvite = function () {
                this.FriendService.invite(this.user._id).then(function () {
                    location.reload();
                });
            };
            SocialUserController.prototype.friendRemove = function () {
                this.FriendService.remove(this.user._id).then(function () {
                    location.reload();
                });
            };
            SocialUserController.prototype.friendAccept = function () {
                this.FriendService.accept(this.user._id).then(function () {
                    location.reload();
                });
            };
            return SocialUserController;
        })();
        controllers.SocialUserController = SocialUserController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
