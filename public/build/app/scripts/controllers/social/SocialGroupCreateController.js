"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialGroupCreateController = (function () {
            function SocialGroupCreateController($scope, $modalInstance, GroupService, UsersService, LoggedUserService) {
                var _this = this;
                this.$scope = $scope;
                this.$modalInstance = $modalInstance;
                this.GroupService = GroupService;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.group = new smf.models.Group();
                this.group.isPrivate = false;
                this.group.isSecret = false;
                var filters = new smf.models.UserFilters();
                this.LoggedUserService.getUser().then(function (loggedUser) {
                    _this.UsersService.getAll(filters)
                        .then(function (users) {
                        _this.users = users;
                        for (var i in _this.users) {
                            if (_this.users[i]._id == loggedUser._id) {
                                _this.users.splice(i, 1);
                                break;
                            }
                        }
                        _this.selectedUsers = [];
                    });
                });
                console.debug(smf.utils.LogUtils.format("SocialGroupCreateController initialized"));
            }
            SocialGroupCreateController.prototype.cancel = function () {
                this.$modalInstance.dismiss();
            };
            SocialGroupCreateController.prototype.submit = function () {
                var _this = this;
                this.submitted = true;
                if (!this.form.$invalid) {
                    this.group.users = [];
                    for (var i in this.selectedUsers) {
                        if (this.selectedUsers[i] == true) {
                            this.group.users.push(angular.copy(i));
                        }
                    }
                    this.GroupService.create(this.group).then(function () {
                        _this.$modalInstance.dismiss();
                        location.reload();
                    });
                }
                else {
                    alert("NOK");
                }
            };
            return SocialGroupCreateController;
        })();
        controllers.SocialGroupCreateController = SocialGroupCreateController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
