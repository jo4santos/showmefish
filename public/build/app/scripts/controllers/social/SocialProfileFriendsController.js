"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialProfileFriendsController = (function () {
            function SocialProfileFriendsController($scope, UsersService, FriendService, SocketIOFactory, $stateParams, LoggedUserService) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.UsersService = UsersService;
                this.FriendService = FriendService;
                this.LoggedUserService = LoggedUserService;
                this.SocketIOFactory = SocketIOFactory;
                console.debug(smf.utils.LogUtils.format("SocialProfileFriendsController initialized"));
            }
            return SocialProfileFriendsController;
        })();
        controllers.SocialProfileFriendsController = SocialProfileFriendsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
