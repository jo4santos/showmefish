"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialSidebarRightController = (function () {
            function SocialSidebarRightController($scope, ClassifiedService, BlogsService) {
                var _this = this;
                this.blogItems = [];
                this.$scope = $scope;
                this.ClassifiedService = ClassifiedService;
                this.BlogsService = BlogsService;
                this.classified = [];
                this.sortOptionsList = [];
                this.sortOptionsList.push({ title: "Mais recentes", field: "_id", direction: "-1" });
                this.sortOptionsList.push({ title: "Mais antigos", field: "_id", direction: "1" });
                this.sortOptionsList.push({ title: "Mais caros", field: "price", direction: "-1" });
                this.sortOptionsList.push({ title: "Mais baratos", field: "price", direction: "1" });
                this.sortOptionsList.push({ title: "Mais vistos", field: "views.count", direction: "-1" });
                this.sortOptionsList.push({ title: "Menos vistos", field: "views.count", direction: "1" });
                this.sortIndex = 0;
                this.filters = new smf.models.ClassifiedFilters();
                this.sortOptions = new smf.models.ClassifiedSortOptions();
                this.paginationOptions = new smf.models.ClassifiedPaginationOptions();
                this.paginationOptions.page = 1;
                this.paginationOptions.limit = 3;
                this.getList();
                this.$scope.$watch('[SocialSidebarRightController.sortIndex, SocialSidebarRightController.paginationOptions.page, SocialSidebarRightController.paginationOptions.limit]', function () {
                    _this.getList();
                });
                console.debug(smf.utils.LogUtils.format("SocialSidebarRightController initialized"));
            }
            SocialSidebarRightController.prototype.getList = function () {
                var _this = this;
                this.sortOptions.field = this.sortOptionsList[this.sortIndex].field;
                this.sortOptions.direction = this.sortOptionsList[this.sortIndex].direction;
                this.ClassifiedService.getAll(this.filters, this.sortOptions, this.paginationOptions)
                    .then(function (classified) {
                    _this.classified = classified;
                    for (var i in _this.classified) {
                        _this.ClassifiedService.enhance(_this.classified[i]);
                    }
                });
                this.BlogsService.getList(null).then(function (blogs) {
                    blogs.forEach(function (blog) {
                        var item = blog.items[0];
                        item.blog = {};
                        item.blog.name = blog.name;
                        item.blog.description = blog.description;
                        item.blog.url = blog.url;
                        _this.blogItems.push(item);
                    });
                });
            };
            SocialSidebarRightController.prototype.applyFilters = function () {
                this.filters.category = "";
                if (this.filters.categoryObj) {
                    this.filters.category = this.filters.categoryObj._id;
                }
                this.getList();
            };
            return SocialSidebarRightController;
        })();
        controllers.SocialSidebarRightController = SocialSidebarRightController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
