"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var MagicBarGroupDetailsController = (function () {
            function MagicBarGroupDetailsController($scope, $modal, $stateParams) {
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.$modal = $modal;
                this.groupId = this.$stateParams.groupId;
                console.debug(smf.utils.LogUtils.format("MagicBarGroupDetailsController initialized"));
            }
            MagicBarGroupDetailsController.prototype.share = function () {
                var _this = this;
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/social/modal-share.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.SocialShareController as SocialShareController";
                modalSettings.resolve = {
                    targetGroupId: function () {
                        return _this.groupId;
                    }
                };
                this.$modal.open(modalSettings);
            };
            return MagicBarGroupDetailsController;
        })();
        controllers.MagicBarGroupDetailsController = MagicBarGroupDetailsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
