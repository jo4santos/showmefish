"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialPostController = (function () {
            function SocialPostController($scope, $modal, PostService, SocketIOFactory) {
                this.$scope = $scope;
                this.$modal = $modal;
                this.PostService = PostService;
                this.SocketIOFactory = SocketIOFactory;
                this.isLoading = true;
                this.post = this.$scope.post;
                this.load();
                console.debug(smf.utils.LogUtils.format("SocialPostController initialized"));
            }
            SocialPostController.prototype.load = function () {
                var _this = this;
                this.isLoading = true;
                this.PostService.getDetails("_id", this.post._id).then(function (post) {
                    _this.post = post;
                    _this.PostService.enhance(_this.post).finally(function () {
                        _this.isLoading = false;
                    });
                });
            };
            SocialPostController.prototype.report = function () {
                var _this = this;
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/social/modal-report-post.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.SocialPostReportController as SocialPostReportController";
                modalSettings.resolve = {
                    postId: function () {
                        return _this.post._id;
                    }
                };
                this.$modal.open(modalSettings);
            };
            SocialPostController.prototype.remove = function () {
                if (confirm("Remove post?")) {
                    this.PostService.remove("_id", this.post._id).then(function () {
                        location.reload();
                    });
                }
            };
            SocialPostController.prototype.toggleLike = function () {
                var _this = this;
                if (this.post.likes.userLiked) {
                    this.PostService.removeLike(this.post._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.PostService.addLike(this.post._id).then(function () {
                        _this.load();
                    });
                }
            };
            SocialPostController.prototype.addComment = function () {
                var _this = this;
                var comment = prompt("Comment?");
                if (comment) {
                    this.PostService.addComment(this.post._id, comment).then(function () {
                        _this.load();
                    });
                }
            };
            SocialPostController.prototype.removeComment = function (comment) {
                var _this = this;
                if (confirm("Remove comment?")) {
                    this.PostService.removeComment(this.post._id, comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            SocialPostController.prototype.toggleLikeComment = function (comment) {
                var _this = this;
                if (comment.likes.userLiked) {
                    this.PostService.removeLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
                else {
                    this.PostService.addLikeComment(comment._id).then(function () {
                        _this.load();
                    });
                }
            };
            return SocialPostController;
        })();
        controllers.SocialPostController = SocialPostController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
