"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialProfileVideosController = (function () {
            function SocialProfileVideosController($scope, VideoService, UsersService, SocketIOFactory, $stateParams, LoggedUserService) {
                var _this = this;
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.VideoService = VideoService;
                this.SocketIOFactory = SocketIOFactory;
                var userId = this.$stateParams.userId || "me";
                this.user = new smf.models.User();
                this.loggedUser = new smf.models.LoggedUser();
                this.user._id = userId;
                this.isLoggedUser = this.user._id == this.loggedUser._id;
                this.UsersService.getDetails("_id", userId).then(function (user) {
                    _this.user = user[0];
                    _this.UsersService.enhance(_this.user);
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.loadVideos();
                console.debug(smf.utils.LogUtils.format("SocialProfileVideosController initialized"));
            }
            SocialProfileVideosController.prototype.loadVideos = function () {
                var _this = this;
                this.UsersService.getVideos("userId", this.user._id).then(function (videos) {
                    _this.videos = videos;
                });
            };
            SocialProfileVideosController.prototype.remove = function (videoId) {
                var _this = this;
                if (confirm("Remover video?")) {
                    this.UsersService.removeVideo(videoId).then(function () {
                        _this.loadVideos();
                    });
                }
            };
            SocialProfileVideosController.prototype.addLike = function (elementId) {
                var _this = this;
                this.VideoService.addLike(elementId).then(function () {
                    _this.loadVideos();
                });
            };
            SocialProfileVideosController.prototype.removeLike = function (elementId) {
                var _this = this;
                this.VideoService.removeLike(elementId).then(function () {
                    _this.loadVideos();
                });
            };
            return SocialProfileVideosController;
        })();
        controllers.SocialProfileVideosController = SocialProfileVideosController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
