"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialGroupController = (function () {
            function SocialGroupController($scope, GroupService, LoggedUserService) {
                var _this = this;
                this.$scope = $scope;
                this.LoggedUserService = LoggedUserService;
                this.GroupService = GroupService;
                this.group = new smf.models.Group();
                this.group._id = this.$scope.groupId;
                this.LoggedUserService.getUser().then(function (loggedUser) {
                    _this.loggedUser = loggedUser;
                });
                this.load();
                console.debug(smf.utils.LogUtils.format("SocialGroupController initialized"));
            }
            SocialGroupController.prototype.load = function () {
                var _this = this;
                this.GroupService.getDetails("_id", this.group._id).then(function (group) {
                    _this.group = group[0];
                    _this.userBelongs = _this.group.users.indexOf(_this.loggedUser._id) !== -1;
                    _this.userRequested = _this.group.requests.indexOf(_this.loggedUser._id) !== -1;
                    _this.userIsAdmin = _this.group.admins.indexOf(_this.loggedUser._id) !== -1;
                });
            };
            SocialGroupController.prototype.join = function () {
                var _this = this;
                this.GroupService.join(this.group._id, this.loggedUser._id).then(function () {
                    _this.load();
                });
            };
            SocialGroupController.prototype.leave = function () {
                var _this = this;
                this.GroupService.leave(this.group._id, this.loggedUser._id).then(function () {
                    _this.load();
                });
            };
            return SocialGroupController;
        })();
        controllers.SocialGroupController = SocialGroupController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
