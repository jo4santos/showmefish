"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var MagicBarSocialController = (function () {
            // Public attributes available to view
            /**
             *
             * @param userApi
             */
            function MagicBarSocialController($scope, $modal, SocketIOFactory) {
                this.$scope = $scope;
                this.$modal = $modal;
                this.SocketIOFactory = SocketIOFactory;
                console.debug(smf.utils.LogUtils.format("MagicBarSocialController initialized"));
            }
            MagicBarSocialController.prototype.share = function () {
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/social/modal-share.html";
                modalSettings.size = "lg";
                modalSettings.controller = "controllers.SocialShareController as SocialShareController";
                modalSettings.resolve = {
                    targetGroupId: function () {
                        return "";
                    }
                };
                this.$modal.open(modalSettings);
            };
            return MagicBarSocialController;
        })();
        controllers.MagicBarSocialController = MagicBarSocialController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
