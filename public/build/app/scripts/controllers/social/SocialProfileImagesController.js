"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var SocialProfileImagesController = (function () {
            function SocialProfileImagesController($scope, ImageService, UsersService, SocketIOFactory, $stateParams, LoggedUserService) {
                var _this = this;
                this.classified = new Array();
                this.$scope = $scope;
                this.$stateParams = $stateParams;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.ImageService = ImageService;
                this.SocketIOFactory = SocketIOFactory;
                var userId = this.$stateParams.userId || "me";
                this.user = new smf.models.User();
                this.loggedUser = new smf.models.LoggedUser();
                this.user._id = userId;
                this.isLoggedUser = this.user._id == this.loggedUser._id;
                this.UsersService.getDetails("_id", userId).then(function (user) {
                    _this.user = user[0];
                    _this.UsersService.enhance(_this.user);
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.LoggedUserService.getUser().then(function (user) {
                    _this.loggedUser = user;
                    _this.isLoggedUser = _this.user._id == _this.loggedUser._id;
                });
                this.loadImages();
                console.debug(smf.utils.LogUtils.format("SocialProfileImagesController initialized"));
            }
            SocialProfileImagesController.prototype.loadImages = function () {
                var _this = this;
                this.UsersService.getImages("userId", this.user._id).then(function (images) {
                    _this.images = images;
                });
            };
            SocialProfileImagesController.prototype.remove = function (imageId) {
                var _this = this;
                if (confirm("Remover imagem?")) {
                    this.UsersService.removeImage(imageId).then(function () {
                        _this.loadImages();
                    });
                }
            };
            SocialProfileImagesController.prototype.addLike = function (elementId) {
                var _this = this;
                this.ImageService.addLike(elementId).then(function () {
                    _this.loadImages();
                });
            };
            SocialProfileImagesController.prototype.removeLike = function (elementId) {
                var _this = this;
                this.ImageService.removeLike(elementId).then(function () {
                    _this.loadImages();
                });
            };
            return SocialProfileImagesController;
        })();
        controllers.SocialProfileImagesController = SocialProfileImagesController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
