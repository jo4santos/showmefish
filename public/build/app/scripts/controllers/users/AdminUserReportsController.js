"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminUserReportsController = (function () {
            function AdminUserReportsController($scope, UsersService) {
                this.$scope = $scope;
                this.UsersService = UsersService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminUserReportsController initialized"));
            }
            AdminUserReportsController.prototype.getList = function () {
                var _this = this;
                this.UsersService.getReports().then(function (reports) {
                    _this.reports = reports;
                });
            };
            AdminUserReportsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove report?")) {
                    this.UsersService.removeReport(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminUserReportsController;
        })();
        controllers.AdminUserReportsController = AdminUserReportsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
