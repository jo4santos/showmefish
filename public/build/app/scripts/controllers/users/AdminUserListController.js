"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminUserListController = (function () {
            function AdminUserListController($scope, UsersService, SocketIOFactory) {
                this.users = new Array();
                this.$scope = $scope;
                this.UsersService = UsersService;
                this.SocketIOFactory = SocketIOFactory;
                this.getList();
                this.setListeners();
                this.removed = this.added = this.updated = 0;
                console.debug(smf.utils.LogUtils.format("AdminUserListController initialized"));
            }
            AdminUserListController.prototype.getList = function () {
                var _this = this;
                var filters = new smf.models.UserFilters();
                this.UsersService.getAll(filters).then(function (users) {
                    _this.users = users;
                    _this.removed = _this.added = _this.updated = 0;
                });
            };
            AdminUserListController.prototype.remove = function (type, param) {
                if (confirm("Remove user?")) {
                    this.UsersService.remove(type, param).then(function () {
                        console.log("done");
                    });
                }
            };
            AdminUserListController.prototype.setListeners = function () {
                var _this = this;
                this.SocketIOFactory.on('users:removed', function () {
                    _this.removed++;
                });
                this.SocketIOFactory.on('users:added', function () {
                    _this.added++;
                });
                this.SocketIOFactory.on('users:updated', function () {
                    _this.updated++;
                });
            };
            AdminUserListController.prototype.enhance = function (element) {
                this.UsersService.enhance(element);
            };
            return AdminUserListController;
        })();
        controllers.AdminUserListController = AdminUserListController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
