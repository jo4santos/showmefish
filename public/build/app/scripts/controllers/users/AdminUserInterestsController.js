"use strict";
var smf;
(function (smf) {
    var controllers;
    (function (controllers) {
        var AdminUserInterestsController = (function () {
            function AdminUserInterestsController(UsersService) {
                this.UsersService = UsersService;
                this.getList();
                console.debug(smf.utils.LogUtils.format("AdminUserInterestsController initialized"));
            }
            AdminUserInterestsController.prototype.getList = function () {
                var _this = this;
                this.UsersService.getInterests(true).then(function (interests) {
                    _this.interests = interests;
                });
            };
            AdminUserInterestsController.prototype.remove = function (_id) {
                var _this = this;
                if (confirm("Remove option?")) {
                    this.UsersService.removeInterest(_id).then(function () {
                        _this.getList();
                    });
                }
            };
            AdminUserInterestsController.prototype.add = function () {
                var _this = this;
                var name = prompt("Nome do interesse?");
                if (name) {
                    this.UsersService.addInterest(name).then(function () {
                        _this.getList();
                    });
                }
            };
            AdminUserInterestsController.prototype.update = function (_id) {
                var _this = this;
                var name = prompt("Novo nome?");
                if (name) {
                    this.UsersService.updateInterest(_id, name).then(function () {
                        _this.getList();
                    });
                }
            };
            return AdminUserInterestsController;
        })();
        controllers.AdminUserInterestsController = AdminUserInterestsController;
    })(controllers = smf.controllers || (smf.controllers = {}));
})(smf || (smf = {}));
