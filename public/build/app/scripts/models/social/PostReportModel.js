var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smf;
(function (smf) {
    var models;
    (function (models) {
        var PostReport = (function (_super) {
            __extends(PostReport, _super);
            function PostReport() {
                _super.apply(this, arguments);
            }
            return PostReport;
        })(models.Report);
        models.PostReport = PostReport;
    })(models = smf.models || (smf.models = {}));
})(smf || (smf = {}));
