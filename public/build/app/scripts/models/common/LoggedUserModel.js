var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smf;
(function (smf) {
    var models;
    (function (models) {
        var LoggedUser = (function (_super) {
            __extends(LoggedUser, _super);
            function LoggedUser() {
                _super.apply(this, arguments);
            }
            return LoggedUser;
        })(models.User);
        models.LoggedUser = LoggedUser;
    })(models = smf.models || (smf.models = {}));
})(smf || (smf = {}));
