var smf;
(function (smf) {
    var models;
    (function (models) {
        var Views = (function () {
            function Views() {
            }
            return Views;
        })();
        models.Views = Views;
        var View = (function () {
            function View() {
            }
            return View;
        })();
        models.View = View;
    })(models = smf.models || (smf.models = {}));
})(smf || (smf = {}));
