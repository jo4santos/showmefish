var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smf;
(function (smf) {
    var models;
    (function (models) {
        var VideoReport = (function (_super) {
            __extends(VideoReport, _super);
            function VideoReport() {
                _super.apply(this, arguments);
            }
            return VideoReport;
        })(models.Report);
        models.VideoReport = VideoReport;
    })(models = smf.models || (smf.models = {}));
})(smf || (smf = {}));
