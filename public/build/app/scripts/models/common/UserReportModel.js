var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smf;
(function (smf) {
    var models;
    (function (models) {
        var UserReport = (function (_super) {
            __extends(UserReport, _super);
            function UserReport() {
                _super.apply(this, arguments);
            }
            return UserReport;
        })(models.Report);
        models.UserReport = UserReport;
    })(models = smf.models || (smf.models = {}));
})(smf || (smf = {}));
