var smf;
(function (smf) {
    var services;
    (function (services) {
        var ChatService = (function () {
            function ChatService($q, $http, UsersService, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
            }
            //
            //  General chat methods
            //
            ChatService.prototype.getAll = function (filters) {
                filters = filters || new smf.models.ChatFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/chats/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/chats/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.create = function (chat) {
                var deferred = this.$q.defer();
                this.$http.post('/data/chats/', chat).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/chats/' + param + '/' + type).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            ChatService.prototype.sendMessage = function (chatId, content) {
                var deferred = this.$q.defer();
                this.$http.post('/data/chats/message/' + chatId, { "content": content }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.addUsers = function (chatId, users) {
                var deferred = this.$q.defer();
                this.$http.post('/data/chats/users/' + chatId, { users: users }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            ChatService.prototype.setRead = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/chats/unread/' + _id).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.getUnread = function (_id) {
                var deferred = this.$q.defer();
                var _id = _id || "";
                this.$http.get('/data/chats/unread/' + _id).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ChatService.prototype.enhance = function (element) {
                var deferred = this.$q.defer();
                this.$q.all([
                    this.getUnread(element._id)
                ])
                    .then(function (response) {
                    element.unreadCount = angular.copy(response[0]);
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            ChatService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return ChatService;
        })();
        services.ChatService = ChatService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
