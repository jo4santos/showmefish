var smf;
(function (smf) {
    var services;
    (function (services) {
        var NotificationService = (function () {
            function NotificationService($http, $q, socketIOFactory, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.socketIOFactory = socketIOFactory;
                this.LoggedUserService = LoggedUserService;
            }
            NotificationService.prototype.getAll = function () {
                var _this = this;
                var deferred = this.$q.defer();
                this.$http.get('/data/notifications').success(function (response) {
                    _this.LoggedUserService.getUser().then(function (user) {
                        for (var i in response) {
                            if (user.notifications && parseInt(response[i].timestamp) <= parseInt(user.notifications.lastReadDate)) {
                                response[i].read = true;
                            }
                        }
                        deferred.resolve(response);
                    });
                });
                return deferred.promise;
            };
            NotificationService.prototype.setRead = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/notifications/read').success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            NotificationService.$inject = ['$http', '$q', 'SocketIOFactory', 'LoggedUserService'];
            return NotificationService;
        })();
        services.NotificationService = NotificationService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
