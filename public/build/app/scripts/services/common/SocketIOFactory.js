var smf;
(function (smf) {
    var services;
    (function (services) {
        var SocketIOFactory = (function () {
            function SocketIOFactory($rootScope) {
                this.$rootScope = $rootScope;
                this.url = document.URL.toLowerCase().replace("/public/index.html", "");
                if (this.url.indexOf("rhcloud.com") == -1) {
                    this.url = document.URL.toLowerCase().replace("/public/", "");
                }
                else {
                    this.url = "showmefish-josapps.rhcloud.com:8000";
                }
                this.socket = io.connect(this.url);
            }
            SocketIOFactory.prototype.on = function (eventName, callback) {
                this.socket.on(eventName, function () {
                    var _this = this;
                    var args = arguments;
                    this.$rootScope.$apply(function () {
                        callback.apply(_this.socket, args);
                    });
                });
            };
            SocketIOFactory.prototype.emit = function (eventName, data) {
                this.socket.emit(eventName, data);
            };
            SocketIOFactory.$inject = ['$rootScope', '$http'];
            return SocketIOFactory;
        })();
        services.SocketIOFactory = SocketIOFactory;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
