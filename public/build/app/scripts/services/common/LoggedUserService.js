var smf;
(function (smf) {
    var services;
    (function (services) {
        var LoggedUserService = (function () {
            function LoggedUserService($q, $http) {
                this.$q = $q;
                this.$http = $http;
            }
            LoggedUserService.prototype.load = function () {
                var _this = this;
                var deferred = this.$q.defer();
                this.$http.get('/data/loggeduser').success(function (response) {
                    _this.user = response;
                    deferred.resolve(_this.user);
                });
                return deferred.promise;
            };
            LoggedUserService.prototype.save = function (user) {
                var deferred = this.$q.defer();
                this.$http.put('/data/loggeduser', user).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            LoggedUserService.prototype.getUser = function (force) {
                var deferred = this.$q.defer();
                if (!this.user || force) {
                    return this.load();
                }
                else {
                    deferred.resolve(this.user);
                }
                return deferred.promise;
            };
            LoggedUserService.prototype.setProfileImage = function (cropData) {
                var deferred = this.$q.defer();
                this.$http.put('/data/loggeduser/profileImage', cropData).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            LoggedUserService.prototype.setProfileImageSocial = function (src) {
                var deferred = this.$q.defer();
                this.$http.put('/data/loggeduser/profileImage/social', { "src": src }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            LoggedUserService.prototype.unlinkGoogle = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/auth/unlink/google').success(function () {
                    deferred.resolve();
                });
                return deferred.promise;
            };
            LoggedUserService.prototype.unlinkFacebook = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/auth/unlink/facebook').success(function () {
                    deferred.resolve();
                });
                return deferred.promise;
            };
            LoggedUserService.$inject = ['$q', '$http'];
            return LoggedUserService;
        })();
        services.LoggedUserService = LoggedUserService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
