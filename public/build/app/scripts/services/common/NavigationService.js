var smf;
(function (smf) {
    var services;
    (function (services) {
        var NavigationService = (function () {
            function NavigationService($q, $http) {
                this.pages = [];
                this.$q = $q;
                this.$http = $http;
            }
            NavigationService.prototype.getList = function () {
                var deferred = this.$q.defer();
                var page = new smf.models.NavigationPage();
                page.text = "Users";
                page.url = "users";
                this.pages.push(page);
                for (var i = 0; i < 5; i++) {
                    var page = new smf.models.NavigationPage();
                    page.text = "Page " + i;
                    page.url = "page" + i;
                    page.children = [];
                    for (var j = 0; j < 3; j++) {
                        var child = new smf.models.NavigationPage();
                        child.text = "Page " + i + "." + j;
                        child.url = "page" + i + "." + j;
                        child.children = [];
                        page.children.push(child);
                    }
                    this.pages.push(page);
                }
                deferred.resolve(this.pages);
                return deferred.promise;
            };
            NavigationService.$inject = ['$q', '$http'];
            return NavigationService;
        })();
        services.NavigationService = NavigationService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
