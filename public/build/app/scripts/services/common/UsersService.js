var smf;
(function (smf) {
    var services;
    (function (services) {
        var UsersService = (function () {
            function UsersService($q, $http) {
                this.$q = $q;
                this.$http = $http;
            }
            //
            //  General user methods
            //
            UsersService.prototype.getAll = function (filters) {
                filters = filters || new smf.models.UserFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/users/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/users/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.create = function (user) {
                var deferred = this.$q.defer();
                this.$http.post('/data/users', { "user": user }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/users/' + type + '/' + param).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.enhance = function (element) {
                var deferred = this.$q.defer();
                if (!element.image) {
                    element.image = "img-fallback";
                }
                this.$q.all([
                    this.getInterestsObj(element.interests)
                ])
                    .then(function (response) {
                    element.interestsObj = angular.copy(response[0]);
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            //
            //  User interest methods
            //
            UsersService.prototype.getInterests = function (force) {
                var _this = this;
                var deferred = this.$q.defer();
                if (this.interests && !force) {
                    deferred.resolve(this.interests);
                }
                this.$http.get('/data/users/interests/').success(function (response) {
                    _this.interests = response;
                    deferred.resolve(_this.interests);
                });
                return deferred.promise;
            };
            UsersService.prototype.getInterest = function (_id) {
                var deferred = this.$q.defer();
                this.getInterests()
                    .then(function (response) {
                    for (var i in response) {
                        if (response[i]._id == _id) {
                            deferred.resolve(response[i]);
                        }
                    }
                    deferred.reject();
                });
                return deferred.promise;
            };
            UsersService.prototype.addInterest = function (name) {
                var deferred = this.$q.defer();
                this.$http.post('/data/users/interests', { "name": name }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.updateInterest = function (_id, name) {
                var deferred = this.$q.defer();
                this.$http.put('/data/users/interests', { "_id": _id, "name": name }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.removeInterest = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/users/interests/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.getInterestsObj = function (interests) {
                interests = interests || [];
                var deferred = this.$q.defer();
                if (interests.length == 0) {
                    deferred.resolve([]);
                }
                else {
                    this.$http.get('/data/users/interests/' + interests).success(function (response) {
                        deferred.resolve(response);
                    });
                }
                return deferred.promise;
            };
            //
            //  User report methods
            //
            UsersService.prototype.addReport = function (userId, reason) {
                var deferred = this.$q.defer();
                this.$http.post('/data/users/reports', { "userId": userId, "reason": reason }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.prototype.getReports = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/users/reports/').success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.removeReport = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/users/reports/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  User images methods
            //
            UsersService.prototype.getImages = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/users/images/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.removeImage = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/images/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  User videos methods
            //
            UsersService.prototype.getVideos = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/users/videos/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.removeVideo = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/videos/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            UsersService.$inject = ['$q', '$http'];
            return UsersService;
        })();
        services.UsersService = UsersService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
