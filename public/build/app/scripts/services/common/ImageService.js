var smf;
(function (smf) {
    var services;
    (function (services) {
        var ImageService = (function () {
            function ImageService($q, $http, UsersService, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
            }
            ImageService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/images/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response[0]);
                });
                return deferred.promise;
            };
            ImageService.prototype.enhance = function (element) {
                var _this = this;
                var deferred = this.$q.defer();
                this.$q.all([
                    this.UsersService.getDetails("_id", element.userId),
                    this.LoggedUserService.getUser()
                ])
                    .then(function (response) {
                    element.user = angular.copy(response[0][0]);
                    _this.UsersService.enhance(element.user);
                    var loggedUser = response[1];
                    //ToDo: Place this is a common function ??
                    for (var i in element.likes.data) {
                        if (element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }
                    for (var i in element.comments.data) {
                        for (var j in element.comments.data[i].likes.data) {
                            if (element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            //
            //  Image report methods
            //
            ImageService.prototype.addReport = function (imageId, reason) {
                var deferred = this.$q.defer();
                this.$http.post('/data/images/reports', { "imageId": imageId, "reason": reason }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            ImageService.prototype.getReports = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/images/reports/').success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.prototype.removeReport = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/images/reports/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  post likes methods
            //
            ImageService.prototype.addLike = function (elementId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/images/like/' + elementId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.prototype.removeLike = function (elementId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/images/like/' + elementId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.prototype.addLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/images/like/comment/' + commentId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.prototype.removeLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/images/like/comment/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            //
            //  post comments methods
            //
            ImageService.prototype.addComment = function (elementId, comment) {
                var deferred = this.$q.defer();
                this.$http.post('/data/images/comment/' + elementId, { "comment": comment }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.prototype.removeComment = function (elementId, commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/images/comment/' + elementId + '/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ImageService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return ImageService;
        })();
        services.ImageService = ImageService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
