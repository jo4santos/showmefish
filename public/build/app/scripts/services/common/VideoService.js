var smf;
(function (smf) {
    var services;
    (function (services) {
        var VideoService = (function () {
            function VideoService($q, $http, UsersService, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
            }
            VideoService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/videos/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response[0]);
                });
                return deferred.promise;
            };
            VideoService.prototype.create = function (video) {
                var deferred = this.$q.defer();
                if (!video) {
                    deferred.resolve("");
                }
                this.$http.post('/data/videos', video).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.enhance = function (element) {
                var _this = this;
                var deferred = this.$q.defer();
                this.$q.all([
                    this.UsersService.getDetails("_id", element.userId),
                    this.LoggedUserService.getUser()
                ])
                    .then(function (response) {
                    element.user = angular.copy(response[0][0]);
                    _this.UsersService.enhance(element.user);
                    var loggedUser = response[1];
                    //ToDo: Place this is a common function ??
                    for (var i in element.likes.data) {
                        if (element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }
                    for (var i in element.comments.data) {
                        for (var j in element.comments.data[i].likes.data) {
                            if (element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            //
            //  Image report methods
            //
            VideoService.prototype.addReport = function (videoId, reason) {
                var deferred = this.$q.defer();
                this.$http.post('/data/videos/reports', { "videoId": videoId, "reason": reason }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            VideoService.prototype.getReports = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/videos/reports/').success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.removeReport = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/videos/reports/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  post likes methods
            //
            VideoService.prototype.addLike = function (elementId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/videos/like/' + elementId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.removeLike = function (elementId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/videos/like/' + elementId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.addLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/videos/like/comment/' + commentId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.removeLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/videos/like/comment/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            //
            //  post comments methods
            //
            VideoService.prototype.addComment = function (elementId, comment) {
                var deferred = this.$q.defer();
                this.$http.post('/data/videos/comment/' + elementId, { "comment": comment }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.prototype.removeComment = function (elementId, commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/videos/comment/' + elementId + '/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            VideoService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return VideoService;
        })();
        services.VideoService = VideoService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
