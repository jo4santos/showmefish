var smf;
(function (smf) {
    var services;
    (function (services) {
        var ClassifiedService = (function () {
            function ClassifiedService($q, $http) {
                this.$q = $q;
                this.$http = $http;
            }
            //
            //  General classified methods
            //
            ClassifiedService.prototype.getAll = function (filters, sortOptions, paginationOptions) {
                filters = filters || new smf.models.ClassifiedFilters();
                sortOptions = sortOptions || new smf.models.ClassifiedSortOptions();
                paginationOptions = paginationOptions || new smf.models.ClassifiedPaginationOptions();
                var params = {};
                params.filters = filters;
                params.sort = sortOptions;
                params.pagination = paginationOptions;
                var deferred = this.$q.defer();
                this.$http.get('/data/classified/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ClassifiedService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/classified/details/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            ClassifiedService.prototype.create = function (classified) {
                var deferred = this.$q.defer();
                this.$http.post('/data/classified', { "classified": classified }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            ClassifiedService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/classified/' + param + '/' + type).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            ClassifiedService.prototype.getImages = function (images) {
                var deferred = this.$q.defer();
                if (images.length == 0) {
                    deferred.resolve([]);
                }
                else {
                    this.$http.get('/data/images/' + images).success(function (response) {
                        deferred.resolve(response);
                    });
                }
                return deferred.promise;
            };
            ClassifiedService.prototype.enhance = function (element) {
                var deferred = this.$q.defer();
                this.$q.all([
                    this.getCategory(element.category),
                    this.getImages(element.images)
                ])
                    .then(function (response) {
                    console.log(response);
                    element.categoryObj = angular.copy(response[0]);
                    element.imagesObj = angular.copy(response[1]);
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            //
            //  Classified category methods
            //
            ClassifiedService.prototype.getCategories = function (force) {
                var _this = this;
                var deferred = this.$q.defer();
                if (this.categories && !force) {
                    deferred.resolve(this.categories);
                }
                else {
                    this.$http.get('/data/classified/categories/').success(function (response) {
                        _this.categories = response;
                        deferred.resolve(_this.categories);
                    });
                }
                return deferred.promise;
            };
            ClassifiedService.prototype.getCategory = function (_id) {
                var deferred = this.$q.defer();
                this.getCategories()
                    .then(function (categories) {
                    categories.forEach(function (category) {
                        category.children.forEach(function (child) {
                            if (child._id == _id) {
                                deferred.resolve(child);
                            }
                        });
                    });
                    deferred.reject();
                });
                return deferred.promise;
            };
            ClassifiedService.prototype.addCategory = function (name, parentId) {
                return this.$http.post('/data/classified/categories', { "parentId": parentId, "name": name });
            };
            ClassifiedService.prototype.updateCategory = function (_id, name, parentId) {
                return this.$http.put('/data/classified/categories', { "_id": _id, "name": name, "parentId": parentId });
            };
            ClassifiedService.prototype.removeCategory = function (_id, parentId) {
                return this.$http.delete('/data/classified/categories/' + _id + "/" + parentId);
            };
            //
            //  Classified report methods
            //
            ClassifiedService.prototype.addReport = function (classifiedId, reason) {
                return this.$http.post('/data/classified/reports', { "classifiedId": classifiedId, "reason": reason });
            };
            ClassifiedService.prototype.getReports = function () {
                return this.$http.get('/data/classified/reports/');
            };
            ClassifiedService.prototype.removeReport = function (_id) {
                return this.$http.delete('/data/classified/reports/' + _id);
            };
            ClassifiedService.$inject = ['$q', '$http'];
            return ClassifiedService;
        })();
        services.ClassifiedService = ClassifiedService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
