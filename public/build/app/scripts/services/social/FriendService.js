var smf;
(function (smf) {
    var services;
    (function (services) {
        var FriendService = (function () {
            function FriendService($q, $http, UsersService, LoggedUserService, $modal) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
                this.$modal = $modal;
            }
            FriendService.prototype.getAll = function (filters) {
                filters = filters || new smf.models.UserFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/users/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            FriendService.prototype.invite = function (userId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/friends/' + userId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            FriendService.prototype.accept = function (userId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/friends/action/' + userId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            FriendService.prototype.remove = function (userId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/friends/' + userId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            FriendService.prototype.select = function (excluded) {
                var _this = this;
                var deferred = this.$q.defer();
                excluded = excluded || [];
                var modalSettings = {};
                modalSettings.templateUrl = "app/partials/common/modal-select-users.html";
                modalSettings.controller = "controllers.ModalSelectUsersController as ModalSelectUsersController";
                modalSettings.resolve = {
                    DataService: function () {
                        return _this;
                    },
                    excluded: function () {
                        return excluded;
                    },
                    description: function () {
                        return "Seleccione os amigos que pretende adicionar ao grupo";
                    },
                    title: function () {
                        return "Seleccione os seus amigos";
                    }
                };
                this.$modal.open(modalSettings).result.then(function (selectedItems) {
                    deferred.resolve(selectedItems);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            FriendService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return FriendService;
        })();
        services.FriendService = FriendService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
