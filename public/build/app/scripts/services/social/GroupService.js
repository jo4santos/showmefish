var smf;
(function (smf) {
    var services;
    (function (services) {
        var GroupService = (function () {
            function GroupService($q, $http, UsersService, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
            }
            //
            //  General group methods
            //
            GroupService.prototype.getAll = function (filters) {
                filters = filters || new smf.models.GroupFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/groups/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            GroupService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/groups/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            GroupService.prototype.create = function (group) {
                var deferred = this.$q.defer();
                this.$http.post('/data/groups', group).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/groups/' + type + '/' + param).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  General user actions
            //
            GroupService.prototype.join = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/groups/subscription/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.leave = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/groups/subscription/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.acceptRequest = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/groups/subscription/action/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.denyRequest = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/groups/subscription/action/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.addUsers = function (groupId, userId, users) {
                var deferred = this.$q.defer();
                this.$http.post('/data/groups/subscription/' + groupId, { userId: userId, users: users }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            //
            //  General group admin actions
            //
            GroupService.prototype.addAdmin = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/groups/admin/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.prototype.removeAdmin = function (groupId, userId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/groups/admin/' + groupId + '/' + userId, {}).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            GroupService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return GroupService;
        })();
        services.GroupService = GroupService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
