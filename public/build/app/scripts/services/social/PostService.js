var smf;
(function (smf) {
    var services;
    (function (services) {
        var PostService = (function () {
            function PostService($q, $http, UsersService, LoggedUserService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
                this.LoggedUserService = LoggedUserService;
            }
            //
            //  General post methods
            //
            PostService.prototype.getAll = function (filters) {
                filters = filters || new smf.models.PostFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/posts/', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/posts/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response[0]);
                });
                return deferred.promise;
            };
            PostService.prototype.create = function (post) {
                var deferred = this.$q.defer();
                this.$http.post('/data/posts', post).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            PostService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/posts/' + param + '/' + type).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            PostService.prototype.getImages = function (images) {
                var deferred = this.$q.defer();
                if (images.length == 0) {
                    deferred.resolve([]);
                }
                else {
                    this.$http.get('/data/images/' + images).success(function (response) {
                        deferred.resolve(response);
                    });
                }
                return deferred.promise;
            };
            PostService.prototype.getVideo = function (video) {
                var deferred = this.$q.defer();
                if (!video || video == "") {
                    deferred.resolve();
                }
                else {
                    this.$http.get('/data/videos/' + video).success(function (response) {
                        deferred.resolve(response[0]);
                    });
                }
                return deferred.promise;
            };
            PostService.prototype.enhance = function (element) {
                var _this = this;
                var deferred = this.$q.defer();
                var promises = [];
                promises.push(this.UsersService.getDetails("_id", element.userId));
                promises.push(this.LoggedUserService.getUser());
                promises.push(this.getImages(element.images));
                promises.push(this.getVideo(element.video));
                if (element.targetUserId) {
                    promises.push(this.UsersService.getDetails("_id", element.targetUserId));
                }
                this.$q.all(promises)
                    .then(function (response) {
                    var loggedUser = response[1];
                    element.user = angular.copy(response[0][0]);
                    element.imagesObj = angular.copy(response[2]);
                    element.videoObj = angular.copy(response[3]);
                    if (response[4]) {
                        element.targetUser = angular.copy(response[4][0]);
                    }
                    for (var i in element.likes.data) {
                        if (element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }
                    for (var i in element.comments.data) {
                        for (var j in element.comments.data[i].likes.data) {
                            if (element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }
                    _this.UsersService.enhance(element.user);
                    if (element.targetUser) {
                        _this.UsersService.enhance(element.targetUser);
                    }
                    deferred.resolve(element);
                });
                return deferred.promise;
            };
            //
            //  post likes methods
            //
            PostService.prototype.addLike = function (postId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/posts/like/' + postId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.removeLike = function (postId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/posts/like/' + postId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.addLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.post('/data/posts/like/comment/' + commentId, {}).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.removeLikeComment = function (commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/posts/like/comment/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            //
            //  post comments methods
            //
            PostService.prototype.addComment = function (postId, comment) {
                var deferred = this.$q.defer();
                this.$http.post('/data/posts/comment/' + postId, { "comment": comment }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.removeComment = function (postId, commentId) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/posts/comment/' + postId + '/' + commentId).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            //
            //  Post report methods
            //
            PostService.prototype.addReport = function (postId, reason) {
                var deferred = this.$q.defer();
                this.$http.post('/data/posts/reports', { "postId": postId, "reason": reason }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            PostService.prototype.getReports = function () {
                var deferred = this.$q.defer();
                this.$http.get('/data/posts/reports/').success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            PostService.prototype.removeReport = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/posts/reports/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            PostService.$inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];
            return PostService;
        })();
        services.PostService = PostService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
