var smf;
(function (smf) {
    var services;
    (function (services) {
        var BlogsService = (function () {
            function BlogsService($q, $http, UsersService) {
                this.$q = $q;
                this.$http = $http;
                this.UsersService = UsersService;
            }
            BlogsService.prototype.getList = function (filters) {
                filters = filters || new smf.models.BlogFilters();
                var params = {};
                params.filters = filters;
                var deferred = this.$q.defer();
                this.$http.get('/data/blogs', { params: params }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            BlogsService.prototype.remove = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/blogs/' + param + '/' + type).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            BlogsService.prototype.create = function (blog) {
                return this.$http.post('/data/blogs', blog);
            };
            BlogsService.prototype.getDetails = function (type, param) {
                var deferred = this.$q.defer();
                this.$http.get('/data/blogs/' + param + '/' + type).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;
            };
            BlogsService.prototype.refreshPosts = function () {
                return this.$http.get('/data/blogs/refresh');
            };
            //
            //  Blogs theme methods
            //
            BlogsService.prototype.getThemes = function (force) {
                var _this = this;
                var deferred = this.$q.defer();
                if (this.themes && !force) {
                    deferred.resolve(this.themes);
                }
                this.UsersService.getInterests(true).then(function (response) {
                    _this.themes = response;
                    deferred.resolve(_this.themes);
                });
                return deferred.promise;
            };
            BlogsService.prototype.getCategory = function (_id) {
                var deferred = this.$q.defer();
                this.getThemes()
                    .then(function (themes) {
                    for (var i in themes) {
                        if (themes[i]._id == _id) {
                            deferred.resolve(themes[i]);
                        }
                    }
                    deferred.reject();
                });
                return deferred.promise;
            };
            BlogsService.prototype.addTheme = function (name) {
                var deferred = this.$q.defer();
                this.$http.post('/data/blogs/themes', { "name": name }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            BlogsService.prototype.updateTheme = function (_id, name) {
                var deferred = this.$q.defer();
                this.$http.put('/data/blogs/themes', { "_id": _id, "name": name }).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            BlogsService.prototype.removeTheme = function (_id) {
                var deferred = this.$q.defer();
                this.$http.delete('/data/blogs/themes/' + _id).success(function (response) {
                    deferred.resolve("OK");
                });
                return deferred.promise;
            };
            BlogsService.$inject = ['$q', '$http', 'UsersService'];
            return BlogsService;
        })();
        services.BlogsService = BlogsService;
    })(services = smf.services || (smf.services = {}));
})(smf || (smf = {}));
