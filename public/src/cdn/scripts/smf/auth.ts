$(".smf-auth-form").submit((e:JQueryEventObject) => {
    e.preventDefault();

    var form:JQuery;
    form = $(e.target);

    $(".smf-auth-error-message,.smf-auth-success-message").remove();

    $.post(form.attr("action"), form.serialize())
        .then(() => {
            if(form.attr("action") == "/data/auth/recover") {
                var errorObject:JQuery;
                errorObject = $("<div></div>")
                    .addClass("alert alert-success smf-auth-success-message")
                    .html("<i class='fa fa-check'></i> " + window["messages"]["success"]);
                $(".smf-auth-input:eq(0)").before(errorObject);
            }
            else {
                location.reload();
            }
        })
        .fail((data:JQueryXHR) => {
            var errorMessage:string;
            window["messages"] = window["messages"] || {};
            if(window["messages"][data.status]) {
                errorMessage = "<i class='fa fa-warning'></i> " + window["messages"][data.status];
            }
            else {
                errorMessage = "<i class='fa fa-warning'></i> " + window["messages"]["general"];
            }
            var errorObject:JQuery;
            errorObject = $("<div></div>")
                .addClass("alert alert-danger smf-auth-error-message")
                .html(errorMessage);
            $(".smf-auth-input:eq(0)").before(errorObject);
        })
})