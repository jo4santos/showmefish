"use strict"

module smf.controllers {

    export class CommonController {

        // Private

        private $scope: ICommonControllerScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private SocketIOFactory: services.SocketIOFactory;
        private LoggedUserService: services.LoggedUserService;
        private UsersService: services.UsersService;
        private ChatService: services.ChatService;
        private NotificationService: services.NotificationService;

        // Public attributes available to view
        public user: models.LoggedUser;
        public messagesUnread: string;
        public notifications: models.Notification[];
        public notificationsUnread: number;

        /**
         *
         * @param userApi
         */

        public constructor(
            $scope: ICommonControllerScope
            ,$modal: ng.ui.bootstrap.IModalService
            ,SocketIOFactory: services.SocketIOFactory
            ,UsersService: services.UsersService
            ,ChatService: services.ChatService
            ,LoggedUserService: services.LoggedUserService
            ,NotificationService: services.NotificationService
            ) {
            this.$scope = $scope;
            this.SocketIOFactory = SocketIOFactory;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.$modal = $modal;
            this.ChatService = ChatService;
            this.NotificationService = NotificationService;

            this.loadUser();
            this.loadMessagesUnread();
            this.loadNotifications();

            this.SocketIOFactory.on('chats:message:updated', () => {
                this.loadMessagesUnread();
            });
            this.SocketIOFactory.on('notifications:updated', () => {
                this.loadNotifications();
            })

            console.debug(smf.utils.LogUtils.format("CommonController initialized"));
        }

        public loadUser() {
            this.LoggedUserService.getUser().then((user:models.LoggedUser)=>{
                this.user = user;
                this.UsersService.enhance(this.user);
            });
        }

        public loadMessagesUnread() {
            this.ChatService.getUnread().then((unread:string)=>{
                this.messagesUnread = unread;
            });
        }

        public loadNotifications() {
            this.NotificationService.getAll().then((notifications: models.Notification[])=>{
                this.notifications = notifications;
                this.notificationsUnread = this.notifications.filter(function( notification ) {
                    return !notification.read;
                }).length;
            })
        }

        public setNotificationsRead() {
            this.NotificationService.setRead();
        }

        public changeProfileImage () {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-profile-change-image.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.ProfileChangeImageController as ProfileChangeImageController";
            this.$modal.open(modalSettings);
        }

        public reportClassified(classifiedId:string) {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/classified/modal-report.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.ClassifiedReportController as ClassifiedReportController";
            modalSettings.resolve = {
                classifiedId: function () {
                    return classifiedId;
                }
            }
            this.$modal.open(modalSettings);
        }

        public reportUser(userId:string) {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-report-user.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.UserReportController as UserReportController";
            modalSettings.resolve = {
                userId: function () {
                    return userId;
                }
            }
            this.$modal.open(modalSettings);
        }

        public reportImage(imageId:string) {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-report-image.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.ImageReportController as ImageReportController";
            modalSettings.resolve = {
                imageId: function () {
                    return imageId;
                }
            }
            this.$modal.open(modalSettings);
        }
    }

    interface ICommonControllerScope extends ng.IScope {
    }
}