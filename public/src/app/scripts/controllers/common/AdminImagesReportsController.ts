"use strict"

module smf.controllers {

    export class AdminImagesReportsController {

        private $scope:IAdminImagesReportsScope;
        private ImageService:services.ImageService;

        public reports:models.ImageReport[];

        public constructor($scope:IAdminImagesReportsScope, ImageService:services.ImageService) {
            this.$scope = $scope;
            this.ImageService = ImageService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminImagesReportsController initialized"));
        }

        public getList() {
            this.ImageService.getReports().then((reports: models.ImageReport[])=> {
                this.reports = reports;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove report?")) {
                this.ImageService.removeReport(_id).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminImagesReportsScope extends ng.IScope {
    }
}
