"use strict"

module smf.controllers {

    export class NotificationsController {

        private NotificationService:services.NotificationService;
        private SocketIOFactory:services.SocketIOFactory;

        public notifications:models.Notification[];


        public constructor(NotificationService:services.NotificationService, SocketIOFactory:services.SocketIOFactory) {
            this.NotificationService = NotificationService;
            this.SocketIOFactory = SocketIOFactory;

            this.notifications = [];

            this.setRead();
            this.getList();

            this.SocketIOFactory.on('notifications:updated', () => {
                this.getList();
            })

            console.debug(smf.utils.LogUtils.format("NotificationController initialized"));
        }

        public getList() {
            this.NotificationService.getAll()
                .then((notifications:models.Notification[])=> {
                    this.notifications = notifications;
                });
        }

        public setRead() {
            this.NotificationService.setRead();
        }
    }
}
