"use strict"

module smf.controllers {

    export class SettingsController {

        // Private

        private $scope: ISettingsControllerScope;
        private LoggedUserService: services.LoggedUserService;
        private UsersService: services.UsersService;

        // Public attributes available to view
        public user: models.LoggedUser;
        public form:ng.IFormController;
        public interests: models.UserInterest[];
        public newPassword: string;
        public selectedInterests: any[];

        public constructor(
            $scope: ISettingsControllerScope,
            LoggedUserService: services.LoggedUserService,
            UsersService: services.UsersService
            ) {
            this.$scope = $scope;
            this.LoggedUserService = LoggedUserService;
            this.UsersService = UsersService;
            this.newPassword = "";
            this.UsersService.getInterests(true).then((interests:models.UserInterest[]) => {
                this.interests = interests;
                this.selectedInterests = [];
                for(var i in this.interests) {
                    this.selectedInterests[this.interests[i]._id] = false;
                }
                this.LoggedUserService.getUser(true).then((user:models.LoggedUser)=>{
                    this.user = user;
                    for(var i in this.user.interests) {
                        this.selectedInterests[this.user.interests[i]] = true;
                    }
                });
            })

            console.debug(smf.utils.LogUtils.format("SettingsController initialized"));
        }

        public submit() {
            if (!this.form.$invalid) {
                this.user.interests = [];
                for(var i in this.selectedInterests) {
                    if(this.selectedInterests[i] == true) {
                        this.user.interests.push(angular.copy(i))
                    }
                }

                if(this.newPassword != "") {
                    this.user.password = angular.copy(this.newPassword);
                }

                this.LoggedUserService.save(this.user).then(() => {
                    location.reload();
                });
            }
            else {
                alert("NOK");
            }
        }

        public unlinkGoogle() {
            this.LoggedUserService.unlinkGoogle().then(()=>{
                location.reload();
            })
        }

        public unlinkFacebook() {
            this.LoggedUserService.unlinkFacebook().then(()=>{
                location.reload();
            })
        }
    }

    interface ISettingsControllerScope extends ng.IScope {
        classifiedShareForm: ng.IFormController;
        form: ng.IFormController;
        options: any;
    }
}