"use strict"

module smf.controllers {

    export class AdminVideosReportsController {

        private VideoService:services.VideoService;

        public reports:models.VideoReport[];

        public constructor(VideoService:services.VideoService) {
            this.VideoService = VideoService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminVideosReportsController initialized"));
        }

        public getList() {
            this.VideoService.getReports().then((reports: models.VideoReport[])=> {
                this.reports = reports;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove report?")) {
                this.VideoService.removeReport(_id).then(()=> {
                    this.getList();
                });
            }
        }
    }
}
