"use strict"

module smf.controllers {

    export class ChatsController {

        private $scope:IChatsScope;
        private ChatService:services.ChatService;
        private SocketIOFactory:services.SocketIOFactory;

        public chats:models.Chat[];
        public filters:models.ChatFilters;


        public constructor($scope:IChatsScope, ChatService:services.ChatService, SocketIOFactory:services.SocketIOFactory) {
            this.$scope = $scope;
            this.ChatService = ChatService;
            this.SocketIOFactory = SocketIOFactory;

            this.chats = [];

            this.getList();

            this.setListeners();

            console.debug(smf.utils.LogUtils.format("ChatsController initialized"));
        }

        private setListeners() {
            this.SocketIOFactory.on('chats:added', () => {
                this.getList();
            });
            this.SocketIOFactory.on('chats:removed', () => {
                this.getList();
            });
            this.SocketIOFactory.on('chats:user:added', () => {
                this.getList();
            })
            this.SocketIOFactory.on('chats:message:updated', () => {
                this.getList();
            })
        }

        public getList() {
            this.ChatService.getAll(this.filters)
                .then((chats:models.Chat[])=> {
                    this.chats = chats;
                    for(var i in this.chats) {
                        this.ChatService.enhance(this.chats[i]);
                    }
                });
        }

        public createChat() {
            var name: string = prompt("Nome do chat?");
            if(name) {
                var chat: models.Chat = new models.Chat();
                chat.name = name;
                this.ChatService.create(chat).then(() => {
                    this.getList();
                });
            }
        }

        public removeChat(chatId: string) {
            if(confirm("Apagar chat? Todos os conteúdos serão removidos do servidor." + chatId)) {
                this.ChatService.remove("_id",chatId).then(() => {
                    this.getList();
                });
            }
        }
    }

    interface IChatsScope extends ng.IScope {
        chats: models.Chat[];
    }
}
