"use strict"

module smf.controllers {

    export class UserReportController {

        // Private

        private $scope:IUserReportsControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private UsersService:services.UsersService;

        // Public attributes available to view

        public form:ng.IFormController;

        public user:models.User;
        public userId: string;

        public reason: string;

        public submitted: boolean;

        public constructor($scope:IUserReportsControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, UsersService:services.UsersService, userId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.SocketIOFactory = SocketIOFactory;
            this.UsersService = UsersService;

            this.userId = userId;

            this.user = new models.User();
            this.reason = "";

            console.debug(smf.utils.LogUtils.format("UserReportController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.UsersService.addReport(this.userId,this.reason).then(() => {
                    this.$modalInstance.dismiss();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface IUserReportsControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}