"use strict"

module smf.controllers {

    export class ImageViewerController {

        // Private

        private $scope:IImageViewerControllerScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private ImageService:services.ImageService;
        private UsersService:services.UsersService;
        private LoggedUserService:services.LoggedUserService;

        // Public attributes available to view

        public form:ng.IFormController;

        public image:models.Image;
        public imageId: string;

        public submitted: boolean;

        public comment: string;
        public loggedUser: models.LoggedUser;

        public constructor($scope:IImageViewerControllerScope,$modal: ng.ui.bootstrap.IModalService, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, ImageService:services.ImageService, UsersService:services.UsersService, LoggedUserService: services.LoggedUserService, imageId: string) {
            this.$scope = $scope;
            this.$modal = $modal;
            this.$modalInstance = $modalInstance;
            this.ImageService = ImageService;
            this.LoggedUserService = LoggedUserService;
            this.UsersService = UsersService;

            this.image = new models.Image();
            this.image._id = imageId;

            this.loggedUser = new models.LoggedUser();

            this.LoggedUserService.getUser().then((user:models.LoggedUser)=> {
                this.loggedUser = user;
            });

            this.load();

            console.debug(smf.utils.LogUtils.format("ImageViewerController initialized"));
        }

        public load () {
            this.comment = "";

            this.ImageService.getDetails("_id",this.image._id).then((image:models.Image)=>{
                this.image = image;
                this.ImageService.enhance(this.image);

                for(var i in this.image.comments.data) {
                    this.UsersService.getDetails("_id",this.image.comments.data[i].userId).then((user)=> {
                        for(var j in this.image.comments.data) {
                            if(this.image.comments.data[j].userId == user[0]._id) {
                                this.image.comments.data[j].user = user[0];
                            }
                        }
                    });
                }
            })

        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.ImageService.addComment(this.image._id,this.comment).then(()=> {
                this.load();
            });
        }

        public toggleLike() {
            if(this.image.likes.userLiked) {
                this.ImageService.removeLike(this.image._id).then(()=>{
                    this.load();
                });
            }
            else {
                this.ImageService.addLike(this.image._id).then(()=>{
                    this.load();
                });
            }
        }

        public report() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-report-image.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.ImageReportController as ImageReportController";
            modalSettings.resolve = {
                imageId: () => {
                    return this.image._id;
                }
            }
            this.$modal.open(modalSettings);
        }

        public setCover() {
            if (confirm("Escolher como imagem de capa?")) {
                this.loggedUser.coverImage = this.image._id;
                this.LoggedUserService.save(this.loggedUser).then(()=> {
                    location.reload();
                });
            }
        }

        public removeComment(comment:models.Comment) {
            if (confirm("Remove comment?")) {
                this.ImageService.removeComment(this.image._id,comment._id).then(()=> {
                    this.load();
                });
            }
        }

        public toggleLikeComment(comment:models.Comment) {
            if(comment.likes.userLiked) {
                this.ImageService.removeLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
            else {
                this.ImageService.addLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
        }
    }

    interface IImageViewerControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}