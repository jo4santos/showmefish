"use strict"

module smf.controllers {

    export class UserLinkController {

        private $scope:IUserLinkScope;
        private UsersService: services.UsersService;

        public user: models.User;

        public constructor($scope:IUserLinkScope, UsersService:services.UsersService) {
            this.$scope = $scope;
            this.UsersService = UsersService;

            this.user = new models.User();
            this.user._id = this.$scope.userId;

            this.load();

            console.debug(smf.utils.LogUtils.format("UserLinkController initialized"));
        }

        private load() {
            this.UsersService.getDetails("_id",this.user._id).then((user: models.User) => {
                this.user = user[0];
            })
        }

    }

    interface IUserLinkScope extends ng.IScope {
        userId: string;
    }
}
