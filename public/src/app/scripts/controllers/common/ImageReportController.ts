"use strict"

module smf.controllers {

    export class ImageReportController {

        // Private

        private $scope:IImageReportControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private ImageService:services.ImageService;

        // Public attributes available to view

        public form:ng.IFormController;

        public image:models.Image;
        public imageId: string;

        public reason: string;

        public submitted: boolean;

        public constructor($scope:IImageReportControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, ImageService:services.ImageService, imageId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.SocketIOFactory = SocketIOFactory;
            this.ImageService = ImageService;

            this.imageId = imageId;

            this.image = new models.Image();
            this.reason = "";

            console.debug(smf.utils.LogUtils.format("ImageReportController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.ImageService.addReport(this.imageId,this.reason).then(() => {
                    this.$modalInstance.dismiss();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface IImageReportControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}