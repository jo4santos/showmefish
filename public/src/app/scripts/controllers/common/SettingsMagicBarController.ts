"use strict"

module smf.controllers {

    export class SettingsMagicBarController {

        // Private

        private $scope: ISettingsMagicBarControllerScope;

        // Public attributes available to view

        public constructor(
            $scope: ISettingsMagicBarControllerScope
            ) {
            this.$scope = $scope;

            console.debug(smf.utils.LogUtils.format("SettingsMagicBarController initialized"));
        }
    }

    interface ISettingsMagicBarControllerScope extends ng.IScope {
        classifiedShareForm: ng.IFormController;
    }
}