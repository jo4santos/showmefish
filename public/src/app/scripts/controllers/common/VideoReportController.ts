"use strict"

module smf.controllers {

    export class VideoReportController {

        // Private

        private $scope:IVideoReportControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private VideoService:services.VideoService;

        // Public attributes available to view

        public form:ng.IFormController;

        public video:models.Video;
        public videoId: string;

        public reason: string;

        public submitted: boolean;

        public constructor($scope:IVideoReportControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, VideoService:services.VideoService, videoId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.SocketIOFactory = SocketIOFactory;
            this.VideoService = VideoService;

            this.videoId = videoId;

            this.video = new models.Video();
            this.reason = "";

            console.debug(smf.utils.LogUtils.format("VideoReportController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.VideoService.addReport(this.videoId,this.reason).then(() => {
                    this.$modalInstance.dismiss();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface IVideoReportControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}