"use strict"

module smf.controllers {

    export class PostViewerController {

        // Private

        private $modal: ng.ui.bootstrap.IModalService;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private PostService:services.PostService;

        // Public attributes available to view

        public post:models.Post;
        public postId: string;

        public constructor($modal: ng.ui.bootstrap.IModalService, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, PostService:services.PostService, postId: string) {
            this.$modal = $modal;
            this.$modalInstance = $modalInstance;
            this.PostService = PostService;

            this.post = new models.Post();
            this.post._id = postId;

            this.load();

            console.debug(smf.utils.LogUtils.format("PostViewerController initialized"));
        }

        public load () {
            this.PostService.getDetails("_id",this.post._id).then((post:models.Post)=>{
                this.post = post;
            })

        }

        public cancel() {
            this.$modalInstance.dismiss();
        }
    }
}