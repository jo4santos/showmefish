"use strict"

module smf.controllers {

    export class NavigationController {

        // Private

        private $scope: INavigationControllerScope;
        private $location: ng.ILocationService;
        private SocketIOFactory: services.SocketIOFactory;
        private NavigationService: services.NavigationService;

        // Public attributes available to view
        public pages: models.NavigationPage[] = new Array();

        /**
         *
         * @param userApi
         */

        public constructor(
            $scope: INavigationControllerScope
            ,$location: ng.ILocationService
            ,NavigationService: services.NavigationService
            ,SocketIOFactory: services.SocketIOFactory
            ) {
            this.$scope = $scope;
            this.$location = $location;
            this.SocketIOFactory = SocketIOFactory;
            this.NavigationService = NavigationService;
            this.pages = [];
            this.getList();

            console.debug(smf.utils.LogUtils.format("NavigationController initialized"));
        }

        //
        // Private
        //

        public getList() {
            this.NavigationService.getList().then((pages)=>{
                this.pages = pages;
            });
        }
    }

    interface INavigationControllerScope extends ng.IScope {
        pages: models.NavigationPage[];
    }
}