"use strict"

module smf.controllers {

    export class ProfileChangeImageController {

        // Private

        private $scope:IProfileChangeImageControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;
        private $timeout:ng.ITimeoutService;
        private $upload:ng.angularFileUpload.IUploadService;
        private LoggedUserService:services.LoggedUserService;
        private Cropper: any;


        // Public attributes available to view
        public form:ng.IFormController;

        public submitted:boolean;

        public files:any;
        public filesProgress:any;

        private fileReaderSupported:any;
        public user:models.LoggedUser;

        public options:any;
        public cropEnabled: boolean;
        public previewDataUrl: any;

        public cropData: any;

        public constructor($scope:IProfileChangeImageControllerScope, $timeout: ng.ITimeoutService, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, $upload:ng.angularFileUpload.IUploadService, LoggedUserService:services.LoggedUserService, Cropper:any) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.$timeout = $timeout;
            this.$upload = $upload;
            this.SocketIOFactory = SocketIOFactory;
            this.LoggedUserService = LoggedUserService;
            this.Cropper = Cropper;

            this.files = [];

            this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);


            this.options = {
                maximize: true,
                aspectRatio: 1 / 1,
                done: (cropData) => {
                    this.cropData = cropData;
                }
            };

            this.loadUser();

            console.debug(smf.utils.LogUtils.format("ProfileChangeImageController initialized"));
        }

        public loadUser() {
            this.LoggedUserService.getUser(true).then((user:models.LoggedUser)=> {
                this.user = user;
            });
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public generateThumbs() {
            if (this.files.length > 0) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(this.files[i]);

                        fileReader.onload = ((file)=> {
                            return (e:any) => {
                                file.dataUrl = e.target.result;
                            }
                        })(this.files[i]);
                    }
                }
            }
        }

        public upload(file:any) {
            if(!file) return;
            this.files.upload = this.$upload.upload({
                url: "/data/images",
                file: file,
                fields: {title: file.title}
            }).progress((evt:any) => {
                console.log('progress: ' + this.filesProgress + '% ' + evt.config.file.name);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == evt.config.file.name) {
                        this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                        break;
                    }
                }
            }).success((data:any, status:any, headers:any, config:any) => {
                console.log('file ' + config.file.name + 'uploaded. Response: ' + data);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == config.file.name) {
                        this.files[i].progress = 100;
                        this.files[i].id = data;
                        this.user.image = this.files[i].id;
                        this.$timeout(((ctrl)=>{
                            return () => {
                                ctrl.showCropper();
                            }
                        })(this));

                        break;
                    }
                }
            });
        }

        public cancelUpload(file:any) {
            for (var i = 0; i < this.files.length; i++) {
                if (this.files[i].name == file.name) {

                    if (this.files[i].upload) {
                        this.files[i].upload.cancel();
                    }
                    else {
                        this.files.splice(i, 1);
                    }

                    break;
                }
            }
        }

        public submit() {
            var user = new models.User();
            user.image = this.files[0].id;
            this.LoggedUserService.save(user).then(() => {
                this.LoggedUserService.setProfileImage(this.cropData).then((response:string) => {
                    this.loadUser();
                    location.reload();
                });
            });
        }

        public showCropper () {
            this.$scope.$broadcast("showCrop");
            this.cropEnabled = true;
        }

        public setSocial(src: string) {
            this.LoggedUserService.setProfileImageSocial(src).then((response:string) => {
                this.loadUser();
                location.reload();
            });
        }
    }

    interface IProfileChangeImageControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}