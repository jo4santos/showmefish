"use strict"

module smf.controllers {

    export class ModalSelectUsersController {

        private $scope:IModalSelectUsersScope;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;
        private DataService:services.IUserBaseService;

        public users:models.User[];
        public selectedUsers:models.User[];

        public filters:models.UserFilters;
        public title: string;
        public description: string;
        public excluded: string[];

        public constructor($scope:IModalSelectUsersScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, DataService:services.IUserBaseService, title: string, description: string, excluded: string[]) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.DataService = DataService;

            this.title = title;
            this.description = description;
            this.excluded = excluded;
            this.selectedUsers = [];

            this.getList();

            console.debug(smf.utils.LogUtils.format("ModalSelectUsersController initialized"));
        }

        public getList() {
            this.DataService.getAll(this.filters).then((users: models.User[])=> {
                this.users = [];
                for(var i in users) {
                    if(this.excluded.indexOf(users[i]._id) === -1) {
                        this.users.push(users[i]);
                    }
                }
            });
        }

        public selectUser(user: models.User) {
            this.selectedUsers.push(user);
        }

        public removeUser(user: models.User) {
            for(var i in this.selectedUsers) {
                if(this.selectedUsers[i]._id == user._id) {
                    this.selectedUsers.splice(i,1);
                    return;
                }
            }
        }

        public isSelected(user: models.User) {
            for(var i in this.selectedUsers) {
                if(user._id == this.selectedUsers[i]._id) return true;
            }
            return false;
        }

        public confirm () {
            this.$modalInstance.close(this.selectedUsers);
        }

        public cancel () {
            this.$modalInstance.dismiss('cancel');
        }
    }

    interface IModalSelectUsersScope extends ng.IScope {
    }
}
