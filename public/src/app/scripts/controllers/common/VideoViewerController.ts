"use strict"

module smf.controllers {

    export class VideoViewerController {

        // Private

        private $scope:IVideoViewerControllerScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private VideoService:services.VideoService;
        private UsersService:services.UsersService;
        private LoggedUserService:services.LoggedUserService;

        // Public attributes available to view

        public form:ng.IFormController;

        public video:models.Video;
        public videoId: string;

        public submitted: boolean;

        public comment: string;
        public loggedUser: models.LoggedUser;

        public constructor($scope:IVideoViewerControllerScope,$modal: ng.ui.bootstrap.IModalService, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, VideoService:services.VideoService, UsersService:services.UsersService, LoggedUserService: services.LoggedUserService, videoId: string) {
            this.$scope = $scope;
            this.$modal = $modal;
            this.$modalInstance = $modalInstance;
            this.VideoService = VideoService;
            this.LoggedUserService = LoggedUserService;
            this.UsersService = UsersService;

            this.video = new models.Video();
            this.video._id = videoId;

            this.loggedUser = new models.LoggedUser();

            this.LoggedUserService.getUser().then((user:models.LoggedUser)=> {
                this.loggedUser = user;
            });

            this.load();

            console.debug(smf.utils.LogUtils.format("VideoViewerController initialized"));
        }

        public load () {
            this.comment = "";

            this.VideoService.getDetails("_id",this.video._id).then((video:models.Video)=>{
                this.video = video;
                this.VideoService.enhance(this.video);

                for(var i in this.video.comments.data) {
                    this.UsersService.getDetails("_id",this.video.comments.data[i].userId).then((user)=> {
                        for(var j in this.video.comments.data) {
                            if(this.video.comments.data[j].userId == user[0]._id) {
                                this.video.comments.data[j].user = user[0];
                            }
                        }
                    });
                }
            })

        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.VideoService.addComment(this.video._id,this.comment).then(()=> {
                this.load();
            });
        }

        public toggleLike() {
            if(this.video.likes.userLiked) {
                this.VideoService.removeLike(this.video._id).then(()=>{
                    this.load();
                });
            }
            else {
                this.VideoService.addLike(this.video._id).then(()=>{
                    this.load();
                });
            }
        }

        public report() {
            //todo
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-report-video.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.VideoReportController as VideoReportController";
            modalSettings.resolve = {
                videoId: () => {
                    return this.video._id;
                }
            }
            this.$modal.open(modalSettings);
        }

        public removeComment(comment:models.Comment) {
            if (confirm("Remove comment?")) {
                this.VideoService.removeComment(this.video._id,comment._id).then(()=> {
                    this.load();
                });
            }
        }

        public toggleLikeComment(comment:models.Comment) {
            if(comment.likes.userLiked) {
                this.VideoService.removeLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
            else {
                this.VideoService.addLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
        }
    }

    interface IVideoViewerControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}