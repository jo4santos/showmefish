"use strict"

module smf.controllers {

    export class ChatDetailsController {

        private $scope:IChatDetailsScope;
        private $stateParams:ISocialChatDetailsStateParams;
        private ChatService:services.ChatService;
        private FriendService:services.FriendService;
        private SocketIOFactory:services.SocketIOFactory;

        public chat:models.Chat;
        public content: string;

        public constructor($scope:IChatDetailsScope, ChatService:services.ChatService, FriendService:services.FriendService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialChatDetailsStateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.ChatService = ChatService;
            this.FriendService = FriendService;
            this.SocketIOFactory = SocketIOFactory;

            this.chat = new models.Chat();
            this.chat._id = this.$stateParams.chatId;

            this.content = "";

            this.load();

            this.setListeners();

            this.ChatService.setRead(this.chat._id);

            console.debug(smf.utils.LogUtils.format("ChatDetailsController initialized"));
        }

        private setListeners() {
            this.SocketIOFactory.on('chats:message:added', () => {
                this.load();
            })
            this.SocketIOFactory.on('chats:user:added', () => {
                this.load();
            })
            this.SocketIOFactory.on('chats:removed', () => {
                this.load();
            });
        }

        private load() {
            this.ChatService.getDetails("_id", this.chat._id).then((chat:models.Chat) => {
                this.chat = chat[0];
            })
        }

        public send() {
            this.ChatService.sendMessage(this.chat._id,this.content).then(() => {
                this.content = "";
                this.load();
            });
        }

        public addUsers() {
            this.FriendService.select(this.chat.users).then((users:string[])=> {
                this.ChatService.addUsers(this.chat._id, users).then(()=> {
                    this.load();
                })
            })
        }
    }

    interface IChatDetailsScope extends ng.IScope {
        messages: models.Chat[];
    }
    export interface ISocialChatDetailsStateParams extends ng.ui.IStateParams {
        chatId: string;
    }
}
