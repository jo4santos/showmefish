"use strict"

module smf.controllers {

    export class AdminClassifiedCategoriesController {

        private $scope:IAdminClassifiedCategoriesScope;
        private ClassifiedService:services.ClassifiedService;
        private SocketIOFactory:services.SocketIOFactory;

        public categories:models.ClassifiedCategory[];

        public constructor($scope:IAdminClassifiedCategoriesScope, ClassifiedService:services.ClassifiedService) {
            this.$scope = $scope;
            this.ClassifiedService = ClassifiedService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminClassifiedCategoriesController initialized"));
        }

        public getList() {
            this.ClassifiedService.getCategories(true).then((categories:models.ClassifiedCategory[])=> {
                this.categories = categories;
            });
        }

        public remove(_id:string, parentId?:string) {
            if (confirm("Remove category?")) {
                this.ClassifiedService.removeCategory(_id, parentId).then(()=> {
                    this.getList();
                });
            }
        }

        public add(parentId?:string) {
            var name:string = prompt("Category name?");
            if (name) {
                this.ClassifiedService.addCategory(name, parentId).then(()=> {
                    this.getList();
                });
            }
        }

        public update(_id:string, parentId:string) {
            var name:string = prompt("New name?");
            if (name) {
                this.ClassifiedService.updateCategory(_id, name, parentId).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminClassifiedCategoriesScope extends ng.IScope {
    }
}
