"use strict"

module smf.controllers {

    export class ClassifiedReportController {

        // Private

        private $scope:IClassifiedReportsControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private ClassifiedService:services.ClassifiedService;

        // Public attributes available to view

        public form:ng.IFormController;

        public classified:models.Classified;
        public classifiedId: string;

        public reason: string;

        public submitted: boolean;

        public constructor($scope:IClassifiedReportsControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, ClassifiedService:services.ClassifiedService, classifiedId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.SocketIOFactory = SocketIOFactory;
            this.ClassifiedService = ClassifiedService;

            this.classifiedId = classifiedId;

            this.classified = new models.Classified();
            this.reason = "";

            console.debug(smf.utils.LogUtils.format("ClassifiedReportController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.ClassifiedService.addReport(this.classifiedId,this.reason).then(() => {
                    this.$modalInstance.dismiss();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface IClassifiedReportsControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}