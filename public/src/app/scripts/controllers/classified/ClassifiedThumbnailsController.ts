"use strict"

module smf.controllers {

    export class ClassifiedThumbnailController {

        private ChatService: services.ChatService;
        private ClassifiedService: services.ClassifiedService;
        private SocketIOFactory: services.SocketIOFactory;
        private $modal: ng.ui.bootstrap.IModalService;
        private $scope: IClassifiedThumbnailsScope;

        public _id: string;
        public classified: models.Classified;

        public constructor(
            $scope: IClassifiedThumbnailsScope,
            ClassifiedService: services.ClassifiedService,
            SocketIOFactory: services.SocketIOFactory,
            $modal: ng.ui.bootstrap.IModalService,
            ChatService: services.ChatService
            ) {
            this.$scope = $scope;
            this.ChatService = ChatService;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;
            this.$modal = $modal;

            this.classified = <models.Classified>{};
            this.classified._id = this.$scope.classifiedId;

            this.getDetails();
        }

        public getDetails () {
            this.ClassifiedService.getDetails("_id", this.classified._id).then((classified)=>{
                this.classified = classified[0];
                this.ClassifiedService.enhance(this.classified);
            });
        }
    }

    interface IClassifiedThumbnailsScope extends ng.IScope {
        classifiedId: string;
    }
}
