"use strict"

module smf.controllers {

    export class MagicBarClassifiedController {

        // Private

        private $scope: IMagicBarClassifiedControllerScope;
        private $location: ng.ILocationService;
        private SocketIOFactory: services.SocketIOFactory;
        private $modal: ng.ui.bootstrap.IModalService;

        // Public attributes available to view

        /**
         *
         * @param userApi
         */

        public constructor(
            $scope: IMagicBarClassifiedControllerScope
            ,$location: ng.ILocationService
            ,$modal: ng.ui.bootstrap.IModalService
            ,SocketIOFactory: services.SocketIOFactory
            ) {
            this.$scope = $scope;
            this.$location = $location;
            this.$modal = $modal;
            this.SocketIOFactory = SocketIOFactory;

            console.debug(smf.utils.LogUtils.format("MagicBarClassifiedController initialized"));
        }

        public share() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/classified/modal-share.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.ClassifiedShareController as ClassifiedShareController";
            this.$modal.open(modalSettings);
        }
    }

    interface IMagicBarClassifiedControllerScope extends ng.IScope {
        classifiedShareForm: ng.IFormController;
    }
}