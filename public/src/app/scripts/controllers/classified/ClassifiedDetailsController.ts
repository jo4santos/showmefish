"use strict"

module smf.controllers {

    export class ClassifiedDetailsController {

        private $scope: IClassifiedDetailsScope;
        private ChatService: services.ChatService;
        private ClassifiedService: services.ClassifiedService;
        private SocketIOFactory: services.SocketIOFactory;
        private $stateParams: IClassifiedDetailsStateParams;
        private $modal: ng.ui.bootstrap.IModalService;

        public _id: string;
        public classified: models.Classified;

        public constructor(
            $scope: IClassifiedDetailsScope,
            ClassifiedService: services.ClassifiedService,
            SocketIOFactory: services.SocketIOFactory,
            $modal: ng.ui.bootstrap.IModalService,
            $stateParams: IClassifiedDetailsStateParams,
            ChatService: services.ChatService
            ) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.ChatService = ChatService;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;
            this.$modal = $modal;


            this._id = this.$stateParams._id;

            this.getDetails("_id",this._id);
        }

        public getDetails (type: string, param: string) {
            this.ClassifiedService.getDetails(type, param).then((classified)=>{
                this.classified = classified[0];
                this.ClassifiedService.enhance(this.classified);
            });
        }

        public sendMessage() {
            var message: string = prompt("Message?");
            if(message) {
                var chat: models.Chat = new models.Chat();
                chat.users = [this.classified.userId];
                chat.classifiedId = this.classified._id;
                this.ChatService.create(chat).then((id:string)=>{
                    this.ChatService.sendMessage(id,message).then(()=>{
                        alert("Mensagem enviada");
                    })
                })
            }
        }
    }

    interface IClassifiedDetailsScope extends ng.IScope {

    }

    interface IClassifiedDetailsStateParams extends ng.ui.IStateParams {
        _id: string;
    }
}
