"use strict"

declare var FileAPI;

module smf.controllers {

    export class ClassifiedShareController {

        // Private

        private $scope:IClassifiedShareControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;
        private $upload:ng.angularFileUpload.IUploadService;

        private ClassifiedService:services.ClassifiedService;

        // Public attributes available to view

        public testing:string;
        public form:ng.IFormController;

        public classified:models.Classified;
        public categories:models.ClassifiedCategory[];

        public submitted:boolean;

        public files:any;
        public filesProgress:any;

        private fileReaderSupported:any;

        public constructor($scope:IClassifiedShareControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, ClassifiedService:services.ClassifiedService, $upload:ng.angularFileUpload.IUploadService) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.$upload = $upload;
            this.SocketIOFactory = SocketIOFactory;
            this.ClassifiedService = ClassifiedService;

            this.classified = new models.Classified();

            this.files = [];

            this.ClassifiedService.getCategories()
                .then((categories:models.ClassifiedCategory[])=> {
                    this.categories = [];
                    categories.forEach((category)=>{
                        category.children.forEach((child)=>{
                            child["parent"] = category.name;
                            this.categories.push(<models.ClassifiedCategory>child)
                        })
                    })
                });

            this.classified.type = "sell";

            this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

            console.debug(smf.utils.LogUtils.format("ClassifiedShareController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public generateThumbs() {
            if (this.files.length > 0) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(this.files[i]);

                        fileReader.onload = ((file)=> {
                            return (e:any) => {
                                file.dataUrl = e.target.result;
                            }
                        })(this.files[i]);
                    }
                }
            }
        }

        public upload(file:any) {
            this.files.upload = this.$upload.upload({
                url: "/data/images",
                file: file,
                fields: {title:file.title,thumb:true}
            }).progress((evt:any) => {
                console.log('progress: ' + this.filesProgress + '% ' + evt.config.file.name);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == evt.config.file.name) {
                        this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                        break;
                    }
                }
            }).success((data:any, status:any, headers:any, config:any) => {
                console.log('file ' + config.file.name + 'uploaded. Response: ' + data);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == config.file.name) {
                        this.files[i].progress = 100;
                        this.files[i].id = data;
                        break;
                    }
                }
            });
        }

        public cancelUpload (file: any) {
            for (var i = 0; i < this.files.length; i++) {
                if (this.files[i].name == file.name) {

                    if(this.files[i].upload) {
                        this.files[i].upload.cancel();
                    }
                    else {
                        this.files.splice(i,1);
                    }

                    break;
                }
            }
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.classified.images = [];

                if(!this.files) this.files = [];

                if(this.files.length == 0) {
                    alert("Por favor envie pelo menos uma foto.");
                    return;
                }

                for (var i = 0; i < this.files.length; i++) {
                    if(!this.files[i].id) {
                        alert("Existe uma fotografia por enviar, confirme o envio ou cancele.");
                        return;
                    }
                    else {
                        this.classified.images.push((this.files[i].id));
                    }
                }

                this.classified.category = this.classified.categoryObj._id;

                this.ClassifiedService.create(this.classified).then(() => {
                    this.$modalInstance.dismiss();
                    alert("POST PARTILHADO COM SUCESSO");
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface IClassifiedShareControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}

interface Window {
    FileReader: any;
    FileAPI: any;
}