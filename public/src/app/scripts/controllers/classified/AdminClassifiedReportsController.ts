"use strict"

module smf.controllers {

    export class AdminClassifiedReportsController {

        private $scope:IAdminClassifiedReportsScope;
        private ClassifiedService:services.ClassifiedService;

        public reports:models.ClassifiedReport[];

        public constructor($scope:IAdminClassifiedReportsScope, ClassifiedService:services.ClassifiedService) {
            this.$scope = $scope;
            this.ClassifiedService = ClassifiedService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminClassifiedReportsController initialized"));
        }

        public getList() {
            this.ClassifiedService.getReports().then((reports: models.ClassifiedReport[])=> {
                this.reports = reports;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove report?")) {
                this.ClassifiedService.removeReport(_id).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminClassifiedReportsScope extends ng.IScope {
    }
}
