"use strict"

module smf.controllers {

    export class ClassifiedController {

        private $scope:IClassifiedScope;
        private ClassifiedService:services.ClassifiedService;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:IClassifiedStateParams;

        public classified:models.Classified[];
        public categories:models.ClassifiedCategory[]
        public added:number;
        public updated:number;
        public removed:number;

        public filters:models.ClassifiedFilters;
        public sortOptions:models.ClassifiedSortOptions;
        public paginationOptions:models.ClassifiedPaginationOptions;

        public sortIndex:number;
        public sortOptionsList: {
            title: string;
            field: string;
            direction: string;
        }[];

        public constructor($scope:IClassifiedScope, ClassifiedService:services.ClassifiedService, SocketIOFactory:services.SocketIOFactory, $stateParams:IClassifiedStateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;

            this.classified = [];
            this.categories = [];

            this.sortOptionsList = [];
            this.sortOptionsList.push({title:"Mais recentes",field:"_id",direction:"-1"});
            this.sortOptionsList.push({title:"Mais antigos",field:"_id",direction:"1"});
            this.sortOptionsList.push({title:"Mais caros",field:"price",direction:"-1"});
            this.sortOptionsList.push({title:"Mais baratos",field:"price",direction:"1"});
            this.sortOptionsList.push({title:"Mais vistos",field:"views.count",direction:"-1"});
            this.sortOptionsList.push({title:"Menos vistos",field:"views.count",direction:"1"});

            this.sortIndex = 0;

            this.filters = new models.ClassifiedFilters();
            if (this.$stateParams.type && this.$stateParams.type != "") {
                this.filters.type = this.$stateParams.type;
            }
            this.sortOptions = new models.ClassifiedSortOptions();
            this.paginationOptions = new models.ClassifiedPaginationOptions();
            this.paginationOptions.page = 1;
            this.paginationOptions.limit = 2;

            this.getCategories();
            this.getList();

            this.setListeners();

            this.removed = this.added = this.updated = 0;

            this.$scope.$watch('[ClassifiedController.sortIndex, ClassifiedController.paginationOptions.page, ClassifiedController.paginationOptions.limit]',()=>{
                this.getList();
            })

            console.debug(smf.utils.LogUtils.format("ClassifiedController initialized"));
        }

        public getList() {
            this.sortOptions.field = this.sortOptionsList[this.sortIndex].field;
            this.sortOptions.direction = this.sortOptionsList[this.sortIndex].direction;
            this.ClassifiedService.getAll(this.filters,this.sortOptions,this.paginationOptions)
                .then((classified)=> {
                    this.classified = classified;
                    for(var i in this.classified) {
                        this.ClassifiedService.enhance(this.classified[i])
                    }
                    this.removed = this.added = this.updated = 0;
                });
        }

        public getCategories() {
            this.ClassifiedService.getCategories()
                .then((categories:models.ClassifiedCategory[])=> {
                    this.categories = [];
                    categories.forEach((category)=>{
                        category.children.forEach((child)=>{
                            child["parent"] = category.name;
                            this.categories.push(<models.ClassifiedCategory>child)
                        })
                    })
                });
        }

        public remove(type:string, param:string) {
            if (confirm("Remove classified?")) {
                this.ClassifiedService.remove(type, param).then(()=> {
                    console.log("done");
                });
            }
        }

        private setListeners() {
            this.SocketIOFactory.on('classified:removed', () => {
                this.removed++;
            })
            this.SocketIOFactory.on('classified:added', () => {
                this.added++;
            })
            this.SocketIOFactory.on('classified:updated', () => {
                this.updated++;
            })
        }

        public applyFilters() {
            this.filters.category = "";
            if (this.filters.categoryObj) {
                this.filters.category = this.filters.categoryObj._id;
            }
            this.getList();
        }
    }

    interface IClassifiedScope extends ng.IScope {
        variable: string;
    }

    interface IClassifiedStateParams extends ng.ui.IStateParams {
        type: string;
    }
}
