"use strict"

module smf.controllers {

    export class AdminClassifiedListController {

        private $scope:IAdminClassifiedListScope;
        private ClassifiedService:services.ClassifiedService;
        private SocketIOFactory:services.SocketIOFactory;

        public classified:models.Classified[] = new Array();
        public added:number;
        public updated:number;
        public removed:number;

        public constructor($scope:IAdminClassifiedListScope, ClassifiedService:services.ClassifiedService, SocketIOFactory:services.SocketIOFactory) {
            this.$scope = $scope;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;

            this.getList();

            this.setListeners();

            this.removed = this.added = this.updated = 0;

            console.debug(smf.utils.LogUtils.format("AdminClassifiedListController initialized"));
        }

        public getList() {
            this.ClassifiedService.getAll(null, null, null).then((classified)=> {
                this.classified = classified;
                this.removed = this.added = this.updated = 0;
            });
        }

        public remove(type:string, param:string) {
            if (confirm("Remove classified?")) {
                this.ClassifiedService.remove(type, param).then(()=> {
                    console.log("done");
                });
            }
        }

        private setListeners() {
            this.SocketIOFactory.on('classified:removed', () => {
                this.removed++;
            })
            this.SocketIOFactory.on('classified:added', () => {
                this.added++;
            })
            this.SocketIOFactory.on('classified:updated', () => {
                this.updated++;
            })
        }

        public enhance(element:models.Classified) {
            this.ClassifiedService.enhance(element);
        }
    }

    interface IAdminClassifiedListScope extends ng.IScope {
    }
}
