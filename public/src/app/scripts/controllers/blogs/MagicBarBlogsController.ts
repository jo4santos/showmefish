"use strict"

module smf.controllers {

    export class MagicBarBlogsController {

        // Private

        private $modal: ng.ui.bootstrap.IModalService;
        private BlogsService: services.BlogsService;

        // Public attributes available to view

        public constructor(
            $modal: ng.ui.bootstrap.IModalService
            ,BlogsService: services.BlogsService
            ) {
            this.$modal = $modal;
            this.BlogsService = BlogsService;

            console.debug(smf.utils.LogUtils.format("MagicBarBlogsController initialized"));
        }

        public updatePosts() {
            this.BlogsService.refreshPosts();
        }

        public share() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/blogs/modal-share.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.BlogsShareController as BlogsShareController";
            this.$modal.open(modalSettings);
        }
    }
}