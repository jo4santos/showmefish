"use strict"

module smf.controllers {

    export class BlogsShareController {

        // Private

        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;
        private BlogsService:services.BlogsService;

        // Public attributes available to view

        public form:ng.IFormController;

        public blog:models.Blog;
        public themes:models.UserInterest[];

        public submitted:boolean;

        public constructor($modalInstance:ng.ui.bootstrap.IModalServiceInstance, BlogsService:services.BlogsService) {
            this.$modalInstance = $modalInstance;
            this.BlogsService = BlogsService;

            this.blog = new models.Blog();

            this.BlogsService.getThemes()
                .then((themes:models.UserInterest[])=> {
                    this.themes = themes;
                });

            console.debug(smf.utils.LogUtils.format("BlogsShareController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {

                this.blog.theme = this.blog.themeObj._id;

                this.BlogsService.create(this.blog).then(() => {
                    this.$modalInstance.dismiss();
                    alert("BLOG REGISTADO COM SUCESSO");
                });
            }
            else {
                alert("NOK");
            }
        }
    }
}