"use strict"

module smf.controllers {

    export class BlogsController {

        private BlogsService: services.BlogsService;
        private SocketIOFactory: services.SocketIOFactory;

        public blogs: models.Blog[] = [];
        public themes:models.UserInterest[] = [];

        public items: any[] = [];
        public filters:models.BlogFilters;

        public constructor( BlogsService: services.BlogsService, SocketIOFactory: services.SocketIOFactory ) {
            this.BlogsService = BlogsService;
            this.SocketIOFactory = SocketIOFactory;
            this.getList();

            this.SocketIOFactory.on('blogs:added',() => {
                this.getList();
            });
            this.SocketIOFactory.on('blogs:updated',() => {
                this.getList();
            });
            this.SocketIOFactory.on('blogs:removed',() => {
                this.getList();
            });

            this.BlogsService.getThemes()
                .then((themes:models.UserInterest[])=> {
                    this.themes = themes;
                });

            console.debug(smf.utils.LogUtils.format("BlogsController initialized"));
        }

        public getList() {
            this.items = [];
            this.BlogsService.getList(this.filters).then((blogs)=>{
                this.blogs = blogs;
                this.blogs.forEach((blog) => {
                    var item = blog.items[0];
                    item.blog = {};
                    item.blog.name = blog.name;
                    item.blog.description = blog.description;
                    item.blog.url = blog.url;
                    this.items.push(item);
                })
            });
        }
        public remove(type:string, param:string) {
            if(confirm("Remove blog?")) {
                this.BlogsService.remove(type, param).then(()=>{
                    console.log("done");
                });
            }
        }

        public applyFilters() {
            this.filters.theme = "";
            if (this.filters.themeObj) {
                this.filters.theme = this.filters.themeObj._id;
            }
            this.getList();
        }
    }
}
