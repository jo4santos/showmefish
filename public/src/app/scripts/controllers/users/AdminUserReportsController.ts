"use strict"

module smf.controllers {

    export class AdminUserReportsController {

        private $scope:IAdminUserReportsScope;
        private UsersService:services.UsersService;

        public reports:models.UserReport[];

        public constructor($scope:IAdminUserReportsScope, UsersService:services.UsersService) {
            this.$scope = $scope;
            this.UsersService = UsersService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminUserReportsController initialized"));
        }

        public getList() {
            this.UsersService.getReports().then((reports: models.UserReport[])=> {
                this.reports = reports;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove report?")) {
                this.UsersService.removeReport(_id).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminUserReportsScope extends ng.IScope {
    }
}
