"use strict"

module smf.controllers {

    export class AdminUserListController {

        private $scope:IAdminUserListScope;
        private UsersService:services.UsersService;
        private SocketIOFactory:services.SocketIOFactory;

        public users:models.User[] = new Array();
        public added:number;
        public updated:number;
        public removed:number;

        public constructor($scope:IAdminUserListScope, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory) {
            this.$scope = $scope;
            this.UsersService = UsersService;
            this.SocketIOFactory = SocketIOFactory;

            this.getList();

            this.setListeners();

            this.removed = this.added = this.updated = 0;

            console.debug(smf.utils.LogUtils.format("AdminUserListController initialized"));
        }

        public getList() {
            var filters = new models.UserFilters();
            this.UsersService.getAll(filters).then((users)=> {
                this.users = users;
                this.removed = this.added = this.updated = 0;
            });
        }

        public remove(type:string, param:string) {
            if (confirm("Remove user?")) {
                this.UsersService.remove(type, param).then(()=> {
                    console.log("done");
                });
            }
        }

        private setListeners() {
            this.SocketIOFactory.on('users:removed', () => {
                this.removed++;
            })
            this.SocketIOFactory.on('users:added', () => {
                this.added++;
            })
            this.SocketIOFactory.on('users:updated', () => {
                this.updated++;
            })
        }

        public enhance(element:models.User) {
            this.UsersService.enhance(element);
        }
    }

    interface IAdminUserListScope extends ng.IScope {
        variable: string;
    }
}
