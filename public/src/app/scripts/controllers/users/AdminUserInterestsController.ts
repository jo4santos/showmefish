"use strict"

module smf.controllers {

    export class AdminUserInterestsController {

        private UsersService:services.UsersService;

        public interests:models.UserInterest[];

        public constructor(UsersService:services.UsersService) {
            this.UsersService = UsersService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminUserInterestsController initialized"));
        }

        public getList() {
            this.UsersService.getInterests(true).then((interests: models.UserInterest[])=> {
                this.interests = interests;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove option?")) {
                this.UsersService.removeInterest(_id).then(()=> {
                    this.getList();
                });
            }
        }

        public add() {
            var name: string = prompt("Nome do interesse?");
            if(name) {
                this.UsersService.addInterest(name).then(()=> {
                    this.getList();
                });
            }
        }

        public update(_id:string) {
            var name: string = prompt("Novo nome?");
            if(name) {
                this.UsersService.updateInterest(_id,name).then(()=> {
                    this.getList();
                });
            }
        }
    }
}
