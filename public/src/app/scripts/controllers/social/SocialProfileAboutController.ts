"use strict"

module smf.controllers {

    export class SocialProfileAboutController {

        private $scope:ISocialProfileAboutScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:ISocialProfileAboutStateParams;
        private UsersService:services.UsersService;
        private ClassifiedService:services.ClassifiedService;
        private LoggedUserService:services.LoggedUserService;

        public user:models.User;
        public loggedUser:models.LoggedUser;
        public images: models.Image[];
        public videos: models.Video[];

        public isLoggedUser:boolean;
        public classified:models.Classified[] = new Array();

        public constructor($scope:ISocialProfileAboutScope, ClassifiedService:services.ClassifiedService, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialProfileAboutStateParams, LoggedUserService:services.LoggedUserService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;

            var userId = this.$stateParams.userId || "me";

            this.user = new models.User();
            this.loggedUser = new models.LoggedUser();

            this.isLoggedUser = this.user._id == this.loggedUser._id;

            this.UsersService.getDetails("_id", userId).then((user)=> {
                this.user = user[0];
                this.UsersService.enhance(this.user);
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });
            this.LoggedUserService.getUser().then((user:models.LoggedUser)=> {
                this.loggedUser = user;
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });
            this.UsersService.getImages("userId", userId).then((images:models.Image[])=> {
                this.images = images;
            });
            this.UsersService.getVideos("userId", userId).then((videos:models.Video[])=> {
                this.videos = videos;
            });

            var classifiedFilters: models.ClassifiedFilters = new models.ClassifiedFilters();
            classifiedFilters.userId = userId;
            this.ClassifiedService.getAll(classifiedFilters,{field:"views.count",direction:"-1"},null)
                .then((classified)=> {
                    this.classified = classified;
                });

            console.debug(smf.utils.LogUtils.format("SocialProfileAboutController initialized"));
        }

    }

    interface ISocialProfileAboutScope extends ng.IScope {
        posts: string;
    }
    interface ISocialProfileAboutStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
