"use strict"

module smf.controllers {

    export class SocialUserController {

        private $scope:ISocialUserScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private SocketIOFactory:services.SocketIOFactory;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;
        private FriendService: services.FriendService;

        public user: models.User;
        public actions: boolean;
        public status: string;

        public constructor($scope:ISocialUserScope, $modal: ng.ui.bootstrap.IModalService, FriendService:services.FriendService, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory, LoggedUserService: services.LoggedUserService) {
            this.$scope = $scope;
            this.$modal = $modal;
            this.FriendService = FriendService;
            this.UsersService = UsersService;
            this.SocketIOFactory = SocketIOFactory;
            this.LoggedUserService = LoggedUserService;

            this.actions = this.$scope.actions;

            this.user = new models.User();
            this.user._id = this.$scope.userId;

            this.load();

            console.debug(smf.utils.LogUtils.format("SocialUserController initialized"));
        }

        private load() {
            this.UsersService.getDetails("_id",this.user._id).then((user: models.User) => {
                this.user = user[0];
                this.UsersService.enhance(this.user);

                this.LoggedUserService.getUser().then((loggedUser) => {
                    if(loggedUser._id == this.user._id) {
                        this.actions = false;
                    }

                    if(loggedUser.friends.confirmed.indexOf(this.user._id) !== -1) {
                        this.status = "confirmed";
                    }
                    else if(loggedUser.friends.sent.indexOf(this.user._id) !== -1) {
                        this.status = "sent";
                    }
                    else if(loggedUser.friends.received.indexOf(this.user._id) !== -1) {
                        this.status = "received";
                    }
                })
            })
        }

        public friendInvite() {
            this.FriendService.invite(this.user._id).then(()=>{
                location.reload();
            })
        }

        public friendRemove() {
            this.FriendService.remove(this.user._id).then(()=>{
                location.reload();
            })
        }

        public friendAccept() {
            this.FriendService.accept(this.user._id).then(()=>{
                location.reload();
            })
        }

    }

    interface ISocialUserScope extends ng.IScope {
        userId: string;
        actions: boolean;
    }
}
