"use strict"

module smf.controllers {

    export class SocialGroupController {

        private $scope:ISocialGroupScope;
        private GroupService: services.GroupService;
        private LoggedUserService: services.LoggedUserService;

        public loggedUser: models.User;
        public group: models.Group;
        public userBelongs: boolean;
        public userRequested: boolean;
        public userIsAdmin: boolean;

        public constructor($scope:ISocialGroupScope, GroupService:services.GroupService, LoggedUserService: services.LoggedUserService) {
            this.$scope = $scope;
            this.LoggedUserService = LoggedUserService;
            this.GroupService = GroupService;

            this.group = new models.Group();
            this.group._id = this.$scope.groupId;

            this.LoggedUserService.getUser().then((loggedUser:models.LoggedUser)=>{
                this.loggedUser = loggedUser;
            })

            this.load();

            console.debug(smf.utils.LogUtils.format("SocialGroupController initialized"));
        }

        private load() {
            this.GroupService.getDetails("_id",this.group._id).then((group: models.Group) => {
                this.group = group[0];
                this.userBelongs = this.group.users.indexOf(this.loggedUser._id) !== -1;
                this.userRequested = this.group.requests.indexOf(this.loggedUser._id) !== -1;
                this.userIsAdmin = this.group.admins.indexOf(this.loggedUser._id) !== -1;
            })
        }

        public join() {
            this.GroupService.join(this.group._id,this.loggedUser._id).then(()=>{
                this.load();
            })
        }

        public leave() {
            this.GroupService.leave(this.group._id,this.loggedUser._id).then(()=>{
                this.load();
            })
        }

    }

    interface ISocialGroupScope extends ng.IScope {
        groupId: string;
    }
}
