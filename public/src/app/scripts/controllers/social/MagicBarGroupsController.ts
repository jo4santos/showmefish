"use strict"

module smf.controllers {

    export class MagicBarGroupsController {

        // Private

        private $scope: IMagicBarGroupsControllerScope;
        private $modal: ng.ui.bootstrap.IModalService;

        // Public attributes available to view

        /**
         *
         * @param userApi
         */

        public constructor(
            $scope: IMagicBarGroupsControllerScope
            ,$modal: ng.ui.bootstrap.IModalService
            ) {
            this.$scope = $scope;
            this.$modal = $modal;

            console.debug(smf.utils.LogUtils.format("MagicBarGroupsController initialized"));
        }

        public create() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/social/modal-create-group.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.SocialGroupCreateController as SocialGroupCreateController";
            this.$modal.open(modalSettings);
        }
    }

    interface IMagicBarGroupsControllerScope extends ng.IScope {

    }
}