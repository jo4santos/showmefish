"use strict"

module smf.controllers {

    export class SocialClassifiedController {

        private $scope:ISocialClassifiedScope;
        private $stateParams:ISocialClassifiedStateParams;
        private ClassifiedService:services.ClassifiedService;
        private SocketIOFactory:services.SocketIOFactory;

        public classified:models.Classified[] = new Array();

        public constructor($scope:ISocialClassifiedScope, ClassifiedService:services.ClassifiedService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialClassifiedStateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.ClassifiedService = ClassifiedService;
            this.SocketIOFactory = SocketIOFactory;

            var userId: string = this.$stateParams.userId || "me";

            this.getList(userId);

            console.debug(smf.utils.LogUtils.format("SocialClassifiedController initialized"));
        }

        public getList(userId: string) {

            var filters:models.ClassifiedFilters = new models.ClassifiedFilters();
            if (userId && userId != "") {
                filters.userId = userId;
            }
            this.ClassifiedService.getAll(filters,null,null)
                .then((classified)=> {
                    this.classified = classified;
                });
        }
    }

    interface ISocialClassifiedScope extends ng.IScope {
    }

    interface ISocialClassifiedStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
