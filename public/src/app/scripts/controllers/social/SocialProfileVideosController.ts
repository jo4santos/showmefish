"use strict"

module smf.controllers {

    export class SocialProfileVideosController {

        private $scope:ISocialProfileVideosScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:ISocialProfileVideosStateParams;
        private UsersService:services.UsersService;
        private VideoService:services.VideoService;
        private LoggedUserService:services.LoggedUserService;

        public user:models.User;
        public loggedUser:models.LoggedUser;
        public videos: models.Video[];

        public isLoggedUser:boolean;

        public constructor($scope:ISocialProfileVideosScope, VideoService:services.VideoService, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialProfileVideosStateParams, LoggedUserService:services.LoggedUserService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.VideoService= VideoService;
            this.SocketIOFactory = SocketIOFactory;

            var userId = this.$stateParams.userId || "me";

            this.user = new models.User();
            this.loggedUser = new models.LoggedUser();

            this.user._id = userId;

            this.isLoggedUser = this.user._id == this.loggedUser._id;

            this.UsersService.getDetails("_id", userId).then((user)=> {
                this.user = user[0];
                this.UsersService.enhance(this.user);
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });
            this.LoggedUserService.getUser().then((user:models.LoggedUser)=> {
                this.loggedUser = user;
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });

            this.loadVideos();

            console.debug(smf.utils.LogUtils.format("SocialProfileVideosController initialized"));
        }

        public loadVideos() {
            this.UsersService.getVideos("userId", this.user._id).then((videos:models.Video[])=> {
                this.videos = videos;
            });
        }

        public remove(videoId: string) {
            if (confirm("Remover video?")) {
                this.UsersService.removeVideo(videoId).then(()=> {
                    this.loadVideos();
                });
            }
        }

        public addLike(elementId) {
            this.VideoService.addLike(elementId).then(()=>{
                this.loadVideos();
            });
        }

        public removeLike(elementId) {
            this.VideoService.removeLike(elementId).then(()=>{
                this.loadVideos();
            });
        }

    }

    interface ISocialProfileVideosScope extends ng.IScope {
        videos: string;
    }
    interface ISocialProfileVideosStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
