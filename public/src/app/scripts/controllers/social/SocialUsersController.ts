"use strict"

module smf.controllers {

    export class SocialUsersController {

        private $scope:ISocialUsersScope;
        private SocketIOFactory:services.SocketIOFactory;
        private UsersService: services.UsersService;

        public filters:models.UserFilters;
        public users: models.User[];
        public interests: models.UserInterest[];

        public constructor($scope:ISocialUsersScope, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory) {
            this.$scope = $scope;
            this.UsersService = UsersService;
            this.SocketIOFactory = SocketIOFactory;

            this.users = [];

            this.UsersService.getInterests(true).then((interests:models.UserInterest[]) => {
                this.interests = interests;
            })

            this.getList();

            console.debug(smf.utils.LogUtils.format("SocialUsersController initialized"));
        }

        public getList() {
            this.UsersService.getAll(this.filters)
                .then((users: models.User[])=> {
                    this.users = users;
                    for(var i in this.users) {
                        this.UsersService.enhance(this.users[i]);
                    }
                });
        }

        public applyFilters() {
            this.filters.interests = [];
            for(var i in this.filters.interestsObj) {
                this.filters.interests.push(this.filters.interestsObj[i]._id);
            }
            this.getList();
        }
    }

    interface ISocialUsersScope extends ng.IScope {
        users: models.User[];
    }
}
