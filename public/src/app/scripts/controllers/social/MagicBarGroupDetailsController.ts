"use strict"

module smf.controllers {

    export class MagicBarGroupDetailsController {

        // Private

        private $scope: IMagicBarGroupDetailsControllerScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private $stateParams:ISocialGroupDetailsStateParams;

        // Public attributes available to view

        /**
         *
         * @param userApi
         */

        public groupId: string;

        public constructor(
            $scope: IMagicBarGroupDetailsControllerScope
            ,$modal: ng.ui.bootstrap.IModalService
            , $stateParams:ISocialGroupDetailsStateParams
            ) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.$modal = $modal;

            this.groupId = this.$stateParams.groupId;

            console.debug(smf.utils.LogUtils.format("MagicBarGroupDetailsController initialized"));
        }

        public share() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/social/modal-share.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.SocialShareController as SocialShareController";
            modalSettings.resolve = {
                targetGroupId: () => {
                    return this.groupId;
                }
            }
            this.$modal.open(modalSettings);
        }
    }

    interface IMagicBarGroupDetailsControllerScope extends ng.IScope {
    }
}