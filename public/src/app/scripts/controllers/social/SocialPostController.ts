"use strict"

module smf.controllers {

    export class SocialPostController implements ICommentController{

        private $scope:ISocialPostScope;
        private $modal: ng.ui.bootstrap.IModalService;
        private SocketIOFactory:services.SocketIOFactory;
        private PostService: services.PostService;

        public post: models.Post;
        public isLoading: boolean;

        public visibleTab: string;

        public constructor($scope:ISocialPostScope, $modal: ng.ui.bootstrap.IModalService, PostService:services.PostService, SocketIOFactory:services.SocketIOFactory) {
            this.$scope = $scope;
            this.$modal = $modal;
            this.PostService = PostService;
            this.SocketIOFactory = SocketIOFactory;

            this.isLoading = true;

            this.post = this.$scope.post;
            this.load();

            console.debug(smf.utils.LogUtils.format("SocialPostController initialized"));
        }

        private load() {
            this.isLoading = true;
            this.PostService.getDetails("_id",this.post._id).then((post: models.Post) => {
                this.post = post;
                this.PostService.enhance(this.post).finally(()=>{
                    this.isLoading = false;
                });
            })
        }

        public report() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/social/modal-report-post.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.SocialPostReportController as SocialPostReportController";
            modalSettings.resolve = {
                postId: () => {
                    return this.post._id;
                }
            }
            this.$modal.open(modalSettings);
        }

        public remove() {
            if (confirm("Remove post?")) {
                this.PostService.remove("_id", this.post._id).then(()=> {
                    location.reload();
                });
            }
        }

        public toggleLike() {
            if(this.post.likes.userLiked) {
                this.PostService.removeLike(this.post._id).then(()=>{
                    this.load();
                });
            }
            else {
                this.PostService.addLike(this.post._id).then(()=>{
                    this.load();
                });
            }
        }

        public addComment() {
            var comment: string = prompt("Comment?");
            if(comment) {
                this.PostService.addComment(this.post._id,comment).then(()=> {
                    this.load();
                });
            }
        }

        public removeComment(comment:models.Comment) {
            if (confirm("Remove comment?")) {
                this.PostService.removeComment(this.post._id,comment._id).then(()=> {
                    this.load();
                });
            }
        }

        public toggleLikeComment(comment:models.Comment) {
            if(comment.likes.userLiked) {
                this.PostService.removeLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
            else {
                this.PostService.addLikeComment(comment._id).then(()=> {
                    this.load();
                });
            }
        }

    }

    interface ISocialPostScope extends ng.IScope {
        post: models.Post;
    }
}
