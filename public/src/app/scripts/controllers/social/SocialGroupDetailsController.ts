"use strict"

module smf.controllers {

    export class SocialGroupDetailsController {

        private $scope:ISocialGroupDetailsScope;
        private FriendService:services.FriendService;
        private GroupService:services.GroupService;
        private $stateParams:ISocialGroupDetailsStateParams;
        private LoggedUserService:services.LoggedUserService;
        private PostService:services.PostService;

        public loggedUser:models.User;
        public group:models.Group;
        public userBelongs:boolean;
        public userIsAdmin:boolean;
        public error:string;

        public posts: models.Post[];

        public constructor($scope:ISocialGroupDetailsScope, FriendService:services.FriendService, GroupService:services.GroupService, LoggedUserService:services.LoggedUserService, PostService:services.PostService, $stateParams:ISocialGroupDetailsStateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.LoggedUserService = LoggedUserService;
            this.GroupService = GroupService;
            this.FriendService = FriendService;
            this.PostService = PostService;

            this.group = new models.Group();
            this.group._id = this.$stateParams.groupId;

            this.posts = [];

            this.LoggedUserService.getUser().then((loggedUser:models.LoggedUser)=> {
                this.loggedUser = loggedUser;
            })

            this.load();

            console.debug(smf.utils.LogUtils.format("SocialGroupDetailsController initialized"));
        }

        private load() {
            this.GroupService.getDetails("_id", this.group._id).then((group:models.Group) => {
                this.group = group[0];
                this.userBelongs = this.belongs(this.loggedUser._id);
                this.userIsAdmin = this.isAdmin(this.loggedUser._id);

                if (!this.userBelongs && this.group.isSecret && !this.userIsAdmin) {
                    this.error = "This group is secret, you can only join via the admin!";
                    return;
                }
                else if (!this.userBelongs && this.group.isPrivate && !this.userIsAdmin) {
                    this.error = "This group is private, please send a join request!";
                    return;
                }
                var filters = new models.PostFilters();
                filters.targetGroupId = this.group._id;
                this.PostService.getAll(filters).then((posts:models.Post[])=> {
                    this.posts = posts;
                })
            })
        }

        public back() {
            window.history.back();
        }

        public remove() {
            if (confirm("Remover grupo?")) {
                this.GroupService.remove("_id", this.group._id).then(()=> {
                    window.history.back();
                })
            }
        }

        public acceptRequest(userId:string) {
            this.GroupService.acceptRequest(this.group._id, userId).then(()=> {
                this.load();
            })
        }

        public denyRequest(userId:string) {
            this.GroupService.denyRequest(this.group._id, userId).then(()=> {
                this.load();
            })
        }

        public removeUser(userId:string) {
            if (confirm("Remover utilizador do grupo?")) {
                this.GroupService.leave(this.group._id, userId).then(()=> {
                    this.load();
                })
            }
        }

        public setAdmin(userId:string) {
            if (this.isAdmin(userId)) {
                if (this.group.admins.length == 1) {
                    alert("Não é possível remover administrador. Seleccione pelo menos mais um administrador antes de continuar.")
                    return;
                }
                if (confirm("Remover privilégios de administrador do grupo?")) {
                    this.GroupService.removeAdmin(this.group._id, userId).then(()=> {
                        this.load();
                    })
                }
            }
            else {
                if (confirm("Dar privilégios de administrador do grupo?")) {
                    this.GroupService.addAdmin(this.group._id, userId).then(()=> {
                        this.load();
                    })
                }
            }
        }

        public isAdmin(userId:string) {
            if (this.group.admins.indexOf(userId) !== -1) {
                return true;
            }
            return false;
        }

        public belongs(userId:string) {
            if (this.group.users.indexOf(userId) !== -1) {
                return true;
            }
            return false;
        }

        public addUser() {
            this.FriendService.select(this.group.users).then((users:string[])=> {
                this.GroupService.addUsers(this.group._id, this.loggedUser._id, users).then(()=> {
                    this.load();
                })
            })
        }

    }

    interface ISocialGroupDetailsScope extends ng.IScope {
        groupId: string;
    }
    export interface ISocialGroupDetailsStateParams extends ng.ui.IStateParams {
        groupId: string;
    }
}
