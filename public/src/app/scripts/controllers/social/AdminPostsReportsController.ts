"use strict"

module smf.controllers {

    export class AdminPostsReportsController {

        private $scope:IAdminPostsReportsScope;
        private PostService:services.PostService;

        public reports:models.PostReport[];

        public constructor($scope:IAdminPostsReportsScope, PostService:services.PostService) {
            this.$scope = $scope;
            this.PostService = PostService;

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminPostsReportsController initialized"));
        }

        public getList() {
            this.PostService.getReports().then((reports: models.PostReport[])=> {
                this.reports = reports;
            });
        }

        public remove(_id:string) {
            if (confirm("Remove report?")) {
                this.PostService.removeReport(_id).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminPostsReportsScope extends ng.IScope {
    }
}
