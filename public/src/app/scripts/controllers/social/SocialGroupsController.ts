"use strict"

module smf.controllers {

    export class SocialGroupsController {

        private $scope:ISocialGroupsScope;
        private GroupService: services.GroupService;

        public groups: models.Group[];
        public filters:models.GroupFilters;


        public constructor($scope:ISocialGroupsScope, GroupService:services.GroupService) {
            this.$scope = $scope;
            this.GroupService = GroupService;

            this.groups = [];

            this.getList();

            console.debug(smf.utils.LogUtils.format("SocialGroupsController initialized"));
        }

        public getList() {
            this.GroupService.getAll(this.filters)
                .then((groups: models.Group[])=> {
                    this.groups = groups;
                });
        }
    }

    interface ISocialGroupsScope extends ng.IScope {
        groups: models.Group[];
    }
}
