"use strict"

module smf.controllers {

    export class SocialCommentController {

        private $scope:ISocialCommentScope;
        private UsersService: services.UsersService;

        public comment: models.Comment;

        public constructor($scope:ISocialCommentScope, UsersService: services.UsersService) {
            this.$scope = $scope;
            this.UsersService = UsersService;

            this.comment = this.$scope.comment;

            this.UsersService.getDetails("_id",this.comment.userId).then((user:models.User)=>{
                this.comment.user = user[0];
                this.UsersService.enhance(this.comment.user);
            })

            console.debug(smf.utils.LogUtils.format("SocialCommentController initialized"));
        }

    }

    interface ISocialCommentScope extends ng.IScope {
        comment: models.Comment;
    }
}
