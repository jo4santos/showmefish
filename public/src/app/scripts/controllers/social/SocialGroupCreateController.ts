"use strict"

module smf.controllers {

    export class SocialGroupCreateController {

        // Private

        private $scope:ISocialGroupCreateControllerScope;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private GroupService:services.GroupService;
        private LoggedUserService:services.LoggedUserService;
        private UsersService:services.UsersService;

        // Public attributes available to view

        public form:ng.IFormController;

        public group:models.Group;
        public users:models.User[];
        public selectedUsers: any[];

        public submitted:boolean;

        public constructor($scope:ISocialGroupCreateControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, GroupService:services.GroupService, UsersService:services.UsersService,LoggedUserService:services.LoggedUserService) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.GroupService = GroupService;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;

            this.group = new models.Group();
            this.group.isPrivate = false;
            this.group.isSecret = false;

            var filters = new models.UserFilters();
            this.LoggedUserService.getUser().then((loggedUser:models.LoggedUser)=>{
                this.UsersService.getAll(filters)
                    .then((users:models.User[])=> {
                        this.users = users;
                        for(var i in this.users) {
                            if(this.users[i]._id == loggedUser._id) {
                                this.users.splice(i,1);
                                break;
                            }
                        }
                        this.selectedUsers = [];
                    });
            })

            console.debug(smf.utils.LogUtils.format("SocialGroupCreateController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.group.users = [];
                for(var i in this.selectedUsers) {
                    if(this.selectedUsers[i] == true) {
                        this.group.users.push(angular.copy(i))
                    }
                }
                this.GroupService.create(this.group).then(() => {
                    this.$modalInstance.dismiss();
                    location.reload();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface ISocialGroupCreateControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}