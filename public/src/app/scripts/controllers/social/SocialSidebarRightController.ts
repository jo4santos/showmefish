"use strict"

module smf.controllers {

    export class SocialSidebarRightController {

        private $scope: ng.IScope;
        private ClassifiedService:services.ClassifiedService;
        private BlogsService: services.BlogsService;

        public classified:models.Classified[];

        public filters:models.ClassifiedFilters;
        public sortOptions:models.ClassifiedSortOptions;
        public paginationOptions:models.ClassifiedPaginationOptions;

        public sortIndex:number;
        public sortOptionsList: {
            title: string;
            field: string;
            direction: string;
        }[];

        public blogItems: any[] = [];

        public constructor($scope: ng.IScope,
                           ClassifiedService:services.ClassifiedService,
                           BlogsService: services.BlogsService) {
            this.$scope = $scope;
            this.ClassifiedService = ClassifiedService;
            this.BlogsService = BlogsService;

            this.classified = [];

            this.sortOptionsList = [];
            this.sortOptionsList.push({title:"Mais recentes",field:"_id",direction:"-1"});
            this.sortOptionsList.push({title:"Mais antigos",field:"_id",direction:"1"});
            this.sortOptionsList.push({title:"Mais caros",field:"price",direction:"-1"});
            this.sortOptionsList.push({title:"Mais baratos",field:"price",direction:"1"});
            this.sortOptionsList.push({title:"Mais vistos",field:"views.count",direction:"-1"});
            this.sortOptionsList.push({title:"Menos vistos",field:"views.count",direction:"1"});

            this.sortIndex = 0;

            this.filters = new models.ClassifiedFilters();
            this.sortOptions = new models.ClassifiedSortOptions();
            this.paginationOptions = new models.ClassifiedPaginationOptions();
            this.paginationOptions.page = 1;
            this.paginationOptions.limit = 3;

            this.getList();

            this.$scope.$watch('[SocialSidebarRightController.sortIndex, SocialSidebarRightController.paginationOptions.page, SocialSidebarRightController.paginationOptions.limit]',()=>{
                this.getList();
            })

            console.debug(smf.utils.LogUtils.format("SocialSidebarRightController initialized"));
        }

        public getList() {
            this.sortOptions.field = this.sortOptionsList[this.sortIndex].field;
            this.sortOptions.direction = this.sortOptionsList[this.sortIndex].direction;
            this.ClassifiedService.getAll(this.filters,this.sortOptions,this.paginationOptions)
                .then((classified)=> {
                    this.classified = classified;
                    for(var i in this.classified) {
                        this.ClassifiedService.enhance(this.classified[i])
                    }
                });
            this.BlogsService.getList(null).then((blogs)=>{
                blogs.forEach((blog) => {
                    var item = blog.items[0];
                    item.blog = {};
                    item.blog.name = blog.name;
                    item.blog.description = blog.description;
                    item.blog.url = blog.url;
                    this.blogItems.push(item);
                })
            });
        }

        public applyFilters() {
            this.filters.category = "";
            if (this.filters.categoryObj) {
                this.filters.category = this.filters.categoryObj._id;
            }
            this.getList();
        }
    }
}
