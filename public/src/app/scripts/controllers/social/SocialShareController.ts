"use strict"

module smf.controllers {

    export class SocialShareController {

        // Private

        private $scope:ISocialShareControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;
        private $upload:ng.angularFileUpload.IUploadService;

        private PostService:services.PostService;
        private VideoService:services.VideoService;

        // Public attributes available to view

        public form:ng.IFormController;

        public post:models.Post;

        public submitted:boolean;

        public files:any;
        public filesProgress:any;

        private fileReaderSupported:any;

        public video: models.Video;

        public constructor($scope:ISocialShareControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, PostService:services.PostService, VideoService:services.VideoService, $upload:ng.angularFileUpload.IUploadService, targetGroupId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.$upload = $upload;
            this.SocketIOFactory = SocketIOFactory;
            this.PostService = PostService;
            this.VideoService = VideoService;

            this.post = new models.Post();

            this.post.targetGroupId = targetGroupId || "";

            this.files = [];

            this.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

            console.debug(smf.utils.LogUtils.format("SocialShareController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public generateThumbs() {
            if (this.files.length > 0) {
                for (var i = 0; i < this.files.length; i++) {
                    if (this.fileReaderSupported && this.files[i].type.indexOf('image') > -1) {
                        var fileReader = new FileReader();
                        fileReader.readAsDataURL(this.files[i]);

                        fileReader.onload = ((file)=> {
                            return (e:any) => {
                                file.dataUrl = e.target.result;
                            }
                        })(this.files[i]);
                    }
                }
            }
        }

        public upload(file:any) {
            this.files.upload = this.$upload.upload({
                url: "/data/images",
                file: file,
                fields: {title:file.title,thumb:true}
            }).progress((evt:any) => {
                console.log('progress: ' + this.filesProgress + '% ' + evt.config.file.name);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == evt.config.file.name) {
                        this.files[i].progress = parseInt("" + (100.0 * evt.loaded / evt.total));
                        break;
                    }
                }
            }).success((data:any, status:any, headers:any, config:any) => {
                console.log('file ' + config.file.name + 'uploaded. Response: ' + data);

                for (var i = 0; i < this.files.length; i++) {
                    if (this.files[i].name == config.file.name) {
                        this.files[i].progress = 100;
                        this.files[i].id = data;
                        break;
                    }
                }
            });
        }

        public cancelUpload (file: any) {
            for (var i = 0; i < this.files.length; i++) {
                if (this.files[i].name == file.name) {

                    if(this.files[i].upload) {
                        this.files[i].upload.cancel();
                    }
                    else {
                        this.files.splice(i,1);
                    }

                    break;
                }
            }
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.post.images = [];

                if(!this.files) this.files = [];

                for (var i = 0; i < this.files.length; i++) {
                    if(!this.files[i].id) {
                        alert("Existe uma fotografia por enviar, confirme o envio ou cancele.");
                        return;
                    }
                    else {
                        this.post.images.push((this.files[i].id));
                    }
                }

                this.VideoService.create(this.video).then((videoId: string) => {
                    this.post.video = videoId;
                    this.PostService.create(this.post).then(() => {
                        this.$modalInstance.dismiss();
                        location.reload();
                    });
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface ISocialShareControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}