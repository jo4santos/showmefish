"use strict"

module smf.controllers {

    export class AdminGroupListController {

        private $scope:IAdminGroupListScope;
        private GroupService:services.GroupService;

        public groups:models.Group[];

        public constructor($scope:IAdminGroupListScope, GroupService:services.GroupService) {
            this.$scope = $scope;
            this.GroupService = GroupService;

            this.groups = [];

            this.getList();

            console.debug(smf.utils.LogUtils.format("AdminGroupListController initialized"));
        }

        public getList() {
            var filters = new models.GroupFilters();
            this.GroupService.getAll(filters).then((groups)=> {
                this.groups = groups;
            });
        }

        public remove(type: string, param:string) {
            if (confirm("Remover grupo?")) {
                this.GroupService.remove(type, param).then(()=> {
                    this.getList();
                });
            }
        }
    }

    interface IAdminGroupListScope extends ng.IScope {

    }
}
