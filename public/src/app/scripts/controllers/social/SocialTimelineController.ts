"use strict"

module smf.controllers {

    export class SocialTimelineController {

        private $scope: ISocialTimelineScope;
        private $stateParams: ISocialTimelineStateParams;
        private SocketIOFactory: services.SocketIOFactory;
        private PostService:services.PostService;

        public posts: models.Post[];
        public userId: string;
        public isLoading: boolean;

        public constructor( $scope: ISocialTimelineScope, $stateParams: ISocialTimelineStateParams, SocketIOFactory: services.SocketIOFactory, PostService:services.PostService ) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.SocketIOFactory = SocketIOFactory;
            this.PostService = PostService;

            if(this.$stateParams.userId) {
                this.userId = this.$stateParams.userId;
            }

            this.isLoading = true;
            this.posts = [];

            this.getList();

            console.debug(smf.utils.LogUtils.format("SocialTimelineController initialized"));
        }

        private getList() {
            this.isLoading = true;
            var filters = new models.PostFilters();
            if(this.userId) {
                if (this.userId && this.userId != "") {
                    filters.userId = this.userId;
                    filters.targetUserId = this.userId;
                }
            }
            this.PostService.getAll(filters).then((posts:models.Post[])=> {
                this.posts = posts;
                this.isLoading = false;
            })
        }

    }

    interface ISocialTimelineScope extends ng.IScope {
        posts: string;
    }
    interface ISocialTimelineStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
