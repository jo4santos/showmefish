"use strict"

module smf.controllers {

    export class SocialProfileImagesController {

        private $scope:ISocialProfileImagesScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:ISocialProfileImagesStateParams;
        private UsersService:services.UsersService;
        private ImageService:services.ImageService;
        private LoggedUserService:services.LoggedUserService;

        public user:models.User;
        public loggedUser:models.LoggedUser;
        public images: models.Image[];

        public isLoggedUser:boolean;
        public classified:models.Classified[] = new Array();

        public constructor($scope:ISocialProfileImagesScope, ImageService:services.ImageService, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialProfileImagesStateParams, LoggedUserService:services.LoggedUserService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.ImageService = ImageService;
            this.SocketIOFactory = SocketIOFactory;

            var userId = this.$stateParams.userId || "me";

            this.user = new models.User();
            this.loggedUser = new models.LoggedUser();

            this.user._id = userId;

            this.isLoggedUser = this.user._id == this.loggedUser._id;

            this.UsersService.getDetails("_id", userId).then((user)=> {
                this.user = user[0];
                this.UsersService.enhance(this.user);
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });
            this.LoggedUserService.getUser().then((user:models.LoggedUser)=> {
                this.loggedUser = user;
                this.isLoggedUser = this.user._id == this.loggedUser._id;
            });

            this.loadImages();

            console.debug(smf.utils.LogUtils.format("SocialProfileImagesController initialized"));
        }

        public loadImages() {
            this.UsersService.getImages("userId", this.user._id).then((images:models.Image[])=> {
                this.images = images;
            });
        }

        public remove(imageId: string) {
            if (confirm("Remover imagem?")) {
                this.UsersService.removeImage(imageId).then(()=> {
                    this.loadImages();
                });
            }
        }

        public addLike(elementId) {
            this.ImageService.addLike(elementId).then(()=>{
                this.loadImages();
            });
        }

        public removeLike(elementId) {
            this.ImageService.removeLike(elementId).then(()=>{
                this.loadImages();
            });
        }

    }

    interface ISocialProfileImagesScope extends ng.IScope {
        images: string;
    }
    interface ISocialProfileImagesStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
