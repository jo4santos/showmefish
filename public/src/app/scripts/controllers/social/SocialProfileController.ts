"use strict"

module smf.controllers {

    export class SocialProfileController {

        private $scope:ISocialProfileScope;
        private $state: ng.ui.IStateService;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:ISocialProfileStateParams;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;
        private FriendService: services.FriendService;
        private ChatService: services.ChatService;

        public user: models.User;
        public loggedUser: models.LoggedUser;
        public friendStatus: string;

        public isLoggedUser: boolean;

        public constructor($scope:ISocialProfileScope, $state: ng.ui.IStateService, FriendService: services.FriendService, UsersService:services.UsersService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialProfileStateParams, LoggedUserService: services.LoggedUserService, ChatService: services.ChatService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.$state = $state;
            this.FriendService = FriendService;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.ChatService = ChatService;
            this.SocketIOFactory = SocketIOFactory;

            var userId = this.$stateParams.userId || "me";

            this.user = new models.User();
            this.user._id = userId;
            this.loggedUser = new models.LoggedUser();

            this.LoggedUserService.getUser().then((user:models.LoggedUser)=>{
                this.loggedUser = user;
                this.UsersService.getDetails("_id", userId).then((user)=> {
                    this.user = user[0];
                    this.UsersService.enhance(this.user);
                    if(this.loggedUser.friends.confirmed.indexOf(this.user._id) !== -1) {
                        this.friendStatus = "confirmed";
                    }
                    else if(this.loggedUser.friends.sent.indexOf(this.user._id) !== -1) {
                        this.friendStatus = "sent";
                    }
                    else if(this.loggedUser.friends.received.indexOf(this.user._id) !== -1) {
                        this.friendStatus = "received";
                    }
                    this.isLoggedUser = this.user._id == this.loggedUser._id;
                });
            });

            console.debug(smf.utils.LogUtils.format("SocialProfileController initialized"));
        }

        public friendInvite() {
            this.FriendService.invite(this.user._id).then(()=>{
                location.reload();
            })
        }

        public friendRemove() {
            this.FriendService.remove(this.user._id).then(()=>{
                location.reload();
            })
        }

        public friendAccept() {
            this.FriendService.accept(this.user._id).then(()=>{
                location.reload();
            })
        }

        public startChat() {
            var chat: models.Chat = new models.Chat();
            chat.users = [this.user._id];
            this.ChatService.create(chat).then((id:string)=>{
                this.$state.go('social.chats.details',{chatId:id});
            })
        }

    }

    interface ISocialProfileScope extends ng.IScope {
        posts: string;
    }
    interface ISocialProfileStateParams extends ng.ui.IStateParams {
        userId: string;
    }
}
