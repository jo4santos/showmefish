"use strict"

module smf.controllers {

    export class SocialProfileFriendsController {

        private $scope:ISocialProfileFriendsScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $stateParams:ISocialProfileFriendsStateParams;
        private UsersService:services.UsersService;
        private FriendService:services.FriendService;
        private LoggedUserService:services.LoggedUserService;

        public user:models.User;

        public constructor($scope:ISocialProfileFriendsScope, UsersService:services.UsersService, FriendService:services.FriendService, SocketIOFactory:services.SocketIOFactory, $stateParams:ISocialProfileFriendsStateParams, LoggedUserService:services.LoggedUserService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.UsersService = UsersService;
            this.FriendService = FriendService;
            this.LoggedUserService = LoggedUserService;
            this.SocketIOFactory = SocketIOFactory;

            console.debug(smf.utils.LogUtils.format("SocialProfileFriendsController initialized"));
        }

    }

    interface ISocialProfileFriendsScope extends ng.IScope {
    }
    interface ISocialProfileFriendsStateParams extends ng.ui.IStateParams {
    }
}
