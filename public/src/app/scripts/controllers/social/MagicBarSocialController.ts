"use strict"

module smf.controllers {

    export class MagicBarSocialController {

        // Private

        private $scope: IMagicBarSocialControllerScope;
        private SocketIOFactory: services.SocketIOFactory;
        private $modal: ng.ui.bootstrap.IModalService;

        // Public attributes available to view

        /**
         *
         * @param userApi
         */

        public constructor(
            $scope: IMagicBarSocialControllerScope
            ,$modal: ng.ui.bootstrap.IModalService
            ,SocketIOFactory: services.SocketIOFactory
            ) {
            this.$scope = $scope;
            this.$modal = $modal;
            this.SocketIOFactory = SocketIOFactory;

            console.debug(smf.utils.LogUtils.format("MagicBarSocialController initialized"));
        }

        public share() {
            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/social/modal-share.html";
            modalSettings.size = "lg";
            modalSettings.controller = "controllers.SocialShareController as SocialShareController";
            modalSettings.resolve = {
                targetGroupId: () => {
                    return "";
                }
            }
            this.$modal.open(modalSettings);
        }
    }

    interface IMagicBarSocialControllerScope extends ng.IScope {
    }
}