"use strict"

module smf.controllers {

    export class SocialPostReportController {

        // Private

        private $scope:ISocialPostReportsControllerScope;
        private SocketIOFactory:services.SocketIOFactory;
        private $modalInstance:ng.ui.bootstrap.IModalServiceInstance;

        private PostService:services.PostService;

        // Public attributes available to view

        public form:ng.IFormController;

        public post:models.Post;
        public postId: string;

        public reason: string;

        public submitted: boolean;

        public constructor($scope:ISocialPostReportsControllerScope, $modalInstance:ng.ui.bootstrap.IModalServiceInstance, SocketIOFactory:services.SocketIOFactory, PostService:services.PostService, postId: string) {
            this.$scope = $scope;
            this.$modalInstance = $modalInstance;
            this.SocketIOFactory = SocketIOFactory;
            this.PostService = PostService;

            this.postId = postId;

            this.post = new models.Post();
            this.reason = "";

            console.debug(smf.utils.LogUtils.format("SocialPostReportController initialized"));
        }

        public cancel() {
            this.$modalInstance.dismiss();
        }

        public submit() {
            this.submitted = true;
            if (!this.form.$invalid) {
                this.PostService.addReport(this.postId,this.reason).then(() => {
                    this.$modalInstance.dismiss();
                });
            }
            else {
                alert("NOK");
            }
        }
    }

    interface ISocialPostReportsControllerScope extends ng.IScope {
        form: ng.IFormController;
    }
}