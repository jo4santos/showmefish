module smf.utils {
    export class LogUtils {
        public static format(data: string): string {
            return "["+new Date().toUTCString()+"] - " + data;
        }
    }
}