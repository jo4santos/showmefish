"use strict"

module smf {

    export class smfApp {

        public constructor() {
            this.bootstrapAngular();
        }

        private bootstrapAngular() {
            var angularApp:ng.IModule =
                angular.module("smfApp", ["ui.router", "ui.bootstrap", 'angularFileUpload', 'ngCropper', 'luegg.directives', "anguvideo"]);

            angularApp.config(function ($urlRouterProvider:ng.ui.IUrlRouterProvider, $stateProvider:ng.ui.IStateProvider, $locationProvider:ng.ILocationProvider, $compileProvider:ng.ICompileProvider, $controllerProvider:ng.IControllerProvider) {

                $stateProvider.state("admin", {
                    url: "/admin",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/admin/index.html"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/admin/magic-bar.html",
                            controller: controllers.MagicBarSocialController,
                            controllerAs: "MagicBarSocialController"
                        }
                    }
                }).state("admin.users", {
                    url: "/users",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/users/index.html"
                        }
                    }
                }).state("admin.images", {
                    url: "/images",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/images/index.html"
                        }
                    }
                }).state("admin.videos", {
                    url: "/videos",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/videos/index.html"
                        }
                    }
                }).state("admin.posts", {
                    url: "/posts",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/posts/index.html"
                        }
                    }
                }).state("admin.groups", {
                    url: "/groups",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/groups/index.html"
                        }
                    }
                }).state("admin.blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/blogs/index.html"
                        }
                    }
                }).state("admin.classified", {
                    url: "/classified",
                    views: {
                        "smf-view-admin@admin": {
                            templateUrl: "/app/partials/admin/classified/index.html"
                        }
                    }
                });

                $stateProvider.state("social", {
                    url: "/social",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/timeline.html",
                            controller: controllers.SocialTimelineController,
                            controllerAs: "SocialTimelineController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar.html",
                            controller: controllers.MagicBarSocialController,
                            controllerAs: "MagicBarSocialController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/social/sidebar-right.html",
                            controller: controllers.SocialSidebarRightController,
                            controllerAs: "SocialSidebarRightController"
                        }
                    }
                }).state("social.users", {
                    url: "/users",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/users.html",
                            controller: controllers.SocialUsersController,
                            controllerAs: "SocialUsersController"
                        }
                    }
                }).state("social.notifications", {
                    url: "/notifications",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/notifications.html",
                            controller: controllers.NotificationsController,
                            controllerAs: "NotificationsController"
                        }
                    }
                }).state("social.chats", {
                    url: "/chats",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/chats.html",
                            controller: controllers.ChatsController,
                            controllerAs: "ChatsController"
                        }
                    }
                }).state("social.chats.details", {
                    url: "/:chatId",
                    views: {
                        "smf-view-chat-details": {
                            templateUrl: "/app/partials/common/chat-details.html",
                            controller: controllers.ChatDetailsController,
                            controllerAs: "ChatDetailsController"
                        }
                    }
                }).state("social.groups", {
                    url: "/groups",
                    views: {
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar-groups.html",
                            controller: controllers.MagicBarGroupsController,
                            controllerAs: "MagicBarGroupsController"
                        },
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/groups.html",
                            controller: controllers.SocialGroupsController,
                            controllerAs: "SocialGroupsController"
                        }
                    }
                }).state("social.groups.details", {
                    url: "/:groupId",
                    views: {
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/social/magic-bar-group-details.html",
                            controller: controllers.MagicBarGroupDetailsController,
                            controllerAs: "MagicBarGroupDetailsController"
                        },
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/group-details.html",
                            controller: controllers.SocialGroupDetailsController,
                            controllerAs: "SocialGroupDetailsController"
                        }
                    }
                }).state("social.profile", {
                    url: "/profile/{userId}",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/social/profile.html",
                            controller: controllers.SocialProfileController,
                            controllerAs: "SocialProfileController"
                        }
                    }
                }).state("social.profile.timeline", {
                    url: "/timeline",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/timeline.html",
                            controller: controllers.SocialTimelineController,
                            controllerAs: "SocialTimelineController"
                        }
                    }
                }).state("social.profile.blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/blogs/index.html",
                            controller: controllers.BlogsController,
                            controllerAs: "BlogsController"
                        }
                    }
                }).state("social.profile.classified", {
                    url: "/classified",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/classified.html",
                            controller: controllers.SocialClassifiedController,
                            controllerAs: "SocialClassifiedController"
                        }
                    }
                }).state("social.profile.about", {
                    url: "/about",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/about.html",
                            controller: controllers.SocialProfileAboutController,
                            controllerAs: "SocialProfileAboutController"
                        }
                    }
                }).state("social.profile.images", {
                    url: "/images",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/images.html",
                            controller: controllers.SocialProfileImagesController,
                            controllerAs: "SocialProfileImagesController"
                        }
                    }
                }).state("social.profile.videos", {
                    url: "/videos",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/videos.html",
                            controller: controllers.SocialProfileVideosController,
                            controllerAs: "SocialProfileVideosController"
                        }
                    }
                }).state("social.profile.friends", {
                    url: "/friends",
                    views: {
                        "smf-view-profile": {
                            templateUrl: "/app/partials/social/friends.html",
                            controller: controllers.SocialProfileFriendsController,
                            controllerAs: "SocialProfileFriendsController"
                        }
                    }
                });

                $stateProvider.state("classified", {
                    url: "/classified/:type",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/classified/index.html",
                            controller: controllers.ClassifiedController,
                            controllerAs: "ClassifiedController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/classified/magic-bar.html",
                            controller: controllers.MagicBarClassifiedController,
                            controllerAs: "MagicBarClassifiedController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/classified/sidebar-right.html"
                        }
                    }
                }).state("classified.details", {
                    url: "/details/{_id}",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/classified/details.html",
                            controller: controllers.ClassifiedDetailsController,
                            controllerAs: "ClassifiedDetailsController"
                        }
                    }
                });

                $stateProvider.state("blogs", {
                    url: "/blogs",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/blogs/index.html",
                            controller: controllers.BlogsController,
                            controllerAs: "BlogsController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/blogs/magic-bar.html",
                            controller: controllers.MagicBarBlogsController,
                            controllerAs: "MagicBarBlogsController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/blogs/sidebar-right.html"
                        }
                    }
                });

                $stateProvider.state("settings", {
                    url: "/settings",
                    views: {
                        "smf-view-main@": {
                            templateUrl: "/app/partials/common/settings.html",
                            controller: controllers.SettingsController,
                            controllerAs: "SettingsController"
                        },
                        "smf-view-magic-bar@": {
                            templateUrl: "/app/partials/common/settings-magic-bar.html",
                            controller: controllers.SettingsMagicBarController,
                            controllerAs: "SettingsMagicBarController"
                        },
                        "smf-view-sidebar-right@": {
                            templateUrl: "/app/partials/common/settings-sidebar-right.html"
                        }
                    }
                });

                $urlRouterProvider.otherwise("/social");

                $controllerProvider.register("controllers.SocialUsersController", controllers.SocialUsersController);
                $controllerProvider.register("controllers.SocialPostController", controllers.SocialPostController);
                $controllerProvider.register("controllers.SocialUserController", controllers.SocialUserController);
                $controllerProvider.register("controllers.SocialCommentController", controllers.SocialCommentController);
                $controllerProvider.register("controllers.SocialTimelineController", controllers.SocialTimelineController);
                $controllerProvider.register("controllers.SocialProfileAboutController", controllers.SocialProfileAboutController);
                $controllerProvider.register("controllers.SocialProfileImagesController", controllers.SocialProfileImagesController);
                $controllerProvider.register("controllers.SocialProfileFriendsController", controllers.SocialProfileFriendsController);
                $controllerProvider.register("controllers.SocialShareController", controllers.SocialShareController);
                $controllerProvider.register("controllers.SocialPostReportController", controllers.SocialPostReportController);
                $controllerProvider.register("controllers.SocialGroupCreateController", controllers.SocialGroupCreateController);
                $controllerProvider.register("controllers.SocialGroupsController", controllers.SocialGroupsController);

                $controllerProvider.register("controllers.SettingsController", controllers.SettingsController);
                $controllerProvider.register("controllers.SettingsMagicBarController", controllers.SettingsMagicBarController);

                $controllerProvider.register("controllers.BlogsController", controllers.BlogsController);
                $controllerProvider.register("controllers.BlogsShareController", controllers.BlogsShareController);

                $controllerProvider.register("controllers.ClassifiedController", controllers.ClassifiedController);
                $controllerProvider.register("controllers.ClassifiedShareController", controllers.ClassifiedShareController);
                $controllerProvider.register("controllers.ClassifiedReportController", controllers.ClassifiedReportController);

                $controllerProvider.register("controllers.CommonController", controllers.CommonController);
                $controllerProvider.register("controllers.ChatsController", controllers.ChatsController);
                $controllerProvider.register("controllers.ChatDetailsController", controllers.ChatDetailsController);
                $controllerProvider.register("controllers.NavigationController", controllers.NavigationController);
                $controllerProvider.register("controllers.ProfileChangeImageController", controllers.ProfileChangeImageController);
                $controllerProvider.register("controllers.UserReportController", controllers.UserReportController);
                $controllerProvider.register("controllers.ImageReportController", controllers.ImageReportController);
                $controllerProvider.register("controllers.ImageViewerController", controllers.ImageViewerController);
                $controllerProvider.register("controllers.VideoReportController", controllers.VideoReportController);
                $controllerProvider.register("controllers.VideoViewerController", controllers.VideoViewerController);
                $controllerProvider.register("controllers.PostViewerController", controllers.PostViewerController);
                $controllerProvider.register("controllers.ModalSelectUsersController", controllers.ModalSelectUsersController);

                $controllerProvider.register("controllers.AdminClassifiedListController", controllers.AdminClassifiedListController);
                $controllerProvider.register("controllers.AdminClassifiedCategoriesController", controllers.AdminClassifiedCategoriesController);
                $controllerProvider.register("controllers.AdminClassifiedReportsController", controllers.AdminClassifiedReportsController);

                $controllerProvider.register("controllers.AdminUserListController", controllers.AdminUserListController);
                $controllerProvider.register("controllers.AdminUserInterestsController", controllers.AdminUserInterestsController);
                $controllerProvider.register("controllers.AdminUserReportsController", controllers.AdminUserReportsController);

                $controllerProvider.register("controllers.AdminImagesReportsController", controllers.AdminImagesReportsController);
                $controllerProvider.register("controllers.AdminVideosReportsController", controllers.AdminVideosReportsController);

                $controllerProvider.register("controllers.AdminPostsReportsController", controllers.AdminPostsReportsController);

                $controllerProvider.register("controllers.AdminGroupListController", controllers.AdminGroupListController);
            });


            angularApp.directive('smfLoading', () => {
                return {
                    restrict: 'E',
                    templateUrl: './app/partials/common/directives/loading.html',
                    transclude: true
                }
            });

            angularApp.directive('smfUserLink', () => {
                return {
                    restrict: 'E',
                    templateUrl: './app/partials/common/directives/user-link.html',
                    scope: {userId: '=userId'},
                    controller: controllers.UserLinkController,
                    controllerAs: "UserLinkController"
                }
            });

            angularApp.directive('smfSidebarRightPost', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/common/directives/sidebar-right-post.html'
                }
            });

            angularApp.directive('smfSocialPost', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/post.html',
                    scope: {post: '=post'},
                    controller: controllers.SocialPostController,
                    controllerAs: "SocialPostController"
                }
            });

            angularApp.directive('smfSocialUser', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/user.html',
                    scope: {userId: '=userId', actions: '=actions'},
                    controller: controllers.SocialUserController,
                    controllerAs: "SocialUserController"
                }
            });

            angularApp.directive('smfSocialGroup', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/group.html',
                    scope: {groupId: '=groupId'},
                    controller: controllers.SocialGroupController,
                    controllerAs: "SocialGroupController"
                }
            });

            angularApp.directive('smfSocialComment', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/social/directives/comment.html',
                    scope: {comment: '=comment', remove: '&', toggleLike: '&'},
                    controller: controllers.SocialCommentController,
                    controllerAs: "SocialCommentController"
                }
            });

            angularApp.directive('smfBlogPost', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/blogs/directives/post.html',
                    scope: {post: '=post', blog: '=blog'}
                }
            });

            angularApp.directive('smfClassifiedPost', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/classified/directives/post.html',
                    scope: {classified: '=classified'}
                }
            });

            angularApp.directive('smfClassifiedThumbnail', () => {
                return {
                    restrict: 'E',
                    templateUrl: '/app/partials/classified/directives/thumbnail.html',
                    scope: {classifiedId: '=classifiedId'},
                    controller: controllers.ClassifiedThumbnailController,
                    controllerAs: "ClassifiedThumbnailController"
                }
            });

            angularApp.directive('smfImageThumbnail', ($modal) => {
                return {
                    restrict: 'A',
                    scope: false,
                    link: (scope, elem, attrs) => {
                        elem.bind('click', () => {
                            var modalSettings:ng.ui.bootstrap.IModalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-image-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.ImageViewerController as ImageViewerController";
                            modalSettings.resolve = {
                                imageId: () => {
                                    return attrs.imageId;
                                }
                            }
                            $modal.open(modalSettings);
                        });
                    }
                }
            });

            angularApp.directive('smfPostThumbnail', ($modal) => {
                return {
                    restrict: 'A',
                    scope: false,
                    link: (scope, elem, attrs) => {
                        elem.bind('click', () => {
                            var modalSettings:ng.ui.bootstrap.IModalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-post-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.PostViewerController as PostViewerController";
                            modalSettings.resolve = {
                                postId: () => {
                                    return attrs.postId;
                                }
                            }
                            $modal.open(modalSettings);
                        });
                    }
                }
            });

            angularApp.directive('smfVideoLink', ($modal) => {
                return {
                    restrict: 'A',
                    scope: false,
                    link: (scope, elem, attrs) => {
                        elem.bind('click', () => {
                            var modalSettings:ng.ui.bootstrap.IModalSettings = {};
                            modalSettings.templateUrl = "app/partials/common/modal-video-viewer.html";
                            modalSettings.size = "lg";
                            modalSettings.controller = "controllers.VideoViewerController as VideoViewerController";
                            modalSettings.resolve = {
                                videoId: () => {
                                    return attrs.videoId;
                                }
                            }
                            $modal.open(modalSettings);
                        });
                    }
                }
            });

            angularApp.directive('scrollPosition', function ($window) {
                return {
                    scope: {
                        scroll: '=scrollPosition'
                    },
                    link: function (scope, element, attrs) {
                        var windowEl = angular.element($window);
                        var handler = function () {
                            scope.scroll = windowEl.scrollTop();
                        }
                        windowEl.on('scroll', scope.$apply.bind(scope, handler));
                        handler();
                    }
                };
            });

            angularApp.filter('bytes', function () {
                return function (bytes, precision) {
                    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
                    if (typeof precision === 'undefined') precision = 1;
                    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                        number = Math.floor(Math.log(bytes) / Math.log(1024));
                    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
                }
            });

            angularApp.filter('reverse', function() {
                return function(items) {
                    return items.slice().reverse();
                };
            });

            angularApp.provider("LoggedUserService", function () {
                var LoggedUserService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService) => {
                    if (LoggedUserService == null) {
                        LoggedUserService = new services.LoggedUserService($q, $http);
                    }
                    return LoggedUserService;
                };
            });
            angularApp.provider("UsersService", function () {
                var UsersService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService) => {
                    if (UsersService == null) {
                        UsersService = new services.UsersService($q, $http);
                    }
                    return UsersService;
                };
            });
            angularApp.provider("FriendService", function () {
                var FriendService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService, $modal:ng.ui.bootstrap.IModalService) => {
                    if (FriendService == null) {
                        FriendService = new services.FriendService($q, $http, UsersService, LoggedUserService, $modal);
                    }
                    return FriendService;
                };
            });
            angularApp.provider("ImageService", function () {
                var ImageService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) => {
                    if (ImageService == null) {
                        ImageService = new services.ImageService($q, $http, UsersService, LoggedUserService);
                    }
                    return ImageService;
                };
            });
            angularApp.provider("VideoService", function () {
                var VideoService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) => {
                    if (VideoService == null) {
                        VideoService = new services.VideoService($q, $http, UsersService, LoggedUserService);
                    }
                    return VideoService;
                };
            });
            angularApp.provider("ChatService", function () {
                var ChatService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) => {
                    if (ChatService == null) {
                        ChatService = new services.ChatService($q, $http, UsersService, LoggedUserService);
                    }
                    return ChatService;
                };
            });
            angularApp.provider("PostService", function () {
                var PostService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) => {
                    if (PostService == null) {
                        PostService = new services.PostService($q, $http, UsersService, LoggedUserService);
                    }
                    return PostService;
                };
            });
            angularApp.provider("GroupService", function () {
                var GroupService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) => {
                    if (GroupService == null) {
                        GroupService = new services.GroupService($q, $http, UsersService, LoggedUserService);
                    }
                    return GroupService;
                };
            });
            angularApp.provider("BlogsService", function () {
                var BlogsService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService) => {
                    if (BlogsService == null) {
                        BlogsService = new services.BlogsService($q, $http, UsersService);
                    }
                    return BlogsService;
                };
            });
            angularApp.provider("ClassifiedService", function () {
                var ClassifiedService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService) => {
                    if (ClassifiedService == null) {
                        ClassifiedService = new services.ClassifiedService($q, $http);
                    }
                    return ClassifiedService;
                };
            });
            angularApp.provider("NavigationService", function () {
                var NavigationService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService) => {
                    if (NavigationService == null) {
                        NavigationService = new services.NavigationService($q, $http);
                    }
                    return NavigationService;
                };
            });
            angularApp.provider("SocketIOFactory", function () {
                var SocketIOFactory;
                this.$get = ($rootScope:ng.IRootScopeService) => {
                    if (SocketIOFactory == null) {
                        SocketIOFactory = new services.SocketIOFactory($rootScope);
                    }
                    return SocketIOFactory;
                };
            });
            angularApp.provider("NotificationService", function () {
                var NotificationService;
                this.$get = ($http:ng.IHttpService, $q:ng.IQService, SocketIOFactory:services.SocketIOFactory, LoggedUserService:services.LoggedUserService) => {
                    if (NotificationService == null) {
                        NotificationService = new services.NotificationService($http, $q, SocketIOFactory, LoggedUserService);
                    }
                    return NotificationService;
                };
            });

            angularApp.run(($state:ng.ui.IState, $rootScope:smfRootScopeService, LoggedUserService:services.LoggedUserService)=> {
                $rootScope.scroll = 0;

                LoggedUserService.load();
            });
        }
    }
    interface smfRootScopeService extends ng.IRootScopeService {
        scroll: number;
    }
}

new smf.smfApp();
console.debug("[" + new Date().toUTCString() + "] - smfApp has just started!");