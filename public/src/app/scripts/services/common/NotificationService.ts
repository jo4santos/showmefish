module smf.services {

    export class NotificationService {

        private $http:ng.IHttpService;
        private $q:ng.IQService;
        private socketIOFactory:SocketIOFactory;
        private LoggedUserService: LoggedUserService;

        static $inject = ['$http', '$q', 'SocketIOFactory', 'LoggedUserService'];

        public constructor($http:ng.IHttpService, $q:ng.IQService, socketIOFactory:SocketIOFactory, LoggedUserService: LoggedUserService) {
            this.$q = $q;
            this.$http = $http;
            this.socketIOFactory = socketIOFactory;
            this.LoggedUserService = LoggedUserService;
        }

        public getAll():ng.IPromise<models.Notification[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/notifications').success((response:models.Notification[])=> {
                this.LoggedUserService.getUser().then((user:models.LoggedUser)=>{
                    for(var i in response) {
                        if(user.notifications && parseInt(response[i].timestamp) <= parseInt(user.notifications.lastReadDate)) {
                            response[i].read = true;
                        }
                    }
                    deferred.resolve(response);
                })
            });
            return deferred.promise;
        }

        public setRead():ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.get('/data/notifications/read').success((response:any)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }
    }

}