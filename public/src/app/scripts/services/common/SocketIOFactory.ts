
module smf.services {

    declare var io : {
        connect(url: string): Socket;
    }
    interface Socket {
        on(event: string, callback: (data: any) => void );
        emit(event: string, data: any);
    }

    export class SocketIOFactory {

        private $rootScope: ng.IRootScopeService;
        private url: string;

        public socket: Socket;

        static $inject = ['$rootScope','$http'];

        public constructor($rootScope: ng.IRootScopeService)
        {
            this.$rootScope = $rootScope;
            this.url = document.URL.toLowerCase().replace("/public/index.html","");
            if(this.url.indexOf("rhcloud.com") == -1) {
                this.url = document.URL.toLowerCase().replace("/public/","");
            }
            else {
                this.url = "showmefish-josapps.rhcloud.com:8000";
            }
            this.socket = io.connect(this.url);
        }

        public on (eventName, callback): void {
            this.socket.on(eventName, function () {
                var args = arguments;
                this.$rootScope.$apply( () => {
                    callback.apply(this.socket, args);
                });
            });
        }
        public emit (eventName, data): void {
            this.socket.emit(eventName, data)
        }
    }

}