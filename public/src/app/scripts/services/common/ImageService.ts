
module smf.services {

    export class ImageService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;

        static $inject = ['$q','$http','UsersService','LoggedUserService'];

        public constructor($q: ng.IQService, $http: ng.IHttpService, UsersService: services.UsersService, LoggedUserService: services.LoggedUserService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
        }

        public getDetails(type: string, param: string): ng.IPromise<models.Image> {
            var deferred = this.$q.defer();
            this.$http.get('/data/images/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<models.Image[]>) => {
                deferred.resolve(response[0]);
            })
            return deferred.promise;
        }

        public enhance(element:models.Image):ng.IPromise<models.Image> {
            var deferred = this.$q.defer();

            this.$q.all([
                this.UsersService.getDetails("_id",element.userId),
                this.LoggedUserService.getUser()
            ])
                .then((response:any[])=> {
                    element.user = angular.copy(response[0][0]);
                    this.UsersService.enhance(element.user);

                    var loggedUser: models.LoggedUser = response[1];


                    //ToDo: Place this is a common function ??
                    for(var i in element.likes.data) {
                        if(element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }

                    for(var i in element.comments.data) {
                        for(var j in element.comments.data[i].likes.data) {
                            if(element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }

                    deferred.resolve(element);
                })

            return deferred.promise;
        }

        //
        //  Image report methods
        //

        public addReport(imageId:string, reason:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/images/reports', {"imageId": imageId, "reason": reason}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getReports():ng.IPromise<models.ImageReport[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/images/reports/').success((response:models.ImageReport[])=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public removeReport(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/images/reports/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }


        //
        //  post likes methods
        //


        public addLike(elementId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/images/like/'+elementId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLike(elementId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/images/like/'+elementId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public addLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/images/like/comment/'+commentId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/images/like/comment/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }


        //
        //  post comments methods
        //


        public addComment(elementId: string, comment: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/images/comment/'+elementId,{"comment":comment}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeComment(elementId: string,commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/images/comment/'+elementId+'/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
    }

}