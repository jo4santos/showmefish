module smf.services {

    export class LoggedUserService {

        private $q:ng.IQService;
        private $http:ng.IHttpService;

        static $inject = ['$q', '$http'];

        private user:models.LoggedUser;

        public constructor($q:ng.IQService, $http:ng.IHttpService) {
            this.$q = $q;
            this.$http = $http;
        }

        public load():ng.IPromise<models.LoggedUser> {
            var deferred = this.$q.defer();
            this.$http.get('/data/loggeduser').success((response:models.LoggedUser)=> {
                this.user = response;
                deferred.resolve(this.user);
            });
            return deferred.promise;
        }

        public save(user:any):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.put('/data/loggeduser', user).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getUser(force?:boolean):ng.IPromise<models.LoggedUser> {
            var deferred = this.$q.defer();
            if (!this.user || force) {
                return this.load();
            }
            else {
                deferred.resolve(this.user);
            }

            return deferred.promise;
        }

        public setProfileImage(cropData:any):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.put('/data/loggeduser/profileImage', cropData).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public setProfileImageSocial(src: string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.put('/data/loggeduser/profileImage/social',{"src":src}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public unlinkGoogle():ng.IPromise<void> {
            var deferred = this.$q.defer<void>();
            this.$http.get('/data/auth/unlink/google').success(() => {
                deferred.resolve();
            })
            return deferred.promise;
        }

        public unlinkFacebook():ng.IPromise<void> {
            var deferred = this.$q.defer<void>();
            this.$http.get('/data/auth/unlink/facebook').success(() => {
                deferred.resolve();
            })
            return deferred.promise;
        }
    }

}