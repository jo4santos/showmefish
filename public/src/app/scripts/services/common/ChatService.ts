
module smf.services {

    export class ChatService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;

        static $inject = ['$q','$http','UsersService','LoggedUserService'];

        public constructor($q: ng.IQService, $http: ng.IHttpService, UsersService: services.UsersService, LoggedUserService: services.LoggedUserService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
        }

        //
        //  General chat methods
        //

        public getAll(filters:models.ChatFilters):ng.IPromise<models.Chat[]> {
            filters = filters || new models.ChatFilters();

            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/chats/', {params: params}).success((response:ng.IHttpPromiseCallback<models.Chat[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public getDetails(type:string, param:string):ng.IPromise<models.Chat> {
            var deferred = this.$q.defer();
            this.$http.get('/data/chats/' + param + '/' + type).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public create(chat: models.Chat): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/chats/',chat).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public remove(type:string, param:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/chats/' + param + '/' + type).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public sendMessage(chatId: string, content: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/chats/message/'+chatId,{"content":content}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public addUsers(chatId:string, users:string[]):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/chats/users/' + chatId, {users:users}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public setRead(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/chats/unread/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public getUnread(_id?:string): ng.IPromise<string> {
            var deferred = this.$q.defer();
            var _id = _id || "";
            this.$http.get('/data/chats/unread/'+ _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public enhance(element:models.Chat):ng.IPromise<models.Chat> {
            var deferred = this.$q.defer();

            this.$q.all([
                this.getUnread(element._id)
            ])
                .then((response:any[])=> {
                    element.unreadCount = angular.copy(response[0]);
                    deferred.resolve(element);
                })

            return deferred.promise;
        }
    }

}