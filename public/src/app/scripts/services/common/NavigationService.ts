
module smf.services {

    export class NavigationService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;

        static $inject = ['$q','$http'];

        private pages: models.NavigationPage[] = [];

        public constructor($q: ng.IQService, $http: ng.IHttpService)
        {
            this.$q = $q;
            this.$http = $http;
        }

        public getList(): ng.IPromise<models.NavigationPage[]> {
            var deferred = this.$q.defer();

            var page: models.NavigationPage = new models.NavigationPage()
            page.text = "Users";
            page.url = "users"
            this.pages.push(page);

            for(var i=0;i<5;i++) {
                var page: models.NavigationPage = new models.NavigationPage()
                page.text = "Page " + i;
                page.url = "page"+i;
                page.children = [];
                for (var j=0;j<3;j++) {
                    var child: models.NavigationPage = new models.NavigationPage()
                    child.text = "Page " + i + "." + j;
                    child.url = "page"+i + "." + j;
                    child.children = [];
                    page.children.push(child);
                }
                this.pages.push(page);
            }
            deferred.resolve(this.pages);
            return deferred.promise;
        }
    }

}