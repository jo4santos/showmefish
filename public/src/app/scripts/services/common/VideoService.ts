
module smf.services {

    export class VideoService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;

        static $inject = ['$q','$http','UsersService','LoggedUserService'];

        public constructor($q: ng.IQService, $http: ng.IHttpService, UsersService: services.UsersService, LoggedUserService: services.LoggedUserService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
        }

        public getDetails(type: string, param: string): ng.IPromise<models.Video> {
            var deferred = this.$q.defer();
            this.$http.get('/data/videos/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<models.Video[]>) => {
                deferred.resolve(response[0]);
            })
            return deferred.promise;
        }

        public create(video: models.Video): ng.IPromise<string> {
            var deferred = this.$q.defer();
            if(!video) {
                deferred.resolve("");
            }
            this.$http.post('/data/videos',video).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public enhance(element:models.Video):ng.IPromise<models.Video> {
            var deferred = this.$q.defer();

            this.$q.all([
                this.UsersService.getDetails("_id",element.userId),
                this.LoggedUserService.getUser()
            ])
                .then((response:any[])=> {
                    element.user = angular.copy(response[0][0]);
                    this.UsersService.enhance(element.user);

                    var loggedUser: models.LoggedUser = response[1];


                    //ToDo: Place this is a common function ??
                    for(var i in element.likes.data) {
                        if(element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }

                    for(var i in element.comments.data) {
                        for(var j in element.comments.data[i].likes.data) {
                            if(element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }

                    deferred.resolve(element);
                })

            return deferred.promise;
        }

        //
        //  Image report methods
        //

        public addReport(videoId:string, reason:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/videos/reports', {"videoId": videoId, "reason": reason}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getReports():ng.IPromise<models.VideoReport[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/videos/reports/').success((response:models.VideoReport[])=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public removeReport(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/videos/reports/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }


        //
        //  post likes methods
        //


        public addLike(elementId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/videos/like/'+elementId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLike(elementId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/videos/like/'+elementId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public addLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/videos/like/comment/'+commentId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/videos/like/comment/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }


        //
        //  post comments methods
        //


        public addComment(elementId: string, comment: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/videos/comment/'+elementId,{"comment":comment}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeComment(elementId: string,commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/videos/comment/'+elementId+'/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
    }

}