
module smf.services {

    export interface IUserBaseService {
        getAll(filters:models.UserFilters): ng.IPromise<models.User[]>;
    }

    export class UsersService implements IUserBaseService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;

        static $inject = ['$q','$http'];

        private interests:models.UserInterest[];

        public constructor($q: ng.IQService, $http: ng.IHttpService)
        {
            this.$q = $q;
            this.$http = $http;
        }


        //
        //  General user methods
        //

        public getAll(filters:models.UserFilters):ng.IPromise<models.User[]> {
            filters = filters || new models.UserFilters();

            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/users/', {params: params}).success((response:ng.IHttpPromiseCallback<models.User[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public getDetails(type: string, param: string): ng.IPromise<models.User> {
            var deferred = this.$q.defer();
            this.$http.get('/data/users/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public create(user: any): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/users',{"user":user}).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public remove(type: string, param: string): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/users/'+type+'/'+param).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public enhance(element:models.User):ng.IPromise<models.User> {
            var deferred = this.$q.defer();

            if(!element.image) {
                element.image = "img-fallback";
            }

            this.$q.all([
                this.getInterestsObj(element.interests)
            ])
                .then((response:any[])=> {
                    element.interestsObj = angular.copy(response[0]);
                    deferred.resolve(element);
                })

            return deferred.promise;
        }


        //
        //  User interest methods
        //

        public getInterests(force?:boolean):ng.IPromise<models.UserInterest[]> {
            var deferred = this.$q.defer();
            if (this.interests && !force) {
                deferred.resolve(this.interests);
            }
            this.$http.get('/data/users/interests/').success((response:models.UserInterest[])=> {
                this.interests = response;
                deferred.resolve(this.interests);
            });
            return deferred.promise;
        }

        public getInterest(_id:string):ng.IPromise<models.UserInterest> {
            var deferred = this.$q.defer();
            this.getInterests()
                .then((response:models.UserInterest[]) => {
                    for (var i in response) {
                        if (response[i]._id == _id) {
                            deferred.resolve(response[i]);
                        }
                    }
                    deferred.reject();
                });
            return deferred.promise;
        }

        public addInterest(name:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/users/interests', {"name": name}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public updateInterest(_id:string, name:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.put('/data/users/interests', {"_id": _id, "name": name}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public removeInterest(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/users/interests/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getInterestsObj(interests:string[]):ng.IPromise<models.UserInterest[]> {
            interests = interests || [];
            var deferred = this.$q.defer();
            if (interests.length == 0) {
                deferred.resolve([]);
            }
            else {
                this.$http.get('/data/users/interests/' + interests).success((response:ng.IHttpPromiseCallback<models.UserInterest[]>) => {
                    deferred.resolve(response);
                })
            }
            return deferred.promise;
        }

        //
        //  User report methods
        //

        public addReport(userId:string, reason:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/users/reports', {"userId": userId, "reason": reason}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getReports():ng.IPromise<models.UserReport[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/users/reports/').success((response:models.UserReport[])=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public removeReport(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/users/reports/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        //
        //  User images methods
        //

        public getImages(type: string, param: string): ng.IPromise<models.Image[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/users/images/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<models.Image[]>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public removeImage(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/images/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        //
        //  User videos methods
        //

        public getVideos(type: string, param: string): ng.IPromise<models.Video[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/users/videos/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<models.Video[]>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public removeVideo(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/videos/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }
    }

}