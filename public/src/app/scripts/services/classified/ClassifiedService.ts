module smf.services {

    export class ClassifiedService {

        private $q:ng.IQService;
        private $http:ng.IHttpService;

        static $inject = ['$q', '$http'];

        private categories:models.ClassifiedCategory[];

        public constructor($q:ng.IQService, $http:ng.IHttpService) {
            this.$q = $q;
            this.$http = $http;
        }

        //
        //  General classified methods
        //

        public getAll(filters:models.ClassifiedFilters, sortOptions:models.ClassifiedSortOptions, paginationOptions:models.ClassifiedPaginationOptions):ng.IPromise<models.Classified[]> {
            filters = filters || new models.ClassifiedFilters();
            sortOptions = sortOptions || new models.ClassifiedSortOptions();
            paginationOptions = paginationOptions || new models.ClassifiedPaginationOptions();

            var params:any = {};
            params.filters = filters;
            params.sort = sortOptions;
            params.pagination = paginationOptions;

            var deferred = this.$q.defer();
            this.$http.get('/data/classified/', {params: params}).success((response:ng.IHttpPromiseCallback<models.Classified[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public getDetails(type:string, param:string):ng.IPromise<models.Classified> {
            var deferred = this.$q.defer();
            this.$http.get('/data/classified/details/' + param + '/' + type).success((response:ng.IHttpPromiseCallback<models.Classified[]>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public create(classified:models.Classified):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/classified', {"classified": classified}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public remove(type:string, param:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/classified/' + param + '/' + type).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getImages(images: string[]):ng.IPromise<models.Image[]> {
            var deferred = this.$q.defer();
            if(images.length == 0) {
                deferred.resolve([]);
            }
            else {
                this.$http.get('/data/images/' + images).success((response:ng.IHttpPromiseCallback<models.Image[]>) => {
                    deferred.resolve(response);
                })
            }
            return deferred.promise;
        }

        public enhance(element:models.Classified):ng.IPromise<models.Classified> {
            var deferred = this.$q.defer();

            this.$q.all([
                this.getCategory(element.category),
                this.getImages(element.images)
            ])
                .then((response:any[])=> {
                    console.log(response);
                    element.categoryObj = angular.copy(response[0]);
                    element.imagesObj = angular.copy(response[1]);
                    deferred.resolve(element);
                })

            return deferred.promise;
        }

        //
        //  Classified category methods
        //

        public getCategories(force?:boolean):ng.IPromise<models.ClassifiedCategory[]> {
            var deferred = this.$q.defer();
            if (this.categories && !force) {
                deferred.resolve(this.categories);
            }
            else {
                this.$http.get('/data/classified/categories/').success((response:models.ClassifiedCategory[])=> {
                    this.categories = response;
                    deferred.resolve(this.categories);
                });
            }
            return deferred.promise;
        }

        public getCategory(_id:string):ng.IPromise<models.ClassifiedCategory> {
            var deferred = this.$q.defer();
            this.getCategories()
                .then((categories:models.ClassifiedCategory[]) => {
                    categories.forEach((category) => {
                        category.children.forEach((child)=>{
                            if(child._id == _id) {
                                deferred.resolve(child)
                            }
                        })
                    });
                    deferred.reject();
                });
            return deferred.promise;
        }

        public addCategory(name:string, parentId?: string):ng.IPromise<string> {
            return this.$http.post('/data/classified/categories', {"parentId": parentId, "name": name});
        }

        public updateCategory(_id:string, name:string, parentId?: string):ng.IPromise<string> {
            return this.$http.put('/data/classified/categories', {"_id": _id, "name": name, "parentId": parentId});
        }

        public removeCategory(_id:string, parentId?: string):ng.IPromise<string> {
            return this.$http.delete('/data/classified/categories/' + _id + "/" + parentId);
        }

        //
        //  Classified report methods
        //

        public addReport(classifiedId:string, reason:string):ng.IPromise<string> {
            return this.$http.post('/data/classified/reports', {"classifiedId": classifiedId, "reason": reason});
        }

        public getReports():ng.IPromise<models.ClassifiedReport[]> {
            return this.$http.get('/data/classified/reports/');
        }

        public removeReport(_id:string):ng.IPromise<string> {
            return this.$http.delete('/data/classified/reports/' + _id);
        }
    }

}