
module smf.services {

    export class BlogsService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService:services.UsersService;

        private themes:models.UserInterest[];

        static $inject = ['$q','$http', 'UsersService'];

        public constructor($q: ng.IQService, $http: ng.IHttpService, UsersService:services.UsersService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
        }

        public getList(filters: models.BlogFilters): ng.IPromise<models.Blog[]> {
            filters = filters || new models.BlogFilters();
            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/blogs', {params: params}).success((response:ng.IHttpPromiseCallback<models.Blog[]>)=>{
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public remove(type: string, param: string): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/blogs/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public create(blog: any): ng.IPromise<string> {
            return this.$http.post('/data/blogs',blog);
        }

        public getDetails(type: string, param: string): ng.IPromise<models.Blog> {
            var deferred = this.$q.defer();
            this.$http.get('/data/blogs/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public refreshPosts() {
            return this.$http.get('/data/blogs/refresh');
        }

        //
        //  Blogs theme methods
        //

        public getThemes(force?:boolean):ng.IPromise<models.UserInterest[]> {
            var deferred = this.$q.defer();
            if (this.themes && !force) {
                deferred.resolve(this.themes);
            }
            this.UsersService.getInterests(true).then((response:models.UserInterest[])=> {
                this.themes = response;
                deferred.resolve(this.themes);
            });
            return deferred.promise;
        }

        public getCategory(_id:string):ng.IPromise<models.UserInterest> {
            var deferred = this.$q.defer();
            this.getThemes()
                .then((themes:models.UserInterest[]) => {
                    for (var i in themes) {
                        if (themes[i]._id == _id) {
                            deferred.resolve(themes[i]);
                        }
                    }
                    deferred.reject();
                });
            return deferred.promise;
        }

        public addTheme(name:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/blogs/themes', {"name": name}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public updateTheme(_id:string, name:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.put('/data/blogs/themes', {"_id": _id, "name": name}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public removeTheme(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/blogs/themes/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }
    }

}