
module smf.services {

    export class PostService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;

        static $inject = ['$q','$http','UsersService','LoggedUserService'];

        public constructor($q: ng.IQService, $http: ng.IHttpService, UsersService: services.UsersService, LoggedUserService: services.LoggedUserService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
        }


        //
        //  General post methods
        //

        public getAll(filters:models.PostFilters):ng.IPromise<models.Post[]> {
            filters = filters || new models.PostFilters();

            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/posts/', {params: params}).success((response:ng.IHttpPromiseCallback<models.Post[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public getDetails(type: string, param: string): ng.IPromise<models.Post> {
            var deferred = this.$q.defer();
            this.$http.get('/data/posts/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<models.Post[]>) => {
                deferred.resolve(response[0]);
            })
            return deferred.promise;
        }

        public create(post: models.Post): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/posts',post).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public remove(type: string, param: string): ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/posts/'+param+'/'+type).success((response: ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getImages(images: string[]):ng.IPromise<models.Image[]> {
            var deferred = this.$q.defer();
            if(images.length == 0) {
                deferred.resolve([]);
            }
            else {
                this.$http.get('/data/images/' + images).success((response:ng.IHttpPromiseCallback<models.Image[]>) => {
                    deferred.resolve(response);
                })
            }
            return deferred.promise;
        }

        public getVideo(video: string):ng.IPromise<models.Video> {
            var deferred = this.$q.defer();
            if(!video || video == "") {
                deferred.resolve();
            }
            else {
                this.$http.get('/data/videos/' + video).success((response:ng.IHttpPromiseCallback<models.Video>) => {
                    deferred.resolve(response[0]);
                })
            }
            return deferred.promise;
        }

        public enhance(element:models.Post):ng.IPromise<models.Post> {
            var deferred = this.$q.defer();

            var promises: ng.IPromise<any>[] = [];
            promises.push(this.UsersService.getDetails("_id",element.userId));
            promises.push(this.LoggedUserService.getUser());
            promises.push(this.getImages(element.images));
            promises.push(this.getVideo(element.video));
            if(element.targetUserId) {
                promises.push(this.UsersService.getDetails("_id",element.targetUserId));
            }

            this.$q.all(promises)
                .then((response:any[])=> {
                    var loggedUser: models.LoggedUser = response[1];
                    element.user = angular.copy(response[0][0]);
                    element.imagesObj = angular.copy(response[2]);
                    element.videoObj = angular.copy(response[3]);
                    if(response[4]) {
                        element.targetUser = angular.copy(response[4][0]);
                    }

                    for(var i in element.likes.data) {
                        if(element.likes.data[i].userId == loggedUser._id) {
                            element.likes.userLiked = true;
                            break;
                        }
                    }

                    for(var i in element.comments.data) {
                        for(var j in element.comments.data[i].likes.data) {
                            if(element.comments.data[i].likes.data[j].userId == loggedUser._id) {
                                element.comments.data[i].likes.userLiked = true;
                                break;
                            }
                        }
                    }

                    this.UsersService.enhance(element.user);
                    if(element.targetUser) {
                        this.UsersService.enhance(element.targetUser);
                    }
                    deferred.resolve(element);
                })

            return deferred.promise;
        }


        //
        //  post likes methods
        //


        public addLike(postId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/posts/like/'+postId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLike(postId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/posts/like/'+postId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public addLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/posts/like/comment/'+commentId,{}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeLikeComment(commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/posts/like/comment/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }


        //
        //  post comments methods
        //


        public addComment(postId: string, comment: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/posts/comment/'+postId,{"comment":comment}).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }
        public removeComment(postId: string,commentId: string): ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/posts/comment/'+postId+'/'+commentId).success((response: ng.IHttpPromiseCallback<any>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        //
        //  Post report methods
        //

        public addReport(postId:string, reason:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/posts/reports', {"postId": postId, "reason": reason}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public getReports():ng.IPromise<models.PostReport[]> {
            var deferred = this.$q.defer();
            this.$http.get('/data/posts/reports/').success((response:models.PostReport[])=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public removeReport(_id:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/posts/reports/' + _id).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }
    }

}