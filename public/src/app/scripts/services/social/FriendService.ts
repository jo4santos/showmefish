
module smf.services {

    export class FriendService implements IUserBaseService {

        private $q: ng.IQService;
        private $http: ng.IHttpService;
        private UsersService: services.UsersService;
        private LoggedUserService: services.LoggedUserService;
        private $modal: ng.ui.bootstrap.IModalService;

        static $inject = ['$q','$http','UsersService','LoggedUserService'];

        public constructor($q: ng.IQService,
                           $http: ng.IHttpService,
                           UsersService: services.UsersService,
                           LoggedUserService: services.LoggedUserService,
                           $modal: ng.ui.bootstrap.IModalService)
        {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
            this.$modal = $modal;
        }

        public getAll(filters:models.UserFilters):ng.IPromise<models.User[]> {
            filters = filters || new models.UserFilters();

            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/users/', {params: params}).success((response:ng.IHttpPromiseCallback<models.User[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public invite(userId:string):ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/friends/'+userId, {}).success((response:ng.IHttpPromiseCallback<any>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public accept(userId:string):ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post('/data/friends/action/'+userId, {}).success((response:ng.IHttpPromiseCallback<any>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public remove(userId:string):ng.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/friends/'+userId, {}).success((response:ng.IHttpPromiseCallback<any>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public select(excluded: string[]):ng.IPromise<any> {
            var deferred = this.$q.defer();

            excluded = excluded || [];

            var modalSettings: ng.ui.bootstrap.IModalSettings = {};
            modalSettings.templateUrl = "app/partials/common/modal-select-users.html";
            modalSettings.controller = "controllers.ModalSelectUsersController as ModalSelectUsersController";
            modalSettings.resolve = {
                DataService: () => {
                    return this;
                },
                excluded: () => {
                    return excluded;
                },
                description: () => {
                    return "Seleccione os amigos que pretende adicionar ao grupo";
                },
                title: () => {
                    return "Seleccione os seus amigos";
                }
            }
            this.$modal.open(modalSettings).result.then((selectedItems:any[]) => {
                deferred.resolve(selectedItems);
            },()=>{
                deferred.reject();
            });

            return deferred.promise;
        }
    }

}