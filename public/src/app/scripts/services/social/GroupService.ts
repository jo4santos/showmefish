module smf.services {

    export class GroupService {

        private $q:ng.IQService;
        private $http:ng.IHttpService;
        private UsersService:services.UsersService;
        private LoggedUserService:services.LoggedUserService;

        static $inject = ['$q', '$http', 'UsersService', 'LoggedUserService'];

        public constructor($q:ng.IQService, $http:ng.IHttpService, UsersService:services.UsersService, LoggedUserService:services.LoggedUserService) {
            this.$q = $q;
            this.$http = $http;
            this.UsersService = UsersService;
            this.LoggedUserService = LoggedUserService;
        }


        //
        //  General group methods
        //

        public getAll(filters:models.GroupFilters):ng.IPromise<models.Group[]> {
            filters = filters || new models.GroupFilters();

            var params:any = {};
            params.filters = filters;

            var deferred = this.$q.defer();
            this.$http.get('/data/groups/', {params: params}).success((response:ng.IHttpPromiseCallback<models.Group[]>)=> {
                deferred.resolve(response);
            });
            return deferred.promise;
        }

        public getDetails(type:string, param:string):ng.IPromise<models.Group> {
            var deferred = this.$q.defer();
            this.$http.get('/data/groups/' + param + '/' + type).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve(response);
            })
            return deferred.promise;
        }

        public create(group:models.Group):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/groups', group).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public remove(type:string, param:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/groups/' + type + '/' + param).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        //
        //  General user actions
        //


        public join(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/groups/subscription/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public leave(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/groups/subscription/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public acceptRequest(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/groups/subscription/action/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public denyRequest(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/groups/subscription/action/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public addUsers(groupId:string, userId:string, users:string[]):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/groups/subscription/' + groupId, {userId:userId,users:users}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        //
        //  General group admin actions
        //


        public addAdmin(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.post('/data/groups/admin/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

        public removeAdmin(groupId:string, userId?:string):ng.IPromise<string> {
            var deferred = this.$q.defer();
            this.$http.delete('/data/groups/admin/' + groupId + '/' + userId, {}).success((response:ng.IHttpPromiseCallback<string>) => {
                deferred.resolve("OK");
            })
            return deferred.promise;
        }

    }

}