module smf.models {

    export class Classified {
        _id: string;
        userId: string;
        type: string;
        state: string;
        name: string;
        description: string;
        images: string [];
        imagesObj: models.Image[];
        category: string;
        categoryObj: ClassifiedCategory;
        price: string;
        location: string;
        timestamp: string;
        endDate: string;
        likes: {
            count: number;
            data: string [];
        };
        views: {
            count: number;
            data: string [];
        };
    }

}