module smf.models {

    export class ClassifiedCategory {
        _id: string;
        name: string;
        children: {
            _id: string;
            name: string;
        }[]
    }

}