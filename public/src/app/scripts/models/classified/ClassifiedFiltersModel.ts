module smf.models {

    export class ClassifiedFilters {
        userId: string;
        category: string;
        categoryObj: ClassifiedCategory;
        type: string;
        text: string;
        priceMin: string;
        priceMax: string;
    }

}