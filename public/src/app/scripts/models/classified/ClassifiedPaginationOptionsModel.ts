module smf.models {

    export class ClassifiedPaginationOptions {
        page: number;
        limit: number;

        // Probably we should use the last timestamp to get the pagination for performance issues ...
    }

}