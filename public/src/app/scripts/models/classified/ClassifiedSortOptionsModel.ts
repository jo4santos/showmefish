module smf.models {

    export class ClassifiedSortOptions {
        field: string;
        direction: string;
    }

}