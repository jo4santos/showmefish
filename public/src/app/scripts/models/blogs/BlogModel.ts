module smf.models {

    export class Blog {
        userId: number;
        url: string;
        name: string;
        description: string;
        image: string;
        theme: string;
        themeObj: UserInterest;
        items: any[];
    }

}