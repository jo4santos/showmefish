module smf.models {

    export class BlogFilters {
        userId: string;
        theme: string;
        themeObj: UserInterest;
        text: string;
    }

}