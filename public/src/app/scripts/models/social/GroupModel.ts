module smf.models {

    export class Group {
        _id:string;
        timestamp:string;
        creatorId:string;
        name:string;
        description:string;
        isPrivate:boolean;
        isSecret:boolean;
        interests:string[];
        users:string[];
        admins:string[];
        requests:string[];
    }

}