module smf.models {

    export class PostFilters {
        userId: string;
        targetUserId: string;
        targetGroupId: string;
    }

}