module smf.models {

    export class Post {
        _id:string;
        userId: string;
        targetUserId: string;
        targetGroupId: string;
        user: models.User;
        targetUser: models.User;
        description: string;
        timestamp: string;
        images: string[];
        imagesObj: models.Image[];
        video: string;
        videoObj: models.Video;
        likes: Likes;
        views: Views;
        comments: Comments;
    }

}