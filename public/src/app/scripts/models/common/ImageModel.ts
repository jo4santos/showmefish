module smf.models {

    export class Image {
        _id: string;
        userId: string;
        user: models.User;
        title: string;
        timestamp: string;
        likes: Likes;
        views: Views;
        comments: Comments;
    }

}