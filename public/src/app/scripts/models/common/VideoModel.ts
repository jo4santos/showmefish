module smf.models {

    export class Video {
        _id: string;
        userId: string;
        user: models.User;
        url: string;
        title: string;
        timestamp: string;
        likes: Likes;
        views: Views;
        comments: Comments;
    }

}