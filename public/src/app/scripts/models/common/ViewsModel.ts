module smf.models {

    export class Views {
        count: number;
        data: View[];
    }

    export class View {
        _id: string;
        userId: string;
        timestamp: string;
    }
}