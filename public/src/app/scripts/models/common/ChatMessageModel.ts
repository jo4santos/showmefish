module smf.models {
    export class ChatMessage {
        _id: string;
        userId: string;
        content: string;
        timestamp: string;
        views: string[];
    }
}