module smf.models {

    export class Likes {
        count: number;
        userLiked: boolean;
        data: Like[];
    }

    export class Like {
        _id: string;
        userId: string;
        timestamp: string;
    }
}