module smf.models {

    export class UserFilters {
        interests: string[];
        interestsObj: UserInterest[];
        text: string;
    }

}