module smf.models {

    export class NavigationPage {
        text        :string;
        url         :string;
        children    :smf.models.NavigationPage[];
    }

}