module smf.models {
    export class Chat {
        _id: string;
        timestamp: string;
        creatorId: string;
        name: string;
        classifiedId: string;
        users: string[];
        messages: {
            count: number;
            data: ChatMessage[];
        };
        unreadCount: string;
    }
}