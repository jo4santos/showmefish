module smf.models {

    export class User {
        _id:string;
        coverImage:string;
        image:string;
        imageObj:models.Image;
        email:string;
        password:string;
        firstname:string;
        lastname:string;
        gender:string;
        birthdate:string;
        introduction:string;
        role:string;
        facebook:{
            id          : string;
            token       : string;
            email       : string;
            name        : string;
        };
        google:{
            id          : string;
            token       : string;
            email       : string;
            name        : string;
        };
        interests:string[];
        interestsObj:models.UserInterest[];
        firstaccessDate:number;
        friends:{
            confirmed   : string[];
            sent        : string[];
            received    : string[];
        };
        notifications:{
            lastReadDate: string;
        };
    }

}