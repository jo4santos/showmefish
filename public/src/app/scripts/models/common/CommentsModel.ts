module smf.models {

    export class Comments {
        count: number;
        data: Comment[];
    }

    export class Comment {
        _id: string;
        userId: string;
        timestamp: string;
        comment: string;
        likes: Likes;
        user: models.User;
    }
}

module smf.controllers {
    export interface ICommentController {
        removeComment(comment:models.Comment);
        toggleLikeComment(comment:models.Comment);
    }
}