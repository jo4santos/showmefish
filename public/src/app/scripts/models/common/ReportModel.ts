module smf.models {

    export class Report {
        _id: string;
        reporterId: string;
        timestamp: string;
        reason: string;
    }

}