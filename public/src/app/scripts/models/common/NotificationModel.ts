module smf.models {
    export class Notification {
        _id: string;
        timestamp: string;
        creatorId: string;
        targetId: string;
        postId: string;
        type: string;
        read: boolean;
    }
}