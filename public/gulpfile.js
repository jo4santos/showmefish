var clean = require('gulp-clean');
var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var filter = require('gulp-filter');
var uglify = require('gulp-uglify');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var debug = require('gulp-debug');
var inject = require('gulp-inject');
var order = require("gulp-order");
var replace = require("gulp-replace");
var regexreplace = require("gulp-regex-replace");
var obfuscate = require('gulp-obfuscate');
var es = require('event-stream');
var bower = require('bower');
var minifycss = require('gulp-minify-css');
var rename = require('gulp-rename');
var Q = require('q');

//Command line argument processing
var argv = require('yargs')
    .usage("-d -b [live|stage]")
    .describe("d", "Distribution mode, js will be uglified and css minified")
    .alias("b", "beta")
    .describe("b", "The environment to which to build the beta")
    .argv;

var build = require('./build.json');

//The paths where the sources are located
var tscSources = [
    'src/cdn/vendor/b/jquery/dist/*.min.*'
    ,'src/cdn/vendor/b/bootstrap-sass-official/assets/fonts/**/*'
    ,'src/cdn/vendor/b/bootstrap-sass-official/assets/javascripts/bootstrap.js'
    ,'src/cdn/vendor/b/font-awesome/fonts/**/*'
    ,'src/cdn/vendor/b/roboto-fontface/fonts/**/*'

    ,'src/cdn/assets/**/*'
    ,'src/cdn/images/**/*'
    ,'src/cdn/scripts/smf/**/*'

    ,'src/app/vendor/typings/**/*'
    ,'src/app/vendor/b/angular/**/*'
    ,'src/app/vendor/b/angular-bootstrap/ui-bootstrap-tpls.min.js'
    ,'src/app/vendor/b/bootstrap-sass-official/assets/fonts/**/*'
    ,'src/app/vendor/b/angular-ui-router/api/angular-ui-router.d.ts'
    ,'src/app/vendor/b/angular-ui-router/release/angular-ui-router.min.js'
    ,'src/app/vendor/b/font-awesome/fonts/**/*'
    ,'src/app/vendor/b/roboto-fontface/fonts/**/*'
    ,'src/app/vendor/b/socket.io-client/socket.io.js'
    ,'src/app/vendor/b/socket.io-client/lib/**/*'
    ,'src/app/vendor/b/jquery/dist/jquery.min.js'
    ,'src/app/vendor/b/jquery/dist/jquery.min.map'
    ,'src/app/vendor/b/ng-file-upload/angular-file-upload-shim.min.js'
    ,'src/app/vendor/b/ng-file-upload/angular-file-upload.min.js'
    ,'src/app/vendor/b/ng-cropper/dist/ngCropper.all.min.*'
    ,'src/app/vendor/b/angular-scroll-glue/src/scrollglue.js'
    ,'src/app/vendor/b/anguvideo/js/anguvideo.js'

    ,'src/app/scripts/**/*'
    ,'src/app/partials/**/*'
    ,'src/app/images/**/*'
];
var sassSources = [
    ,'src/cdn/styles/smf/**/*'
    ,'src/app/vendor/vendor.scss'
    ,'src/app/styles/**/*'
];

//The preferred order in which the sources should be processed
var vendorPreferredOrder = [
    '**/jquery.min.js'
    ,'**/angular.js'
    ,'**/bootstrap.js'
    ,'**/ui-bootstrap.min.js'
    ,'**/angular-ui-router.min.js'
];

var smfPreferredOrder= [
    '**/smf/auth-login.js'
    ,'**/smfApp.js'
    ,'**/models/common/UserModel.js'
    ,'**/models/common/ReportModel.js'
    ,'**/models/**/*'
    ,'**/services/**/*'
    ,'**/controllers/**/*'
];
var target = "build";

function createPipeline(enableDist){

            // vendor javascript files
            var vendorJsStream =
                gulp.src([
                        target + '/app/vendor/b/angular/angular.js'
                        ,target + '/app/vendor/b/angular-ui-router/release/angular-ui-router.min.js'
                        ,target + '/app/vendor/b/angular-bootstrap/ui-bootstrap-tpls.min.js'
                        ,target + '/app/vendor/b/socket.io-client/socket.io.js'
                        ,target + '/app/vendor/b/jquery/dist/jquery.min.js'
                        ,target + '/app/vendor/b/ng-file-upload/angular-file-upload-shim.min.js'
                    ,target + '/app/vendor/b/ng-file-upload/angular-file-upload.min.js'
                    ,target + '/app/vendor/b/ng-cropper/dist/ngCropper.all.min.js'
                    ,target + '/app/vendor/b/angular-scroll-glue/src/scrollglue.js'
                    ,target + '/app/vendor/b/anguvideo/js/anguvideo.js'
                ])
                .pipe(order(vendorPreferredOrder));

            // auth injected files
            var authCssStream =
                gulp.src(
                    gulpif(enableDist
                        ,[target + '/cdn/styles/smf/auth.min.css']
                        ,[target + '/cdn/styles/smf/auth.css']
                    ));

            var authJsStream =
                gulp.src([
                    target + '/cdn/vendor/b/jquery/dist/jquery.min.js'
                    ,target + '/cdn/vendor/b/bootstrap-sass-official/assets/javascripts/bootstrap.js'
                    ,target + '/cdn/scripts/smf/auth.js'
                ]);

            // application injected files
            var smfCssStream =
                gulp.src(
                    gulpif(enableDist
                        ,[target + '/app/styles/main.min.css']
                        ,[target + '/app/styles/main.css']
                    ));

            var smfJsStream =
                gulp.src([
                    target + '/app/scripts/**/*.js'
                ])
                .pipe(order(smfPreferredOrder));

            // inject login.html
            gulp.src('src/static/login.html')
                .pipe(inject(authJsStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(inject(authCssStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(replace('$title', build.title))
                .pipe(gulp.dest(target + "/static/"));

            // inject signup.html
            gulp.src('src/static/signup.html')
                .pipe(inject(authJsStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(inject(authCssStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(replace('$title', build.title))
                .pipe(gulp.dest(target + "/static/"));

            // inject recover.html
            gulp.src('src/static/recover.html')
                .pipe(inject(authJsStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(inject(authCssStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(replace('$title', build.title))
                .pipe(gulp.dest(target + "/static/"));

            // inject firstaccess.html
            gulp.src('src/static/firstaccess.html')
                .pipe(inject(authJsStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(inject(authCssStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(replace('$title', build.title))
                .pipe(gulp.dest(target + "/static/"));

            // inject index.html
            gulp.src('src/index.html')
                .pipe(inject(vendorJsStream,{ignorePath: '/' + target, addRootSlash: false, starttag: '<!-- inject:vendor:js -->'}))
                .pipe(inject(smfJsStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(inject(smfCssStream,{ignorePath: '/' + target, addRootSlash: false}))
                .pipe(replace('$title', build.title))
                .pipe(gulp.dest(target));

}

var typescript = require('gulp-tsc');



gulp.task('clean', function(){
    return gulp.src(["build"])
        .pipe(clean());
});

gulp.task('compile', ['clean','compile-typescript','compile-sass'], function(){

    return createPipeline(argv.d != null);
});

var tscFilter = filter("**/*.ts");
var sassFilter = filter("**/*.scss");

gulp.task('compile-typescript', ['clean'], function () {
    var deferred = Q.defer();

    var tscFilter = filter("**/*.ts");

    gulp.src(tscSources, {base: 'src/'})

        //Tsc
        .pipe(tscFilter)
        .pipe(typescript({emitError: true, safe: false, module: 'commonjs'}))
        .pipe(tscFilter.restore())
        .pipe(gulp.dest(target))
        .on("end",function() {
            deferred.resolve();
        })
    return deferred.promise;
});

gulp.task('compile-sass', ['clean'], function () {
    var deferred = Q.defer();

    var sassFilter = filter("**/*.scss");

    gulp.src(sassSources, {base: 'src/'})
    .pipe(sassFilter)
        .pipe(sass({sourceComments: 'normal', errLogToConsole: true}))
        .pipe(sassFilter.restore())
        .pipe(gulp.dest(target))
        .on("end",function() {
            deferred.resolve();
        })
    return deferred.promise;
});

gulp.task('default', ['compile']);